module world
    public :: print, blinker, beacon, glider, block, tub
    contains
        subroutine print(world)
            integer, dimension(30, 30) :: world
            character (len = 4) x
            integer :: i, j

            print *, ""
            write(*, '(A)', advance="no") '    '
            do i = 1, ubound(world, 1)
                if (i == 1) then
                    do j = 1, ubound(world, 2)
                        write(*, '(I3)', advance="no") j - 1
                        write(*, '(A)', advance="no") '|'
                    end do
                    print *, ""
                end if
                do j = 1, ubound(world, 2)
                    if (j == 1) then
                        write(*, '(I3)', advance="no") i - 1
                        write(*, '(A)', advance="no") '|'
                    end if
                    if (world(j, i) == 1) then
                        x = ' x |'
                    else
                        x = '   |'
                    end if
                    write(*, '(A)', advance="no") x
                end do
                print *, ''
            end do
            print *, ""
        end subroutine print

        function evolve(world) result(result)
            integer, dimension(30, 30) :: world, result, copy
            integer :: x, y, a, b, counter

            copy = world
            world = 0
            result = 0
            do x = 1, ubound(copy, 1)
                do y = 1, ubound(copy, 2)
                    counter = 0
                    do a = x - 1, x + 1
                        do b = y - 1, y + 1
                            if (a >= 1 .and. b >= 1 .and. a <= ubound(copy, 1) .and. b <= ubound(copy, 2)) then
                                counter = counter + copy(a, b)
                            end if
                        end do
                    end do
                    counter = counter - copy(x, y)
                    select case (copy(x, y))
                    case (1)
                        if (counter == 2 .or. counter == 3) then
                            result(x, y) = 1
                        end if
                    case default
                        if (counter == 3) then
                            result(x, y) = 1
                        end if
                    end select
                end do
            end do
        end function evolve

        subroutine blinker(world, x, y)
            integer, dimension(30, 30) :: world
            integer :: x, y

            world(x + 1, y + 1) = 1
            world(x + 2, y + 1) = 1
            world(x + 3, y + 1) = 1
        end subroutine blinker

        subroutine beacon(world, x, y)
            integer, dimension(30, 30) :: world
            integer :: x, y

            world(x + 1, y + 1) = 1
            world(x + 2, y + 1) = 1
            world(x + 1, y + 2) = 1
            world(x + 2, y + 2) = 1
            world(x + 3, y + 3) = 1
            world(x + 4, y + 3) = 1
            world(x + 3, y + 4) = 1
            world(x + 4, y + 4) = 1
        end subroutine beacon

        subroutine glider(world, x, y)
            integer, dimension(30, 30) :: world
            integer :: x, y

            world(x + 3, y + 3) = 1
            world(x + 2, y + 3) = 1
            world(x + 1, y + 3) = 1
            world(x + 3, y + 2) = 1
            world(x + 2, y + 1) = 1
        end subroutine glider

        subroutine block(world, x, y)
            integer, dimension(30, 30) :: world
            integer :: x, y

            world(x + 1, y + 1) = 1
            world(x + 2, y + 1) = 1
            world(x + 1, y + 2) = 1
            world(x + 2, y + 2) = 1
        end subroutine block

        subroutine tub(world, x, y)
            integer, dimension(30, 30) :: world
            integer :: x, y

            world(x + 2, y + 1) = 1
            world(x + 1, y + 2) = 1
            world(x + 3, y + 2) = 1
            world(x + 2, y + 3) = 1
        end subroutine tub
end module world
