program conway
    use world
    implicit none
    integer, dimension(30, 30) :: array
    integer :: generation

    generation = 0
    array = 0
    call blinker(array, 0, 1)
    call beacon(array, 10, 10)
    call glider(array, 4, 5)
    call block(array, 1, 10)
    call block(array, 18, 3)
    call tub(array, 6, 1)

    do
        generation = generation + 1
        call execute_command_line("clear")
        call print(array)
        write(*, '(A)', advance="no") 'Generation '
        write(*, '(I3)') generation
        print *, ""
        array = evolve(array)
        call sleep(1)
    end do
end program conway
