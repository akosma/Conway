"""
Cell enumeration
"""

from enum import Enum

class Cell(Enum):
    """
    Represents a Cell in a World
    """
    DEAD = 0
    ALIVE = 1

    def __str__(self):
        return "   |" if self == Cell.DEAD else " x |"
