"""
Conway module
"""
#pylint: disable=cyclic-import

from .coord import Coord
from .cell import Cell
from .world import World
