"""
Coord structure
"""

from dataclasses import dataclass

@dataclass(eq=True, frozen=True)
class Coord:
    """
    Represents a coordinate in a Conway world.
    """
    x_coord: int
    y_coord: int
