"""
World class
"""

from itertools import product
from . import Cell, Coord

NEIGHBORS = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

class World:
    """
    Represents a World class.
    """

    def __init__(self, size, alive_cells):
        self.size = size
        self.cells = {}
        for coord_x, coord_y in product(range(0, self.size), range(0, self.size)):
            coord = Coord(coord_x, coord_y)
            self.cells[coord] = Cell.ALIVE if coord in alive_cells else Cell.DEAD

    def evolve(self):
        """
        Returns a new World instance, representing its next step of evolution.
        """
        alive_cells = []
        for key, value in self.cells.items():
            count = 0
            for current_coord in [Coord(key.x_coord + c[0], key.y_coord + c[1]) for c in NEIGHBORS]:
                if current_coord in self.cells and self.cells[current_coord] == Cell.ALIVE:
                    count += 1
            match value:
                case Cell.ALIVE if count in (2, 3):
                    alive_cells.append(key)
                case Cell.DEAD if count == 3:
                    alive_cells.append(key)
        return World(self.size, alive_cells)

    def __eq__(self, other):
        return (self.size, self.cells) == (other.size, other.cells)

    def __str__(self):
        result = "\n"
        for coord_x in range(0, self.size):
            # First line with coordinates
            if coord_x == 0:
                result += "    "
                for coord_y in range(0, self.size):
                    result += f"{coord_y: 3}|"
                result += "\n"
            result += f"{coord_x: 3}|"
            for coord_y in range(0, self.size):
                coord = Coord(coord_y, coord_x)
                result += f"{self.cells[coord]}"
            result += "\n"
        return result

    @staticmethod
    def blinker(origin):
        """
        Returns a set of coordinates representing a blinker.
        """
        return [Coord(origin.x_coord, origin.y_coord),
                Coord(origin.x_coord + 1, origin.y_coord),
                Coord(origin.x_coord + 2, origin.y_coord)]

    @staticmethod
    def beacon(origin):
        """
        Returns a set of coordinates representing a beacon.
        """
        return [Coord(origin.x_coord, origin.y_coord),
                Coord(origin.x_coord + 1, origin.y_coord),
                Coord(origin.x_coord, origin.y_coord + 1),
                Coord(origin.x_coord + 1, origin.y_coord + 1),
                Coord(origin.x_coord + 2, origin.y_coord + 2),
                Coord(origin.x_coord + 3, origin.y_coord + 2),
                Coord(origin.x_coord + 2, origin.y_coord + 3),
                Coord(origin.x_coord + 3, origin.y_coord + 3)]

    @staticmethod
    def glider(origin):
        """
        Returns a set of coordinates representing a glider.
        """
        return [Coord(origin.x_coord + 2, origin.y_coord + 2),
                Coord(origin.x_coord + 1, origin.y_coord + 2),
                Coord(origin.x_coord, origin.y_coord + 2),
                Coord(origin.x_coord + 2, origin.y_coord + 1),
                Coord(origin.x_coord + 1, origin.y_coord)]

    @staticmethod
    def block(origin):
        """
        Returns a set of coordinates representing a block.
        """
        return [Coord(origin.x_coord, origin.y_coord),
                Coord(origin.x_coord + 1, origin.y_coord),
                Coord(origin.x_coord, origin.y_coord + 1),
                Coord(origin.x_coord + 1, origin.y_coord + 1)]

    @staticmethod
    def tub(origin):
        """
        Returns a set of coordinates representing a tub.
        """
        return [Coord(origin.x_coord + 1, origin.y_coord),
                Coord(origin.x_coord, origin.y_coord + 1),
                Coord(origin.x_coord + 2, origin.y_coord + 1),
                Coord(origin.x_coord + 1, origin.y_coord + 2)]
