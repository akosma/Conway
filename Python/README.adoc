= Python

This application is written in Python 3.10, and can be run using the `./main.py` command.

It has no pylint warnings when checked with `pylint **/*.py`

== Installation

Use the following commands to bootstrap an environment for this application:

[source]
--
$ python -m venv .venv
$ source .venv/bin/activate
$ python --version
Python 3.10.0
$ pip install -r requirements.txt
--

NOTE: In Ubuntu, `sudo apt-get install libffi-dev` might be required. If you installed Python 3.10 using `pyenv`, try `pyenv install 3.10.0 --force` first, and then recreate the local `venv` environment.

== Tests

The Python application can be tested by running `./tests.py` at the root of the folder.
