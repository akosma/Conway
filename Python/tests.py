#!/usr/bin/env python3

"""
Definition of tests for the Conway application.
"""

from unittest import TestCase, main
from conway import World, Coord

class ConwayTestCase(TestCase):
    """
    Tests module for the Conway application.
    """
    def test_block(self):
        """
        Tests the evolution of a "block" in a Conway world.
        """
        alive = World.block(Coord(0, 0))
        original = World(5, alive)
        next_world = original.evolve()
        self.assertEqual(next_world, original)

    def test_tub(self):
        """
        Tests the evolution of a "tub" in a Conway world.
        """
        alive = World.tub(Coord(0, 0))
        original = World(5, alive)
        next_world = original.evolve()
        self.assertEqual(next_world, original)

    def test_blinker(self):
        """
        Tests the evolution of a "blinker" in a Conway world.
        """
        alive = World.blinker(Coord(0, 1))
        original = World(3, alive)
        gen1 = original.evolve()
        expected_alive = [Coord(1, 0),
                          Coord(1, 1),
                          Coord(1, 2)]
        expected = World(3, expected_alive)
        self.assertEqual(expected, gen1)
        gen2 = gen1.evolve()
        self.assertEqual(original, gen2)

if __name__ == "__main__":
    main()
