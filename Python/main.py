#!/usr/bin/env python3
"""
Main entry point of the Conway program.
"""

import os
import sys
from signal import signal, SIGINT
from time import sleep
from conway import World, Coord

def handler(received_signal, frame):
    """
    Called when receiving a SIGINT signal.
    """
    #pylint: disable=unused-argument
    os.system("clear")
    sys.exit(0)

def main():
    """
    Main entry of the program.
    """
    signal(SIGINT, handler)

    blinker = World.blinker(Coord(0, 1))
    beacon = World.beacon(Coord(10, 10))
    glider = World.glider(Coord(4, 5))
    block1 = World.block(Coord(1, 10))
    block2 = World.block(Coord(18, 3))
    tub = World.tub(Coord(6, 1))
    alive_cells = blinker + beacon + glider + block1 + block2 + tub
    world = World(30, alive_cells)
    generation = 0

    while True:
        os.system("clear")
        generation += 1
        print(world)
        print(f"Generation {generation}")
        sleep(0.5)
        world = world.evolve()

if __name__ == "__main__":
    main()
