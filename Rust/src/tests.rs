#[cfg(test)]
mod tests {
    use crate::coord::Coord;
    use crate::world::World;
    use std::vec::Vec;

    #[test]
    fn block() {
        let alive = World::block(Coord { x: 0, y: 0 });
        let original = World::create(5, alive);
        let next = original.evolve();
        assert_eq!(next, original);
    }

    #[test]
    fn tub() {
        let alive = World::tub(Coord { x: 0, y: 0 });
        let original = World::create(5, alive);
        let next = original.evolve();
        assert_eq!(next, original);
    }

    #[test]
    fn blinker() {
        let alive = World::blinker(Coord { x: 0, y: 1 });
        let original = World::create(3, alive);
        let gen1 = original.evolve();
        let mut expected_alive: Vec<Coord> = Vec::new();
        expected_alive.push(Coord { x: 1, y: 0 });
        expected_alive.push(Coord { x: 1, y: 1 });
        expected_alive.push(Coord { x: 1, y: 2 });
        let expected = World::create(3, expected_alive);
        assert_eq!(expected, gen1);
        let gen2 = gen1.evolve();
        assert_eq!(original, gen2);
    }
}
