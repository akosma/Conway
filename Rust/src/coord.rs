use std::fmt;

#[derive(Debug)]
pub struct Coord {
    pub x: i32,
    pub y: i32,
}

impl fmt::Display for Coord {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.x, self.y)
    }
}

impl std::cmp::Eq for Coord {}

impl std::cmp::PartialEq for Coord {
    fn eq(&self, other: &Coord) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

impl std::hash::Hash for Coord {
    fn hash<H>(&self, state: &mut H)
    where
        H: std::hash::Hasher,
    {
        state.write_i32((*self).x);
        state.write_i32((*self).y);
        let _ = state.finish();
    }
}

impl std::marker::Copy for Coord {}

impl Clone for Coord {
    fn clone(&self) -> Coord {
        return Coord {
            x: self.x,
            y: self.y,
        };
    }
}
