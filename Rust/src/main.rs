use std::{thread, time};
extern crate signal_hook;

mod cell;
mod coord;
mod tests;
mod world;

use coord::Coord;
use world::World;

// https://piers.rocks/rust/signal/sigint/container/docker/2019/01/06/rust-catch-ctrl-c-container.html
pub fn reg_for_sigs() {
    unsafe { signal_hook::register(signal_hook::SIGINT, || on_sigint()) }
        .and_then(|_| Ok(()))
        .or_else(|e| Err(e))
        .ok();
}

fn on_sigint() {
    clrscr();
    std::process::exit(0);
}

// https://stackoverflow.com/a/34837038/133764
fn clrscr() {
    print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
}

fn main() {
    reg_for_sigs();
    let mut alive = World::blinker(Coord { x: 0, y: 1 });
    alive.extend(World::beacon(Coord { x: 10, y: 10 }));
    alive.extend(World::glider(Coord { x: 4, y: 5 }));
    alive.extend(World::block(Coord { x: 1, y: 10 }));
    alive.extend(World::block(Coord { x: 18, y: 3 }));
    alive.extend(World::tub(Coord { x: 6, y: 1 }));
    let mut w : World = World::create(30, alive);
    let mut generation = 0;

    loop {
        clrscr();
        println!("{}", w);
        println!("Generation {}", generation);
        let interval = time::Duration::from_millis(500);
        thread::sleep(interval);
        w = w.evolve();
        generation += 1;
    }
}
