use std::fmt;

#[derive(Debug, PartialEq)]
pub enum Cell {
    Alive,
    Dead,
}

// https://stackoverflow.com/a/45893535/133764
impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Cell::Alive => write!(f, " x |"),
            Cell::Dead => write!(f, "   |"),
        }
    }
}

impl std::marker::Copy for Cell {}

impl Clone for Cell {
    fn clone(&self) -> Cell {
        *self
    }
}
