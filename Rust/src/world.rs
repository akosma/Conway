use std::collections::HashMap;
use std::fmt;
use std::vec::Vec;

use crate::cell::Cell;
use crate::coord::Coord;

#[derive(Debug, PartialEq)]
pub struct World {
    pub cells: HashMap<Coord, Cell>,
    pub size: i32,
}

impl World {
    pub fn create(size: i32, alive_cells: Vec<Coord>) -> World {
        let mut cells: HashMap<Coord, Cell> = HashMap::with_capacity(size as usize);
        for a in 0..size {
            for b in 0..size {
                let coord = Coord { x: a, y: b };
                if alive_cells.contains(&coord) {
                    cells.insert(coord, Cell::Alive);
                } else {
                    cells.insert(coord, Cell::Dead);
                }
            }
        }
        return World {
            cells: cells,
            size: size,
        };
    }

    pub fn evolve(&self) -> World {
        let mut alive: Vec<Coord> = Vec::new();
        for (coord, cell) in &(self.cells) {
            let mut count = 0;
            for a in -1..2 {
                for b in -1..2 {
                    let current_coord = Coord {
                        x: (*coord).x + a,
                        y: (*coord).y + b,
                    };
                    if current_coord != *coord
                        && self.cells.contains_key(&current_coord)
                        && self.cells[&current_coord] == Cell::Alive
                    {
                        count = count + 1;
                    }
                }
            }

            match cell {
                Cell::Alive => {
                    if count == 2 || count == 3 {
                        alive.push(*coord)
                    }
                }
                Cell::Dead => {
                    if count == 3 {
                        alive.push(*coord)
                    }
                }
            }
        }
        return World::create(self.size, alive);
    }

    pub fn blinker(coord: Coord) -> Vec<Coord> {
        return vec![
            Coord {
                x: coord.x,
                y: coord.y,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y,
            },
            Coord {
                x: coord.x + 2,
                y: coord.y,
            },
        ];
    }

    pub fn beacon(coord: Coord) -> Vec<Coord> {
        return vec![
            Coord {
                x: coord.x,
                y: coord.y,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y,
            },
            Coord {
                x: coord.x,
                y: coord.y + 1,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y + 1,
            },
            Coord {
                x: coord.x + 2,
                y: coord.y + 2,
            },
            Coord {
                x: coord.x + 3,
                y: coord.y + 2,
            },
            Coord {
                x: coord.x + 2,
                y: coord.y + 3,
            },
            Coord {
                x: coord.x + 3,
                y: coord.y + 3,
            },
        ];
    }

    pub fn glider(coord: Coord) -> Vec<Coord> {
        return vec![
            Coord {
                x: coord.x + 2,
                y: coord.y + 2,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y + 2,
            },
            Coord {
                x: coord.x,
                y: coord.y + 2,
            },
            Coord {
                x: coord.x + 2,
                y: coord.y + 1,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y,
            },
        ];
    }

    pub fn block(coord: Coord) -> Vec<Coord> {
        return vec![
            Coord {
                x: coord.x,
                y: coord.y,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y,
            },
            Coord {
                x: coord.x,
                y: coord.y + 1,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y + 1,
            },
        ];
    }

    pub fn tub(coord: Coord) -> Vec<Coord> {
        return vec![
            Coord {
                x: coord.x + 1,
                y: coord.y,
            },
            Coord {
                x: coord.x,
                y: coord.y + 1,
            },
            Coord {
                x: coord.x + 2,
                y: coord.y + 1,
            },
            Coord {
                x: coord.x + 1,
                y: coord.y + 2,
            },
        ];
    }
}

impl fmt::Display for World {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "\n").ok();
        for a in 0..self.size {
            if a == 0 {
                write!(f, "    ").ok();
                for b in 0..self.size {
                    write!(f, "{:3}|", b).ok();
                }
                write!(f, "\n").ok();
            }
            write!(f, "{:3}|", a).ok();
            for b in 0..self.size {
                let coord = Coord { x: b, y: a };
                let cell = self.cells[&coord];
                write!(f, "{}", cell).ok();
            }
            write!(f, "\n").ok();
        }
        return Ok(());
    }
}
