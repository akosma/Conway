package ma.akos.conway

// For more information on writing tests, see
// https://scalameta.org/munit/docs/getting-started.html
class MySuite extends munit.FunSuite {
  test("Block") {
    val alive = World.block(Coord(0, 0))
    val original = World(5, alive)
    val next = original.evolve
    assertEquals(original, next)
  }

  test("Tub") {
    val alive = World.tub(Coord(0, 0))
    val original = World(5, alive)
    val next = original.evolve
    assertEquals(original, next)
  }

  test("Blinker") {
    val alive = World.blinker(Coord(0, 1))
    val original = World(3, alive)
    val gen1 = original.evolve
    val expectedAlive = List(Coord(1, 0), Coord(1, 1), Coord(1, 2))
    val expected = World(3, expectedAlive)
    assertEquals(expected, gen1)
    val gen2 = gen1.evolve
    assertEquals(original, gen2)
  }
}
