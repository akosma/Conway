package ma.akos.conway

enum Cell {
  case Alive, Dead

  override def toString: String = this match {
          case Alive => " x |"
          case Dead  => "   |"
        }
}
