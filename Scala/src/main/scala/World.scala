package ma.akos.conway

import scala.collection.immutable.Map

class World(val size: Int, aliveCells: List[Coord]) {

  val cells: Map[Coord, Cell] = (for {
    a <- 0 until size
    b <- 0 until size
    coord = Coord(a, b)
  } yield coord -> (if (aliveCells.contains(coord)) Cell.Alive else Cell.Dead)).toMap

  override def equals(obj: Any): Boolean = obj match {
    case other: World if other.size == this.size =>
      this.cells == other.cells
    case _ => false
  }

  def evolve: World = {
    val nextAliveCells = cells.collect {
      case (coord, cell) =>
        val count = neighbors(coord).count(c => cells.getOrElse(c, Cell.Dead) == Cell.Alive)
        cell match {
          case Cell.Alive if count == 2 || count == 3 => Some(coord)
          case Cell.Dead if count == 3 => Some(coord)
          case _ => None
        }
    }.flatten.toList

    new World(size, nextAliveCells)
  }

  private def neighbors(coord: Coord): Seq[Coord] =
    for {
      dx <- -1 to 1
      dy <- -1 to 1 if !(dx == 0 && dy == 0)
    } yield Coord(coord.x + dx, coord.y + dy)

  override def toString: String = {
    val builder = new StringBuilder("\n")

    for (a <- 0 until size) {
      // First line with coordinates
      if (a == 0) {
        builder.append("    ")
        (0 until size).foreach(b => builder.append(f"$b%3d|"))
        builder.append("\n")
      }

      builder.append(f"$a%3d|")
      (0 until size).foreach { b =>
        val cell = cells(Coord(b, a))
        builder.append(cell)
      }
      builder.append("\n")
    }

    builder.toString()
  }
}

object World {
  def blinker(coord: Coord): List[Coord] =
    List(
      coord,
      coord.copy(x = coord.x + 1),
      coord.copy(x = coord.x + 2)
    )

  def beacon(coord: Coord): List[Coord] =
    List(
      coord,
      coord.copy(x = coord.x + 1),
      coord.copy(y = coord.y + 1),
      coord.copy(x = coord.x + 1, y = coord.y + 1),
      coord.copy(x = coord.x + 2, y = coord.y + 2),
      coord.copy(x = coord.x + 3, y = coord.y + 2),
      coord.copy(x = coord.x + 2, y = coord.y + 3),
      coord.copy(x = coord.x + 3, y = coord.y + 3)
    )

  def glider(coord: Coord): List[Coord] =
    List(
      coord.copy(x = coord.x + 2, y = coord.y + 2),
      coord.copy(x = coord.x + 1, y = coord.y + 2),
      coord.copy(y = coord.y + 2),
      coord.copy(x = coord.x + 2, y = coord.y + 1),
      coord.copy(x = coord.x + 1)
    )

  def block(coord: Coord): List[Coord] =
    List(
      coord,
      coord.copy(x = coord.x + 1),
      coord.copy(y = coord.y + 1),
      coord.copy(x = coord.x + 1, y = coord.y + 1)
    )

  def tub(coord: Coord): List[Coord] =
    List(
      coord.copy(x = coord.x + 1),
      coord.copy(y = coord.y + 1),
      coord.copy(x = coord.x + 2, y = coord.y + 1),
      coord.copy(x = coord.x + 1, y = coord.y + 2)
    )
}
