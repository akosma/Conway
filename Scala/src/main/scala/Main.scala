package ma.akos.conway

import scala.collection.mutable.ArrayBuffer

object Main:

  @volatile private var keepRunning: Boolean = true

  def clearScreen(): Unit =
    print("\u001b[H\u001b[2J")
    System.out.flush()

  def main(args: Array[String]): Unit =
    sys.addShutdownHook {
      clearScreen()
      keepRunning = false
    }

    var aliveCells : List[Coord] = List()
    aliveCells ++= World.blinker(Coord(0, 1))
    aliveCells ++= World.beacon(Coord(10, 10))
    aliveCells ++= World.glider(Coord(4, 5))
    aliveCells ++= World.block(Coord(1, 10))
    aliveCells ++= World.block(Coord(18, 3))
    aliveCells ++= World.tub(Coord(6, 1))

    var world = World(30, aliveCells)
    var generation = 0

    while keepRunning do
      clearScreen()
      generation += 1
      println(world)
      println(s"Generation $generation")
      world = world.evolve
      try Thread.sleep(500)
      catch case _: InterruptedException => keepRunning = false
