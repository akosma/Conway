#import "CWWorld.h"
#import "CWCell.h"

@implementation CWWorld

@synthesize size = _size;
@synthesize cells = _cells;

- (id)initWithSize:(int)size aliveCells:(OFArray *)cells {
    self = [super init];
    _size = size;
    OFMutableDictionary *aliveCells = [OFMutableDictionary dictionary];
    for (int a = 0; a < _size; ++a) {
        for (int b = 0; b < _size; ++b) {
            OFValue *coord = @(OFMakePoint(a, b));
            if ([cells containsObject:coord]) {
                aliveCells[coord] = @(kCWCellAlive);
            }
            else {
                aliveCells[coord] = @(kCWCellDead);
            }
        }
    }
    _cells = aliveCells;
    return self;
}

- (BOOL)isEqual:(CWWorld *)other {
    if (_size != other.size) return NO;
    for (OFValue *coord in self.cells) {
        if (other.cells[coord] == nil) return NO;
        OFValue *cell = self.cells[coord];
        OFValue *otherCell = other.cells[coord];
        if (![cell isEqual:otherCell]) return NO;
    }
    return YES;
}

- (CWWorld *)evolve {
    OFMutableArray *alive = [OFMutableArray array];
    for (OFValue *value in self.cells) {
        OFPoint coord = [value pointValue];
        int count = 0;
        for (int a = -1; a <= 1; ++a) {
            for (int b = -1; b <= 1; ++b) {
                OFValue *currentCoord = @(OFMakePoint((coord.x + a), (coord.y + b)));
                if (![currentCoord isEqual:value]
                    && self.cells[currentCoord].intValue == kCWCellAlive) count += 1;
            }
        }

        if (self.cells[value].intValue == kCWCellAlive) {
            if (count == 2 || count == 3) [alive addObject:value];
        }
        else {
            if (count == 3) [alive addObject:value];
        }
    }
    return [[CWWorld alloc] initWithSize:_size aliveCells:alive];
}

- (OFString *)description {
    OFMutableString *result = [OFMutableString stringWithString:@"    "];
    for (int a = 0; a < _size; ++a) {
        // First line with coordinates
        if (a == 0) {
            for (int b = 0; b < _size; ++b)
            {
                [result appendFormat:@"%3d|", b];
            }
            [result appendString:@"\n"];
        }

        [result appendFormat:@"%3d|", a];
        for (int b = 0; b < _size; ++b) {
            OFValue *coord = @(OFMakePoint(b, a));
            CWCell cell = self.cells[coord].intValue;
            [result appendString:cellDescription(cell)];
        }
        [result appendString:@"\n"];
    }
    return [result description];
}

+ (OFArray *)blinkerAt:(OFPoint)coord {
    return @[@(OFMakePoint( coord.x     , coord.y)),
             @(OFMakePoint((coord.x + 1), coord.y)),
             @(OFMakePoint((coord.x + 2), coord.y))];
}

+ (OFArray *)beaconAt:(OFPoint)coord {
    return @[@(OFMakePoint( coord.x     ,  coord.y)),
             @(OFMakePoint((coord.x + 1),  coord.y)),
             @(OFMakePoint( coord.x     , (coord.y + 1))),
             @(OFMakePoint((coord.x + 1), (coord.y + 1))),
             @(OFMakePoint((coord.x + 2), (coord.y + 2))),
             @(OFMakePoint((coord.x + 3), (coord.y + 2))),
             @(OFMakePoint((coord.x + 2), (coord.y + 3))),
             @(OFMakePoint((coord.x + 3), (coord.y + 3)))];
}

+ (OFArray *)gliderAt:(OFPoint)coord {
    return @[@(OFMakePoint((coord.x + 2), (coord.y + 2))),
             @(OFMakePoint((coord.x + 1), (coord.y + 2))),
             @(OFMakePoint( coord.x     , (coord.y + 2))),
             @(OFMakePoint((coord.x + 2), (coord.y + 1))),
             @(OFMakePoint((coord.x + 1),  coord.y))];
}

+ (OFArray *)blockAt:(OFPoint)coord {
    return @[@(OFMakePoint( coord.x     ,  coord.y)),
             @(OFMakePoint((coord.x + 1),  coord.y)),
             @(OFMakePoint( coord.x     , (coord.y + 1))),
             @(OFMakePoint((coord.x + 1), (coord.y + 1)))];
}

+ (OFArray *)tubAt:(OFPoint)coord {
    return @[@(OFMakePoint((coord.x + 1),  coord.y)),
             @(OFMakePoint( coord.x     , (coord.y + 1))),
             @(OFMakePoint((coord.x + 2), (coord.y + 1))),
             @(OFMakePoint((coord.x + 1), (coord.y + 2)))];
}

@end
