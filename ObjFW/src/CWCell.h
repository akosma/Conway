#import <ObjFW/ObjFW.h>

typedef enum { kCWCellDead, kCWCellAlive } CWCell;

OFString *cellDescription(CWCell cell) {
    if (cell == kCWCellAlive) {
        return @" x |";
    }
    return @"   |";
}
