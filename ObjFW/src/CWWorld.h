#import <ObjFW/ObjFW.h>

@interface CWWorld : OFObject

@property (readonly) int size;
@property (readonly, retain) OFDictionary<OFValue *, OFNumber *> *cells;

- (id)initWithSize:(int)size aliveCells:(OFArray *)cells;

- (CWWorld *)evolve;

+ (OFArray *)blinkerAt:(OFPoint)coord;

+ (OFArray *)beaconAt:(OFPoint)coord;

+ (OFArray *)gliderAt:(OFPoint)coord;

+ (OFArray *)blockAt:(OFPoint)coord;

+ (OFArray *)tubAt:(OFPoint)coord;

@end
