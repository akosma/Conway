#import <ObjFW/ObjFW.h>
#import "src/CWWorld.h"

volatile sig_atomic_t flag = 0;

@interface Conway: OFObject <OFApplicationDelegate> {
    int _generation;
    CWWorld *_world;
}

- (void)clearScreen;
- (void)run;
@end

OF_APPLICATION_DELEGATE(Conway)

@implementation Conway

- (void)applicationDidReceiveSIGINT {
    flag = 1;
}

- (void)applicationDidFinishLaunching: (OFNotification *)notification {
    OFMutableArray *alive = [OFMutableArray array];
    [alive addObjectsFromArray:[CWWorld blinkerAt:OFMakePoint(0, 1)]];
    [alive addObjectsFromArray:[CWWorld beaconAt:OFMakePoint(10, 10)]];
    [alive addObjectsFromArray:[CWWorld gliderAt:OFMakePoint(4, 5)]];
    [alive addObjectsFromArray:[CWWorld blockAt:OFMakePoint(1, 10)]];
    [alive addObjectsFromArray:[CWWorld blockAt:OFMakePoint(18, 3)]];
    [alive addObjectsFromArray:[CWWorld tubAt:OFMakePoint(6, 1)]];
    _world = [[CWWorld alloc] initWithSize:30 aliveCells:alive];
    _generation = 0;
    [self run];
}

- (void)clearScreen {
    [OFStdOut clear];
    [OFStdOut setCursorPosition:OFMakePoint(0, 0)];
}

- (void)run {
    _generation++;
    [self clearScreen];
    [OFStdOut writeFormat:@"%@ \nGeneration %d\n", _world, _generation];
    _world = [_world evolve];

    if (flag == 0) {
        [self performSelector:@selector(run) afterDelay:0.5];
    }
    else {
        [self clearScreen];
        [OFApplication terminate];
    }
}
@end
