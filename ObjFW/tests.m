#import <ObjFW/ObjFW.h>
#import <ObjFWTest/ObjFWTest.h>
#import "src/CWWorld.h"

@interface Tests: OTTestCase
@end

@implementation Tests

- (void)testBlock {
    OFArray *alive = [CWWorld blockAt:OFMakePoint(0, 0)];
    CWWorld *original = [[CWWorld alloc] initWithSize:5 aliveCells:alive];
    CWWorld *next = [original evolve];
    OTAssertTrue([next isEqual:original]);
}

- (void)testTub {
    OFArray *alive = [CWWorld tubAt:OFMakePoint(0, 0)];
    CWWorld *original = [[CWWorld alloc] initWithSize:5 aliveCells:alive];
    CWWorld *next = [original evolve];
    OTAssertTrue([next isEqual:original]);
}

- (void)testBlinker {
    OFArray *alive = [CWWorld blinkerAt:OFMakePoint(0, 1)];
    CWWorld *original = [[CWWorld alloc] initWithSize:3 aliveCells:alive];
    CWWorld *gen1 = [original evolve];
    OFArray *expectedAlive = @[@(OFMakePoint(1, 0)), @(OFMakePoint(1, 1)), @(OFMakePoint(1, 2))];
    CWWorld *expected = [[CWWorld alloc] initWithSize:3 aliveCells:expectedAlive];
    OTAssertTrue([expected isEqual:gen1]);
    CWWorld *gen2 = [gen1 evolve];
    OTAssertTrue([original isEqual:gen2]);
}

@end
