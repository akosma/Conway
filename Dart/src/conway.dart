import 'dart:io';
import 'coord.dart';
import 'world.dart';

void clrscr() {
  print("\x1B[2J\x1B[0;0H");
}

void main() {
  var alive_cells = World.blinker(new Coord(0, 1));
  alive_cells.addAll(World.beacon(new Coord(10, 10)));
  alive_cells.addAll(World.glider(new Coord(4, 5)));
  alive_cells.addAll(World.block(new Coord(1, 10)));
  alive_cells.addAll(World.block(new Coord(18, 3)));
  alive_cells.addAll(World.tub(new Coord(6, 1)));
  var world = new World(30, alive_cells);
  var generation = 0;
  clrscr();
  while (true) {
    generation += 1;
    print("$world");
    print("Generation $generation");
    world = world.evolve();
    sleep(Duration(milliseconds: 500));
    clrscr();
  }
}
