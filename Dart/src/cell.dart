enum Cell { alive, dead }

extension ToString on Cell {
  String get string {
    if (this == Cell.alive) {
      return " x |";
    }
    return "   |";
  }
}
