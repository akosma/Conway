class Coord {
  int _x = 0;
  int _y = 0;

  Coord(int x, int y) {
    _x = x;
    _y = y;
  }

  int get x => _x;
  int get y => _y;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }

    if (other.runtimeType != Coord) {
      return false;
    }

    return _x == (other as Coord).x && _y == (other as Coord).y;
  }

  @override
  String toString() => "$x:$y";

  @override
  int get hashCode => toString().hashCode;
}
