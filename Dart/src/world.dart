import 'dart:collection';
import 'coord.dart';
import 'cell.dart';

class World {
  Map<Coord, Cell> _cells = HashMap();
  int _size = 0;

  World(int size, List<Coord> aliveCells) {
    _size = size;
    var range = List.generate(_size, (index) => index);
    for (var a in range) {
      for (var b in range) {
        var coord = new Coord(a, b);
        if (aliveCells.contains(coord)) {
          _cells[coord] = Cell.alive;
        } else {
          _cells[coord] = Cell.dead;
        }
      }
    }
  }

  World evolve() {
    List<Coord> alive_cells = [];
    var range = List.generate(3, (index) => index - 1);
    _cells.forEach((coord, cell) {
      var count = 0;
      for (var a in range) {
        for (var b in range) {
          var current_coord = new Coord(coord.x + a, coord.y + b);
          if (current_coord != coord &&
              _cells.keys.contains(current_coord) &&
              _cells[current_coord] == Cell.alive) {
            count += 1;
          }
        }
      }

      switch (cell) {
        case Cell.alive:
          if (count == 2 || count == 3) {
            alive_cells.add(coord);
          }
          break;

        case Cell.dead:
          if (count == 3) {
            alive_cells.add(coord);
          }
      }
    });
    return new World(_size, alive_cells);
  }

  int get size => _size;
  Map<Coord, Cell> get cells => _cells;

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }

    if (other.runtimeType != World) {
      return false;
    }

    _cells.forEach((coord, cell) {
      if (!(other as World).cells.keys.contains(coord)) {
        return false;
      }
      if ((other as World).cells[coord] != _cells[coord]) {
        return false;
      }
    });
    return _size == (other as World).size;
  }

  @override
  String toString() {
    var str = "";
    var range = List.generate(_size, (index) => index);
    for (var a in range) {
      // First line with coordinates
      if (a == 0) {
        str += "    ";
        for (var b in range) {
          str += b.toString().padLeft(3) + "|";
        }
        str += "\n";
      }
      str += a.toString().padLeft(3) + "|";
      for (var b in range) {
        var c = new Coord(b, a);
        var cell = _cells[c];
        str += cell.string;
      }
      str += "\n";
    }
    return str;
  }

  static List<Coord> blinker(Coord c) {
    return [
      new Coord(c.x, c.y),
      new Coord(c.x + 1, c.y),
      new Coord(c.x + 2, c.y)
    ];
  }

  static List<Coord> beacon(Coord c) {
    return [
      new Coord(c.x, c.y),
      new Coord(c.x + 1, c.y),
      new Coord(c.x, c.y + 1),
      new Coord(c.x + 1, c.y + 1),
      new Coord(c.x + 2, c.y + 2),
      new Coord(c.x + 3, c.y + 2),
      new Coord(c.x + 2, c.y + 3),
      new Coord(c.x + 3, c.y + 3)
    ];
  }

  static List<Coord> glider(Coord c) {
    return [
      new Coord(c.x + 2, c.y + 2),
      new Coord(c.x + 1, c.y + 2),
      new Coord(c.x, c.y + 2),
      new Coord(c.x + 2, c.y + 1),
      new Coord(c.x + 1, c.y)
    ];
  }

  static List<Coord> block(Coord c) {
    return [
      new Coord(c.x, c.y),
      new Coord(c.x + 1, c.y),
      new Coord(c.x, c.y + 1),
      new Coord(c.x + 1, c.y + 1)
    ];
  }

  static List<Coord> tub(Coord c) {
    return [
      new Coord(c.x + 1, c.y),
      new Coord(c.x, c.y + 1),
      new Coord(c.x + 2, c.y + 1),
      new Coord(c.x + 1, c.y + 2)
    ];
  }
}
