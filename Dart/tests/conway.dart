import 'package:test/test.dart';
import '../src/world.dart';
import '../src/coord.dart';

void main() {
  // Define the test
  test("Block", () {
    var alive = World.block(new Coord(0, 0));
    var original = new World(5, alive);
    var next = original.evolve();
    expect(next, original);
  });

  test("Tub", () {
    var alive = World.tub(new Coord(0, 0));
    var original = new World(5, alive);
    var next = original.evolve();
    expect(next, original);
  });

  test("Blinker", () {
    var alive = World.blinker(new Coord(0, 1));
    var original = new World(3, alive);
    var gen1 = original.evolve();
    var expected_alive = [new Coord(1, 0), new Coord(1, 1), new Coord(1, 2)];
    var expected = new World(3, expected_alive);
    expect(expected, gen1);
    var gen2 = gen1.evolve();
    expect(original, gen2);
  });
}
