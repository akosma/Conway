const std = @import("std");
const Cell = @import("cell.zig").Cell;
const Coord = @import("coord.zig").Coord;

pub const World = struct {
    cells: std.AutoHashMap(Coord, Cell),
    size: u8,
    allocator: *const std.mem.Allocator,

    pub fn create(size: u8, coords: std.ArrayList(Coord), allocator: *const std.mem.Allocator) !World {
        var cells = std.AutoHashMap(Coord, Cell).init(allocator.*);
        const list = try createList(size, allocator);
        defer list.deinit();
        for (list.items) |a| {
            for (list.items) |b| {
                const coord = Coord{ .x = a, .y = b };
                if (contains(coords, coord)) {
                    try cells.put(coord, Cell.alive);
                } else {
                    try cells.put(coord, Cell.dead);
                }
            }
        }
        return World{
            .cells = cells,
            .size = size,
            .allocator = allocator,
        };
    }

    pub fn deinit(self: *World) void {
        self.cells.deinit();
    }

    fn createList(size: u8, allocator: *const std.mem.Allocator) !std.ArrayList(u8) {
        var list = std.ArrayList(u8).init(allocator.*);
        var counter: u8 = 0;
        while (counter < size) {
            try list.append(counter);
            counter += 1;
        }
        return list;
    }

    fn contains(coords: std.ArrayList(Coord), coord: Coord) bool {
        for (coords.items) |c| {
            if (c.x == coord.x and c.y == coord.y) {
                return true;
            }
        }
        return false;
    }

    pub fn format(self: World, writer: anytype) !void {
        const list = try createList(self.size, self.allocator);
        defer list.deinit();
        try writer.print("\n", .{});
        for (list.items) |a| {
            if (a == 0) {
                // First line with coordinates
                try writer.print("     ", .{});
                for (list.items) |b| {
                    try writer.print("{d:3}|", .{b});
                }
                try writer.print("\n", .{});
            }
            try writer.print("{d:3}| ", .{a});
            for (list.items) |b| {
                const coord = Coord{ .x = b, .y = a };
                const value: Cell = self.cells.get(coord) orelse Cell.dead;
                try writer.print("{s}", .{value.format()});
            }
            try writer.print("\n", .{});
        }
        try writer.print("\n", .{});
    }

    pub fn equals(self: World, other: World) bool {
        if (self.size != other.size) {
            return false;
        }
        var iterator = self.cells.iterator();
        while (iterator.next()) |entry| {
            const key: Coord = entry.key_ptr.*;
            const value: Cell = entry.value_ptr.*;
            const other_value: Cell = other.cells.get(key) orelse Cell.dead;
            if (value != other_value) {
                return false;
            }
        }
        return true;
    }

    pub fn evolve(self: World) !World {
        var list = std.ArrayList(Coord).init(self.allocator.*);
        defer list.deinit();
        var iterator = self.cells.iterator();
        const range = [3]i8{ -1, 0, 1 };

        while (iterator.next()) |entry| {
            const key: Coord = entry.key_ptr.*;
            const value: Cell = entry.value_ptr.*;
            var count: i8 = 0;
            for (range) |a| {
                for (range) |b| {
                    const current_coord = Coord{ .x = key.x + a, .y = key.y + b };
                    const current_cell = self.cells.get(current_coord) orelse Cell.dead;
                    if (!(current_coord.x == key.x and current_coord.y == key.y) and current_cell == Cell.alive) {
                        count += 1;
                    }
                }
            }

            if (value == Cell.alive) {
                if (count == 2 or count == 3) {
                    try list.append(key);
                }
            } else {
                if (count == 3) {
                    try list.append(key);
                }
            }
        }
        const new_world = World.create(self.size, list, self.allocator);
        return new_world;
    }

    pub fn blinker(coord: Coord, allocator: *const std.mem.Allocator) !std.ArrayList(Coord) {
        var list = std.ArrayList(Coord).init(allocator.*);
        try list.append(Coord{ .x = coord.x, .y = coord.y });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y });
        try list.append(Coord{ .x = coord.x + 2, .y = coord.y });
        return list;
    }

    pub fn beacon(coord: Coord, allocator: *const std.mem.Allocator) !std.ArrayList(Coord) {
        var list = std.ArrayList(Coord).init(allocator.*);
        try list.append(Coord{ .x = coord.x, .y = coord.y });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y });
        try list.append(Coord{ .x = coord.x, .y = coord.y + 1 });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y + 1 });
        try list.append(Coord{ .x = coord.x + 2, .y = coord.y + 2 });
        try list.append(Coord{ .x = coord.x + 3, .y = coord.y + 2 });
        try list.append(Coord{ .x = coord.x + 2, .y = coord.y + 3 });
        try list.append(Coord{ .x = coord.x + 3, .y = coord.y + 3 });
        return list;
    }

    pub fn glider(coord: Coord, allocator: *const std.mem.Allocator) !std.ArrayList(Coord) {
        var list = std.ArrayList(Coord).init(allocator.*);
        try list.append(Coord{ .x = coord.x + 2, .y = coord.y + 2 });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y + 2 });
        try list.append(Coord{ .x = coord.x, .y = coord.y + 2 });
        try list.append(Coord{ .x = coord.x + 2, .y = coord.y + 1 });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y });
        return list;
    }

    pub fn block(coord: Coord, allocator: *const std.mem.Allocator) !std.ArrayList(Coord) {
        var list = std.ArrayList(Coord).init(allocator.*);
        try list.append(Coord{ .x = coord.x, .y = coord.y });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y });
        try list.append(Coord{ .x = coord.x, .y = coord.y + 1 });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y + 1 });
        return list;
    }

    pub fn tub(coord: Coord, allocator: *const std.mem.Allocator) !std.ArrayList(Coord) {
        var list = std.ArrayList(Coord).init(allocator.*);
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y });
        try list.append(Coord{ .x = coord.x, .y = coord.y + 1 });
        try list.append(Coord{ .x = coord.x + 2, .y = coord.y + 1 });
        try list.append(Coord{ .x = coord.x + 1, .y = coord.y + 2 });
        return list;
    }
};
