pub const Cell = enum {
    alive,
    dead,

    pub fn format(self: Cell) *const [4:0]u8 {
        return switch (self) {
            .alive => " x |",
            .dead => "   |",
        };
    }
};
