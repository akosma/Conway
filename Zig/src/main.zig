const std = @import("std");
const Coord = @import("coord.zig").Coord;
const World = @import("world.zig").World;

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const allocator = gpa.allocator();

fn clrscr() void {
    std.debug.print("\x1B[2J\x1B[0;0H", .{});
}

fn sigintHandler(sig: c_int) callconv(.C) void {
    _ = sig;
    clrscr();
    std.process.exit(0);
}

pub fn main() !void {
    const act = std.os.linux.Sigaction{
        .handler = .{ .handler = sigintHandler },
        .mask = std.os.linux.empty_sigset,
        .flags = 0,
    };

    if (std.os.linux.sigaction(std.os.linux.SIG.INT, &act, null) != 0) {
        return error.SignalHandlerError;
    }

    var alive = try World.blinker(Coord{ .x = 0, .y = 1 }, &allocator);
    defer alive.deinit();
    const beacon = try World.beacon(Coord{ .x = 10, .y = 10 }, &allocator);
    defer beacon.deinit();
    try alive.appendSlice(beacon.items);

    const glider = try World.glider(Coord{ .x = 4, .y = 5 }, &allocator);
    defer glider.deinit();
    try alive.appendSlice(glider.items);

    const block = try World.block(Coord{ .x = 1, .y = 10 }, &allocator);
    defer block.deinit();
    try alive.appendSlice(block.items);

    const block2 = try World.block(Coord{ .x = 18, .y = 3 }, &allocator);
    defer block2.deinit();
    try alive.appendSlice(block2.items);

    const tub = try World.tub(Coord{ .x = 6, .y = 1 }, &allocator);
    defer tub.deinit();
    try alive.appendSlice(tub.items);

    var world = try World.create(30, alive, &allocator);
    var generation: u16 = 0;

    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const writer = bw.writer();

    while (generation < 1000) {
        clrscr();
        generation += 1;
        try world.format(writer);
        try bw.flush();
        std.debug.print("Generation {d}", .{generation});
        std.time.sleep(500000000);
        const new_world = try World.evolve(world);
        world.deinit();
        world = new_world;
    }
    clrscr();
    world.deinit();
}
