const std = @import("std");

pub const Coord = struct {
    x: i32 = 0,
    y: i32 = 0,

    pub fn format(self: *const Coord, comptime fmt: []const u8, options: std.fmt.FormatOptions, writer: anytype) !void {
        _ = fmt;
        _ = options;
        try writer.print("{d}:{d}", .{ self.x, self.y });
    }
};
