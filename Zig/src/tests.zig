const std = @import("std");
const Coord = @import("coord.zig").Coord;
const World = @import("world.zig").World;
const allocator = std.testing.allocator;

test "blinker" {
    var alive = try World.blinker(Coord{ .x = 0, .y = 1 }, &allocator);
    var original = try World.create(3, alive, &allocator);
    alive.deinit();
    var gen1 = try original.evolve();
    var expectedAlive = std.ArrayList(Coord).init(allocator);
    try expectedAlive.append(Coord{ .x = 1, .y = 0 });
    try expectedAlive.append(Coord{ .x = 1, .y = 1 });
    try expectedAlive.append(Coord{ .x = 1, .y = 2 });
    var expected = try World.create(3, expectedAlive, &allocator);
    expectedAlive.deinit();
    try std.testing.expect(expected.equals(gen1));
    var gen2 = try gen1.evolve();
    try std.testing.expect(original.equals(gen2));
    expected.deinit();
    original.deinit();
    gen1.deinit();
    gen2.deinit();
}

test "block" {
    var alive = try World.block(Coord{ .x = 0, .y = 0 }, &allocator);
    var original = try World.create(5, alive, &allocator);
    alive.deinit();
    var next = try original.evolve();
    try std.testing.expect(original.equals(next));
    original.deinit();
    next.deinit();
}

test "tub" {
    var alive = try World.tub(Coord{ .x = 0, .y = 0 }, &allocator);
    var original = try World.create(5, alive, &allocator);
    alive.deinit();
    var next = try original.evolve();
    try std.testing.expect(original.equals(next));
    original.deinit();
    next.deinit();
}
