#!/usr/bin/perl -I lib

use Conway::Cell;
use Conway::Coord;
use Conway::World;
use strict;
use warnings;
use Test::More qw(no_plan);

BEGIN { use_ok('Conway::Cell') };
BEGIN { use_ok('Conway::Coord') };
BEGIN { use_ok('Conway::World') };

sub test_block {
    my @alive = ();
    push(@alive, Conway::World::block(new Conway::Coord(0, 0)));
    my $original = new Conway::World(5, \@alive);
    my $next = $original->evolve;
    is($original->to_string, $next->to_string);
}

sub test_tub {
    my @alive = ();
    push(@alive, Conway::World::tub(new Conway::Coord(0, 0)));
    my $original = new Conway::World(5, \@alive);
    my $next = $original->evolve;
    is($original->to_string, $next->to_string);
}

sub test_blinker {
    my @alive = ();
    push(@alive, Conway::World::blinker(new Conway::Coord(0, 1)));
    my $original = new Conway::World(3, \@alive);
    my $gen1 = $original->evolve;
    my @expected_alive = (
        (new Conway::Coord(1, 0))->to_string,
        (new Conway::Coord(1, 1))->to_string,
        (new Conway::Coord(1, 2))->to_string
    );
    my $expected = new Conway::World(3, \@expected_alive);
    is($expected->to_string, $gen1->to_string);
    my $gen2 = $gen1->evolve;
    is($original->to_string, $gen2->to_string);
}

test_block;
test_tub;
test_blinker;
