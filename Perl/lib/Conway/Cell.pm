package Conway::Cell;
use strict;

use constant {
    ALIVE => 1,
    DEAD  => 0
};

sub to_string {
    my( $cell ) = @_;
    if ($cell eq DEAD) {
        return "   |";
    }
    return " x |";
}

1;
