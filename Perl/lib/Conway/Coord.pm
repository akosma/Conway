package Conway::Coord;
use strict;

sub new {
   my $class = shift;
   my $self = {
      _x => shift,
      _y  => shift,
   };
   bless $self, $class;
   return $self;
}

sub x {
    my ( $self ) = @_;
    return $self->{_x};
}

sub y {
    my ( $self ) = @_;
    return $self->{_y};
}

sub to_string {
    my( $self ) = @_;
    return sprintf($self->{_x} . ":" . $self->{_y});
}

sub from_string {
    my( $coord ) = @_;
    my @arr = split(':', $coord);
    my $a = int($arr[0]);
    my $b = int($arr[1]);
    return new Conway::Coord($a, $b);
}

1;
