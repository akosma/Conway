package Conway::World;

use Conway::Cell;
use Conway::Coord;
use strict;
use experimental 'smartmatch';

sub new {
    my $class = shift;
    my $self = {
        _size => shift,
        _aliveCells  => shift,
        _cells => ()
    };
    foreach $a (0 .. $self->{_size} - 1) {
        foreach $b (0 .. $self->{_size} - 1) {
            my $coord = $a . ":" . $b;
            if ($coord ~~ $self->{_aliveCells}) {
                $self->{_cells}{$coord} = Conway::Cell::ALIVE;
            }
            else {
                $self->{_cells}{$coord} = Conway::Cell::DEAD;
            }
        }
    }
    bless $self, $class;
    return $self;
}

sub evolve {
    my( $self ) = @_;
    my @alive = ();
    while ((my $coord_str, my $cell) = each %{ $self->{_cells} }) {
        my $count = 0;
        my $coord = Conway::Coord::from_string($coord_str);
        foreach $a (-1 .. 1) {
            foreach $b (-1 .. 1) {
                my $current_coord = Conway::Coord::from_string(($coord->x + $a) . ":" . ($coord->y + $b))->to_string;
                if ($current_coord ne $coord_str and exists($self->{_cells}{$current_coord}) and $self->{_cells}{$current_coord} eq Conway::Cell::ALIVE) {
                    $count += 1;
                }
            }
        }
        if ($cell eq Conway::Cell::ALIVE) {
            if ($count eq 2 or $count eq 3) {
                push @alive, $coord_str;
            }
        }
        else {
            if ($count eq 3) {
                push @alive, $coord_str;
            }
        }
    }
    return new Conway::World($self->{_size}, \@alive);
}

sub to_string {
    my( $self ) = @_;
    my $ret = "\n";
    foreach $a (0 .. $self->{_size} - 1) {
        # First line with coordinates
        if ($a eq 0) {
            $ret .= "    ";
            foreach $b (0 .. $self->{_size} - 1) {
                $ret .= sprintf("%3s|", $b);
            }
            $ret .= "\n";
        }
        $ret .= sprintf("%3s|", $a);
        foreach $b (0 .. $self->{_size} - 1) {
            my $c = new Conway::Coord($b, $a);
            my $cell = $self->{_cells}{$c->to_string};
            $ret .= Conway::Cell::to_string($cell);
        }
        $ret .= "\n";
    }
    return $ret;
}

sub blinker {
    my( $coord ) = @_;
    return [
        (new Conway::Coord($coord->x, $coord->y))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y))->to_string,
        (new Conway::Coord($coord->x + 2, $coord->y))->to_string
    ];
}

sub beacon {
    my( $coord ) = @_;
    return [
        (new Conway::Coord($coord->x, $coord->y))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y))->to_string,
        (new Conway::Coord($coord->x, $coord->y + 1))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y + 1))->to_string,
        (new Conway::Coord($coord->x + 2, $coord->y + 2))->to_string,
        (new Conway::Coord($coord->x + 3, $coord->y + 2))->to_string,
        (new Conway::Coord($coord->x + 2, $coord->y + 3))->to_string,
        (new Conway::Coord($coord->x + 3, $coord->y + 3))->to_string
    ];
}

sub glider {
    my( $coord ) = @_;
    return [
        (new Conway::Coord($coord->x + 2, $coord->y + 2))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y + 2))->to_string,
        (new Conway::Coord($coord->x, $coord->y + 2))->to_string,
        (new Conway::Coord($coord->x + 2, $coord->y + 1))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y))->to_string
    ];
}

sub block {
    my( $coord ) = @_;
    return [
        (new Conway::Coord($coord->x, $coord->y))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y))->to_string,
        (new Conway::Coord($coord->x, $coord->y + 1))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y + 1))->to_string
    ];
}

sub tub {
    my( $coord ) = @_;
    return [
        (new Conway::Coord($coord->x + 1, $coord->y))->to_string,
        (new Conway::Coord($coord->x, $coord->y + 1))->to_string,
        (new Conway::Coord($coord->x + 2, $coord->y + 1))->to_string,
        (new Conway::Coord($coord->x + 1, $coord->y + 2))->to_string
    ];
}

1;
