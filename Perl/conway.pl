#!/usr/bin/perl -I lib

use Conway::Cell;
use Conway::Coord;
use Conway::World;
use strict;

# Courtesy of
# https://stackoverflow.com/a/251441
sub clrscr {
print "\033[2J";   #clear the screen
print "\033[0;0H"; #jump to 0,0
}

$SIG{INT} = sub {
    clrscr;
    exit 0;
};

my @alive = ();
push(@alive, Conway::World::blinker(new Conway::Coord(0, 1)));
push(@alive, Conway::World::beacon(new Conway::Coord(10, 10)));
push(@alive, Conway::World::glider(new Conway::Coord(4, 5)));
push(@alive, Conway::World::block(new Conway::Coord(1, 10)));
push(@alive, Conway::World::block(new Conway::Coord(18, 3)));
push(@alive, Conway::World::tub(new Conway::Coord(6, 1)));
my $world = new Conway::World(30, \@alive);
my $generation = 0;
while (1) {
    clrscr;
    $generation += 1;
    print($world->to_string, "\n");
    printf("Generation %d\n", $generation);
    select(undef, undef, undef, 0.5);
    $world = $world->evolve;
}
