#!/usr/bin/regina

/* Initialization */
size = 29
separator = "|"
generation = 0
do i = 0 to size
	do j = 0 to size
		world.i.j = 0
	end
end

signal on halt

call Blinker 0, 1
call Beacon 10, 10
call Glider 4, 5
call Block 1, 10
call Block 18, 3
call Tub 6, 1

/* Print grid in an endless loop */
do forever
    'clear'
    say ""
    do a = 0 to size
        if a = 0 then call FirstLine
        call charout ,format(a, 3)
        call charout ,separator
        do b = 0 to size
            if world.b.a = 1 then call charout ," x |"
            else call charout ,"   |"
        end
        say ""
    end
    generation = generation + 1
    say ""
    say "Generation " generation
	call Evolve
    sleep(0.5)
end

halt:
	'clear'
	return

/* First line with coordinates */
FirstLine:
    do b = 0 to size
        if b = 0 then call charout ,"    "
        call charout ,format(b, 3)
        call charout ,separator
    end
    say ""
    return

/* Evolve world to next generation */
Evolve:
	do i = 0 to size
		do j = 0 to size
			copy.i.j = world.i.j
			world.i.j = 0
		end
	end
	do x = 0 to size
		do y = 0 to size
			counter = 0
			do a = x - 1 to x + 1
				do b = y - 1 to y + 1
					if a >= 0 & b >= 0 & a <= size & b <= size then counter = counter + copy.a.b
				end
			end
			counter = counter - copy.x.y
			select
			when copy.x.y = 1 then
				if counter = 2 | counter = 3 then world.x.y = 1
			otherwise
				if counter = 3 then world.x.y = 1
			end
		end
	end
	return

Blinker:
	arg x, y
	a = x + 1
	b = x + 2
	world.x.y = 1
	world.a.y = 1
	world.b.y = 1
	return

Beacon:
	arg x, y
	a = x + 1
	b = y + 1
	c = x + 2
	d = y + 2
	e = x + 3
	f = y + 3
	world.x.y = 1
	world.a.y = 1
	world.x.b = 1
	world.a.b = 1
	world.c.d = 1
	world.e.d = 1
	world.c.f = 1
	world.e.f = 1
	return

Glider:
	arg x, y
	a = x + 1
	b = y + 1
	c = x + 2
	d = y + 2
	world.c.d = 1
	world.a.d = 1
	world.x.d = 1
	world.c.b = 1
	world.a.y = 1
	return

Block:
	arg x, y
	a = x + 1
	b = y + 1
	world.x.y = 1
	world.a.y = 1
	world.x.b = 1
	world.a.b = 1
	return

Tub:
	arg x, y
	a = x + 1
	b = y + 1
	c = x + 2
	d = y + 2
	world.a.y = 1
	world.x.b = 1
	world.c.b = 1
	world.a.d = 1
	return
