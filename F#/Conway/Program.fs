﻿open System
open System.Threading
open ConwayLib
open Coord
open World

[<EntryPoint>]
let main argv =
    Console.CancelKeyPress.Add (fun arg ->
        Console.Clear()
        Environment.Exit(0))

    let blinker = blinker { X = 0; Y = 1 }
    let beacon = beacon { X = 10; Y = 10 }
    let glider = glider { X = 4; Y = 5 }
    let block1 = block { X = 1; Y = 10 }
    let block2 = block { X = 18; Y = 3 }
    let tub = tub { X = 6; Y = 1 }
    let aliveCells = blinker @ beacon @ glider @ block1 @ block2 @ tub

    let rec iterate world generation =
        Console.Clear()
        printfn "%s" (worldDescription world)
        printfn "Generation %d" (generation)
        Thread.Sleep(500)
        iterate (evolve world) (generation + 1)

    iterate (createWorld 30 aliveCells) 1

    // return an integer exit code
    0
