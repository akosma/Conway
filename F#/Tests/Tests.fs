module Tests

open ConwayLib
open Xunit
open World
open Coord

[<Fact>]
let ``Blinker`` () =
    let alive = blinker { X = 0; Y = 1 }
    let original = createWorld 3 alive
    let gen1 = evolve original
    let expectedAlive = [ { X = 1; Y = 0 }
                          { X = 1; Y = 1 }
                          { X = 1; Y = 2 } ]
    let expected = createWorld 3 expectedAlive
    Assert.Equal(expected, gen1)
    let gen2 = evolve gen1
    Assert.Equal(original, gen2)

[<Fact>]
let ``Block`` () =
    let alive = block { X = 0; Y = 0 }
    let original = createWorld 5 alive
    let next = evolve original
    Assert.Equal(next, original)

[<Fact>]
let ``Tub`` () =
    let alive = tub { X = 0; Y = 0 }
    let original = createWorld 5 alive
    let next = evolve original
    Assert.Equal(next, original)
