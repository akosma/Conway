namespace ConwayLib

open System.Text
open ConwayLib.Cell
open ConwayLib.Coord

module World =
    type World = {
        Size: int;
        Cells: Map<Coord, Cell>
    }

    let createWorld size aliveCells =
        // Auxiliary functions
        let exists c = List.contains c aliveCells
        let alive b = if b then Cell.Alive else Cell.Dead

        // Create a list of coordinates
        // https://stackoverflow.com/a/1335793/133764
        let keys = [for a in 0..size-1 do
                    for b in 0..size-1 -> { X = a; Y = b } ]
        // Map the list of coordinates into a list of "Alive" or "Dead" values
        let values = List.map (exists >> alive) keys
        // Merge both lists into a map
        let cells = Seq.zip keys values |> List.ofSeq |> Map.ofList
        // Return the new world
        { Size = size; Cells = cells }

    let evolve world =
        // Auxiliary functions
        let oneIfAlive currentCoord =
            if world.Cells.ContainsKey(currentCoord) && world.Cells[currentCoord] = Cell.Alive then 1 else 0
        let countAliveNeighbors c =
            [for a in -1..1 do
             for b in -1..1 do
             yield { X = c.X + a; Y = c.Y + b }]
             |> List.filter (fun o -> o <> c) |> List.sumBy oneIfAlive

        // Build a list of coordinates
        let coords = [for a in 0..world.Size-1 do
                      for b in 0..world.Size-1 -> { X = a; Y = b } ]
        // Map the coordinates to build a map of coordinates to their number of living neighbors
        let counters = coords |> List.map countAliveNeighbors |> List.zip coords |> Map.ofList
        // Function returning a Coord option for the next generation
        let decide c =
            let cell = world.Cells[c]
            let count = counters[c]
            match cell with
            | Cell.Alive -> if count = 2 || count = 3 then Some(c) else None
            | _ -> if count = 3 then Some(c) else None
        // Map the coordinates into a list of Coord with living cells in the next generation
        // https://stackoverflow.com/a/3548556/133764
        let aliveCells = coords |> List.map decide |> List.choose id
        createWorld world.Size aliveCells

    let worldDescription world =
        let size = world.Size - 1
        // First line with coordinates
        let s = StringBuilder("\n    ")
        let top = [0..size] |> List.map (sprintf "%3d|") |> String.concat ""
        s.AppendFormat("{0}\n", top) |> ignore

        // Argh, maybe we can do better than this?
        for a in [0..size] do
            s.AppendFormat("{0,3}|", a) |> ignore
            let line = [for b in [0..size] do
                        yield world.Cells[{ X = b; Y = a }]] |> List.map cellDescription |> String.concat ""
            s.AppendFormat("{0}\n", line) |> ignore
        s.ToString()

    let blinker coord =
        [ { X = coord.X;     Y = coord.Y }
          { X = coord.X + 1; Y = coord.Y }
          { X = coord.X + 2; Y = coord.Y } ]

    let beacon coord =
        [ { X = coord.X;     Y = coord.Y }
          { X = coord.X + 1; Y = coord.Y }
          { X = coord.X;     Y = coord.Y + 1 }
          { X = coord.X + 1; Y = coord.Y + 1 }
          { X = coord.X + 2; Y = coord.Y + 2 }
          { X = coord.X + 3; Y = coord.Y + 2 }
          { X = coord.X + 2; Y = coord.Y + 3 }
          { X = coord.X + 3; Y = coord.Y + 3 } ]

    let glider coord =
        [ { X = coord.X + 2; Y = coord.Y + 2 }
          { X = coord.X + 1; Y = coord.Y + 2 }
          { X = coord.X;     Y = coord.Y + 2 }
          { X = coord.X + 2; Y = coord.Y + 1 }
          { X = coord.X + 1; Y = coord.Y } ]

    let block coord =
        [ { X = coord.X;     Y = coord.Y }
          { X = coord.X + 1; Y = coord.Y }
          { X = coord.X;     Y = coord.Y + 1 }
          { X = coord.X + 1; Y = coord.Y + 1 } ]

    let tub coord =
        [ { X = coord.X + 1; Y = coord.Y }
          { X = coord.X;     Y = coord.Y + 1 }
          { X = coord.X + 2; Y = coord.Y + 1 }
          { X = coord.X + 1; Y = coord.Y + 2 } ]
