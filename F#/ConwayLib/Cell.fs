namespace ConwayLib

module Cell =
    type Cell = Dead=0 | Alive=1

    let cellDescription (c : Cell) =
        match c with
        | Cell.Dead -> "   |"
        | _ -> " x |"
