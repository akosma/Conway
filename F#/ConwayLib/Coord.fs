namespace ConwayLib

open System

module Coord =
    type Coord = { X: int; Y: int }

    let coordDescription c =
        sprintf "%d:%d" c.X c.Y

    let parseCoord (s: String) =
        let integerParts = ':' |> s.Split |> Array.map int
        { X = integerParts[0]; Y = integerParts[1] }
