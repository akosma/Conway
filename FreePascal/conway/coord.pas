Unit coord;

Interface

Uses SysUtils, strutils, types;

Type

  TCoord = Record
    x, y: integer;
    s: String;
  End;

Function TCoordCreate(a: integer; b: integer): TCoord; overload;
Function TCoordCreate(s: String): TCoord; overload;
Function TCoordEquals(a, b: TCoord): boolean;

Implementation

Function TCoordCreate(a: integer; b: integer): TCoord;
Var
  r: TCoord;
Begin
  r.x := a;
  r.y := b;
  r.s := format('%d:%d', [a, b]);
  result := r;
End;

Function TCoordCreate(s: String): TCoord;
Var
  elements: TStringDynArray;
  r: TCoord;
Begin
  elements := SplitString(s, ':');
  r.x := StrToInt(elements[0]);
  r.y := StrToInt(elements[1]);
  r.s := s;
  result := r;
End;

Function TCoordEquals(a, b: TCoord): boolean;
Begin
  result := (a.x = b.x) And (a.y = b.y);
End;

End.
