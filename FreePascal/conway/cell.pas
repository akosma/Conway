Unit cell;

Interface

Type
  TCell = (Alive, Dead);

Function TCellToString(cell: TCell): ansistring;

Implementation

Function TCellToString(cell: TCell): ansistring;
Begin
  If cell = Alive Then
    result := ' x |'
  Else
    result := '   |';
End;

End.
