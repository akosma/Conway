Unit world;

Interface

Uses SysUtils, Generics.Collections, coord, cell;

Type
  TWorld = Class(TObject)
    Private
      fsize: integer;
      fcells: TDictionary<String, TCell>;
    Public
      Constructor Create(s: integer; aliveCells: TList<String>);
      Destructor Destroy; override;
      Function ToString(): ansistring; override;
      Function Evolve(): TWorld;
      Function Equals(Obj: TObject): boolean; override;
      Property Size: integer read fsize;
      Property Cells: TDictionary<String, TCell> read fcells;
      Class Function Blinker(coord: TCoord): TList<String>; static;
      Class Function Beacon(coord: TCoord): TList<String>; static;
      Class Function Glider(coord: TCoord): TList<String>; static;
      Class Function Block(coord: TCoord): TList<String>; static;
      Class Function Tub(coord: TCoord): TList<String>; static;
End;

Implementation

Constructor TWorld.Create(s: integer; aliveCells: TList<String>);
Var
  a, b: integer;
  c: TCoord;
Begin
  fsize := s;
  fcells := TDictionary<String, TCell>.Create;

  For a := 0 To fsize - 1 Do
    For b := 0 To fsize - 1 Do
      Begin
        c := TCoordCreate(a, b);
        If aliveCells.Contains(c.s) Then
          fcells.Add(c.s, Alive)
        Else
          fcells.Add(c.s, Dead);
      End;
End;

Destructor TWorld.Destroy;
Begin
  FreeAndNil(fcells);
  inherited Destroy;
End;

Function TWorld.Equals(Obj: TObject): boolean;
Var
  w: TWorld;
  sizeEquals, contentsEquals: boolean;
  pair: TDictionary<String, TCell>.TDictionaryPair;
  key: String;
  value: TCell;
Begin
  w := Obj as TWorld;
  sizeEquals := (fsize = w.Size);
  contentsEquals := TRUE;
  For pair In fcells Do
    Begin
      key := pair.Key;
      value := pair.Value;
      contentsEquals := contentsEquals And w.Cells.ContainsKey(key) And (w.Cells[key] = value);
    End;
  result := sizeEquals And contentsEquals;
End;

Function TWorld.Evolve(): TWorld;
Var
  aliveCells: TList<String>;
  pair: TDictionary<String, TCell>.TDictionaryPair;
  count, a, b: integer;
  keyCoord, currentCoord: TCoord;
  coord, currentCoordStr: String;
  cell: TCell;
Begin
  aliveCells := TList<String>.Create();
  For pair In fcells Do
    Begin
      coord := pair.Key;
      cell := pair.Value;
      count := 0;
      For a := -1 To 1 Do
        For b := -1 To 1 Do
          Begin
            keyCoord := TCoordCreate(coord);
            currentCoord := TCoordCreate(keyCoord.x + a, keyCoord.y + b);
            currentCoordStr := currentCoord.s;
            If (Not TCoordEquals(currentCoord, keyCoord)) And (fcells.ContainsKey(currentCoordStr)) And (fcells[currentCoordStr] = Alive) Then
              count := count + 1;
          End;

      Case cell Of
        Alive:
               If (count = 2) Or (count = 3) Then
                 aliveCells.Add(coord);
        Dead:
              If (count = 3) Then
                aliveCells.Add(coord);
      End;
    End;

  result := TWorld.Create(fsize, aliveCells);
  aliveCells.Free;
End;

Function TWorld.ToString(): ansistring;
Var
  res, temp: ansistring;
  c: TCoord;
  a, b: integer;
Begin;
  res := '';
  For a := 0 To size - 1 Do
    Begin
      If a = 0 Then
        Begin
          res := Concat(res, '    ');
          For b := 0 To size - 1 Do
            res := Concat(res, Format('%3d|', [b]));
          res := Concat(res, sLineBreak);
        End;
      res := Concat(res, Format('%3d|', [a]));
      For b := 0 To size - 1 Do
        Begin
          c := TCoordCreate(b, a);
          temp := TCellToString(fcells[c.s]);
          res := Concat(res, temp);
        End;
      res := Concat(res, sLineBreak);
    End;

  result := res;
End;

Class Function TWorld.Blinker(coord: TCoord): TList<String>;
Var
  list: TList<String>;
Begin
  list := TList<String>.Create();
  list.Add(TCoordCreate(coord.x, coord.y).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y).s);
  list.Add(TCoordCreate(coord.x + 2, coord.y).s);
  result := list;
End;

Class Function TWorld.Beacon(coord: TCoord): TList<String>;
Var
  list: TList<String>;
Begin
  list := TList<String>.Create();
  list.Add(TCoordCreate(coord.x, coord.y).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y).s);
  list.Add(TCoordCreate(coord.x, coord.y + 1).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y + 1).s);
  list.Add(TCoordCreate(coord.x + 2, coord.y + 2).s);
  list.Add(TCoordCreate(coord.x + 3, coord.y + 2).s);
  list.Add(TCoordCreate(coord.x + 2, coord.y + 3).s);
  list.Add(TCoordCreate(coord.x + 3, coord.y + 3).s);
  result := list;
End;

Class Function TWorld.Glider(coord: TCoord): TList<String>;
Var
  list: TList<String>;
Begin
  list := TList<String>.Create();
  list.Add(TCoordCreate(coord.x + 2, coord.y + 2).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y + 2).s);
  list.Add(TCoordCreate(coord.x, coord.y + 2).s);
  list.Add(TCoordCreate(coord.x + 2, coord.y + 1).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y).s);
  result := list;
End;

Class Function TWorld.Block(coord: TCoord): TList<String>;
Var
  list: TList<String>;
Begin
  list := TList<String>.Create();
  list.Add(TCoordCreate(coord.x, coord.y).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y).s);
  list.Add(TCoordCreate(coord.x, coord.y + 1).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y + 1).s);
  result := list;
End;

Class Function TWorld.Tub(coord: TCoord): TList<String>;
Var
  list: TList<String>;
Begin
  list := TList<String>.Create();
  list.Add(TCoordCreate(coord.x + 1, coord.y).s);
  list.Add(TCoordCreate(coord.x, coord.y + 1).s);
  list.Add(TCoordCreate(coord.x + 2, coord.y + 1).s);
  list.Add(TCoordCreate(coord.x + 1, coord.y + 2).s);
  result := list;
End;

End.
