Program tests;

Uses
consoletestrunner, fpcunit, testregistry, world, coord, Generics.Collections;

Type
  TTests = Class(TTestCase)
    Protected
      Procedure SetUp; override;
      Procedure TearDown; override;
    Published
      Procedure Test_Block;
      Procedure Test_Tub;
      Procedure Test_Blinker;
End;

Procedure TTests.Test_Block;
Var
  alive: TList<String>;
  original, next: TWorld;
Begin
  alive := TWorld.Block(TCoordCreate(0, 0));
  original := TWorld.Create(5, alive);
  next := original.Evolve;
  AssertTrue('Original must be equal to next', original.Equals(next));
End;

Procedure TTests.Test_Tub;
Var
  alive: TList<String>;
  original, next: TWorld;
Begin
  alive := TWorld.Tub(TCoordCreate(0, 0));
  original := TWorld.Create(5, alive);
  next := original.Evolve;
  AssertTrue('Original must be equal to next', original.Equals(next));
End;

Procedure TTests.Test_Blinker;
Var
  alive, expectedAlive: TList<String>;
  original, gen1, expected, gen2: TWorld;
Begin
  alive := TWorld.Blinker(TCoordCreate(0, 1));
  original := TWorld.Create(3, alive);
  gen1 := original.Evolve;
  expectedAlive := TList<String>.Create;
  expectedAlive.Add('1:0');
  expectedAlive.Add('1:1');
  expectedAlive.Add('1:2');
  expected := TWorld.Create(3, expectedAlive);
  AssertTrue('Gen1 must be equal to expected', expected.Equals(gen1));
  gen2 := gen1.Evolve;
  AssertTrue('Original must be equal to gen2', original.Equals(gen2));
End;

Procedure TTests.SetUp;
Begin
End;

Procedure TTests.TearDown;
Begin
End;

Var
  Application: TTestRunner;

Begin
  RegisterTest(TTests);
  Application := TTestRunner.Create(Nil);
  Application.Initialize;
  Application.Run;
  Application.Free;
End.
