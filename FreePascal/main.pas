Program Conway;

Uses Crt, SysUtils, Generics.Collections, world, coord;

Var
  w1, w2 : TWorld;
  list, temp : TList<String>;
  ch: char;
  generation: integer;
Begin
  list := TWorld.Blinker(TCoordCreate(0, 1));

  temp := TWorld.Beacon(TCoordCreate(10, 10));
  list.AddRange(temp);
  FreeAndNil(temp);

  temp := TWorld.Glider(TCoordCreate(4, 5));
  list.AddRange(temp);
  FreeAndNil(temp);

  temp := TWorld.Block(TCoordCreate(1, 10));
  list.AddRange(temp);
  FreeAndNil(temp);

  temp := TWorld.Block(TCoordCreate(18, 3));
  list.AddRange(temp);
  FreeAndNil(temp);

  temp := TWorld.Tub(TCoordCreate(6, 1));
  list.AddRange(temp);
  FreeAndNil(temp);

  w1 := TWorld.Create(30, list);
  list.Free;
  generation := 0;
  Repeat
    Begin
      ClrScr;
      generation := generation + 1;
      WriteLn(w1.ToString);
      WriteLn(format('Generation %d', [generation]));
      w2 := w1.Evolve;
      FreeAndNil(w1);
      w1 := w2;
      Delay(500);
    End;
  Until KeyPressed;
  ch := ReadKey;
  FreeAndNil(w1);
  ClrScr;
End.
