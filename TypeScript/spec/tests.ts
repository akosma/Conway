import { World } from '../src/World.ts'
import { Coord } from '../src/Coord.ts'
import { assertEquals } from 'https://deno.land/std@0.152.0/testing/asserts.ts'

Deno.test('Block', () => {
  const alive = World.block(new Coord(0, 0))
  const original = new World(5, alive)
  const next = original.evolve()
  assertEquals(next, original)
})

Deno.test('Tub', () => {
  const alive = World.tub(new Coord(0, 0))
  const original = new World(5, alive)
  const next = original.evolve()
  assertEquals(next, original)
})

Deno.test('Blinker', () => {
  const alive = World.blinker(new Coord(0, 1))
  const original = new World(3, alive)
  const gen1 = original.evolve()
  const expectedAlive: Coord[] = [
    new Coord(1, 0),
    new Coord(1, 1),
    new Coord(1, 2),
  ]
  const expected = new World(3, expectedAlive)
  assertEquals(expected, gen1)
  const gen2 = gen1.evolve()
  assertEquals(original, gen2)
})
