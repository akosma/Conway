import { Cell } from './Cell.ts'
import { Coord } from './Coord.ts'

export class World {
  cells = new Map<string, Cell>()

  constructor(public size: number, aliveCells: Array<Coord>) {
    this.size = size
    const aliveCellStrings: string[] = aliveCells.map((item) => item.toString())
    for (let a = 0; a < size; ++a) {
      for (let b = 0; b < size; ++b) {
        const coord = new Coord(a, b)
        if (aliveCellStrings.includes(coord.toString())) {
          this.cells.set(coord.toString(), Cell.alive)
        } else {
          this.cells.set(coord.toString(), Cell.dead)
        }
      }
    }
  }

  evolve(): World {
    const alive: Coord[] = []
    const range = [-1, 0, 1]
    this.cells.forEach((cell: Cell, coordText: string) => {
      const coord = Coord.fromString(coordText)
      let count = 0

      range.map((a) => {
        count += range.map((b) => {
          return (new Coord(coord.x + a, coord.y + b)).toString()
        }).filter((c) => {
          return c !== coordText
        }).map((c) => {
          return this.cells.get(c)
        }).filter((o) => {
          return o === Cell.alive
        }).length
      })

      switch (cell) {
        case Cell.alive: {
          if (count === 2 || count === 3) {
            alive.push(coord)
          }
          break
        }
        case Cell.dead: {
          if (count === 3) {
            alive.push(coord)
          }
          break
        }
      }
    })
    return new World(this.size, alive)
  }

  toString(): string {
    let str = '\n'

    for (let a = 0; a < this.size; ++a) {
      if (a === 0) {
        // First line with coordinates
        str += '    '
        for (let b = 0; b < this.size; ++b) {
          str += b.toString().padStart(3) + '|'
        }
        str += '\n'
      }
      str += a.toString().padStart(3) + '|'
      for (let b = 0; b < this.size; ++b) {
        const coord = new Coord(b, a)
        const cell = this.cells.get(coord.toString())
        str += `${cell}`
      }
      str += '\n'
    }
    return str
  }

  static blinker(origin: Coord): Array<Coord> {
    return [
      new Coord(origin.x, origin.y),
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x + 2, origin.y),
    ]
  }

  static beacon(origin: Coord): Array<Coord> {
    return [
      new Coord(origin.x, origin.y),
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x, origin.y + 1),
      new Coord(origin.x + 1, origin.y + 1),
      new Coord(origin.x + 2, origin.y + 2),
      new Coord(origin.x + 3, origin.y + 2),
      new Coord(origin.x + 2, origin.y + 3),
      new Coord(origin.x + 3, origin.y + 3),
    ]
  }

  static glider(origin: Coord): Array<Coord> {
    return [
      new Coord(origin.x + 2, origin.y + 2),
      new Coord(origin.x + 1, origin.y + 2),
      new Coord(origin.x, origin.y + 2),
      new Coord(origin.x + 2, origin.y + 1),
      new Coord(origin.x + 1, origin.y),
    ]
  }

  static block(origin: Coord): Array<Coord> {
    return [
      new Coord(origin.x, origin.y),
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x, origin.y + 1),
      new Coord(origin.x + 1, origin.y + 1),
    ]
  }

  static tub(origin: Coord): Array<Coord> {
    return [
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x, origin.y + 1),
      new Coord(origin.x + 2, origin.y + 1),
      new Coord(origin.x + 1, origin.y + 2),
    ]
  }
}
