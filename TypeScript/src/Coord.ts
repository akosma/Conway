export class Coord {
  constructor(public x: number, public y: number) {}

  toString(): string {
    return `${this.x}:${this.y}`
  }

  static fromString(string: string): Coord {
    const parts: string[] = string.split(':')
    const c: number[] = parts.map(parseFloat)
    return new Coord(c[0], c[1])
  }
}
