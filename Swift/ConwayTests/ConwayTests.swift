//
//  ConwayTests.swift
//  ConwayTests
//
//  Created by Adrian Kosmaczewski on 20.10.17.
//  Copyright © 2017 Adrian Kosmaczewski. All rights reserved.
//

import XCTest
import Conway

class ConwayTests: XCTestCase {
    func testBlockIsStable() {
        let alive = World.block(at: Coord(0, 0))
        let original = World(size: 5, aliveCells: alive)
        let next = original.evolve()
        XCTAssertEqual(original, next)
    }
    
    func testTubIsStable() {
        let alive = World.tub(at: Coord(0, 0))
        let original = World(size: 5, aliveCells: alive)
        let next = original.evolve()
        XCTAssertEqual(original, next)
    }
    
    func testBlinkerPeriodIsTwo() {
        let alive = World.blinker(at: Coord(0, 1))
        let original = World(size: 3, aliveCells: alive)
        let gen1 = original.evolve()
        let expectedAlive = [
            Coord(1, 0),
            Coord(1, 1),
            Coord(1, 2)
        ]
        let expected = World(size: 3, aliveCells: expectedAlive)
        XCTAssertEqual(expected, gen1)
        let gen2 = gen1.evolve()
        XCTAssertEqual(original, gen2)
    }
}
