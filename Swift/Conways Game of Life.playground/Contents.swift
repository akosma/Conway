import Foundation

enum Cell {
    case 🐱
    case 💀
    
    var tableDescription: String {
        if self == .🐱 {
            return " 🐱  | "
        }
        return "     | "
    }
}

struct Coord: CustomStringConvertible, Hashable, Equatable {
    let x: Int
    let y: Int
    
    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
    
    var description: String {
        return "\(x):\(y)"
    }
    
    var hashValue: Int {
        return description.hashValue
    }
    
    static func ==(lhs: Coord, rhs: Coord) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
}


struct World: CustomStringConvertible, Equatable {
    private let size: Int
    private var cells = [Coord: Cell]()
    private let q = DispatchQueue.global(qos: .background)
    
    init(size: Int = 5, aliveCells: [Coord]? = nil) {
        self.size = size
        
        for a in 0..<size {
            for b in 0..<size {
                let coord = Coord(a, b)
                if let contains = aliveCells?.contains(coord) {
                    cells[coord] = contains ? .🐱 : .💀
                }
                else {
                    cells[coord] = .💀
                }
            }
        }
    }
    
    func evolve() -> World {
        var alive = [Coord]()
        
        q.sync {
            for (coord, cell) in cells {
                var count = 0
                for a in -1...1 {
                    for b in -1...1 {
                        let neighborCoord = Coord(coord.x + a, coord.y + b)
                        if neighborCoord != coord, let neighbor = cells[neighborCoord], neighbor == .🐱 {
                            count += 1
                        }
                    }
                }
                
                switch cell {
                case .🐱:
                    if count == 2 || count == 3 {
                        alive.append(coord)
                    }
                case .💀:
                    if count == 3 {
                        alive.append(coord)
                    }
                }
            }
        }
        
        return World(size: size, aliveCells: alive)
    }
    
    var description: String {
        var str = "\n"
        
        q.sync {
            for a in 0..<size {
                if a == 0 {
                    // First line with coordinates
                    str += "   "
                    for b in 0..<size {
                        str += "   \(b)   "
                    }
                    str += "\n"
                }
                str += " \(a) | "
                for b in 0..<size {
                    let coord = Coord(b, a)
                    if let cell = cells[coord] {
                        str += cell.tableDescription
                    }
                }
                str += "\n"
            }
        }
        
        return str
    }
    
    static func ==(lhs: World, rhs: World) -> Bool {
        return lhs.cells == rhs.cells
    }
}

extension World {
    static func blinker(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x, coord.y),
            Coord(coord.x + 1, coord.y),
            Coord(coord.x + 2, coord.y)
        ]
    }

    static func beacon(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x, coord.y),
            Coord(coord.x + 1, coord.y),
            Coord(coord.x, coord.y + 1),
            Coord(coord.x + 1, coord.y + 1),
            Coord(coord.x + 2, coord.y + 2),
            Coord(coord.x + 3, coord.y + 2),
            Coord(coord.x + 2, coord.y + 3),
            Coord(coord.x + 3, coord.y + 3),
        ]
    }

    static func glider(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x + 2, coord.y + 2),
            Coord(coord.x + 1, coord.y + 2),
            Coord(coord.x, coord.y + 2),
            Coord(coord.x + 2, coord.y + 1),
            Coord(coord.x + 1, coord.y),
        ]
    }

    static func block(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x, coord.y),
            Coord(coord.x + 1, coord.y),
            Coord(coord.x, coord.y + 1),
            Coord(coord.x + 1, coord.y + 1),
        ]
    }

    static func tub(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x + 1, coord.y),
            Coord(coord.x, coord.y + 1),
            Coord(coord.x + 2, coord.y + 1),
            Coord(coord.x + 1, coord.y + 2),
        ]
    }
}

//let pattern1 = World.blinker(at: Coord(7, 1))
//let pattern2 = World.glider(at: Coord(0, 0))
//var coords = [Coord](pattern1)
//coords.append(contentsOf: pattern2)
//var world = World(size: 10, aliveCells: coords)

let coords = World.block(at: Coord(0, 0))
var world = World(size: 5, aliveCells: coords)
print(world)

for _ in 0...10 {
    let next = world.evolve()
    print(next)
    print(next == world)
    world = next
}







