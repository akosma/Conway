import XCTest

import ConwayTests

var tests = [XCTestCaseEntry]()
tests += ConwayTests.allTests()
XCTMain(tests)
