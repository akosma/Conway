import Foundation
import ConwayLib

var alive = [Coord]()
alive.append(contentsOf: World.blinker(at: Coord(0, 1)))
alive.append(contentsOf: World.beacon(at: Coord(10, 10)))
alive.append(contentsOf: World.glider(at: Coord(4, 5)))
alive.append(contentsOf: World.block(at: Coord(1, 10)))
alive.append(contentsOf: World.block(at: Coord(18, 3)))
alive.append(contentsOf: World.tub(at: Coord(6, 1)))
var world : World = World(size: 30, aliveCells: alive)
var generation = 0

while(true) {
    print("\u{001B}[2J") // https://stackoverflow.com/a/36704070/133764
    generation += 1
    print(String(describing: world))
    print(String(format: "Generation %d", generation))
    world = world.evolve()
    usleep(500000)
}
