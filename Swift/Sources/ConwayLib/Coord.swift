//
//  Coord.swift
//  Conway
//
//  Created by Adrian Kosmaczewski on 19.10.17.
//  Copyright © 2017 Adrian Kosmaczewski. All rights reserved.
//

import Foundation

public struct Coord {
    let x: Int
    let y: Int

    public init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
}

extension Coord: CustomStringConvertible {
    public var description: String {
        return "\(x):\(y)"
    }
}

extension Coord: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(description)
    }
}

extension Coord: Equatable {
    public static func ==(lhs: Coord, rhs: Coord) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
}
