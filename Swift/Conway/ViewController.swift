//
//  ViewController.swift
//  Conway
//
//  Created by Adrian Kosmaczewski on 19.10.17.
//  Copyright © 2017 Adrian Kosmaczewski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var worldView: WorldView!
    var world: World? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var alive = [Coord]()
        alive.append(contentsOf: World.blinker(at: Coord(0, 1)))
        alive.append(contentsOf: World.beacon(at: Coord(10, 10)))
        alive.append(contentsOf: World.glider(at: Coord(4, 5)))
        alive.append(contentsOf: World.block(at: Coord(1, 10)))
        alive.append(contentsOf: World.block(at: Coord(18, 3)))
        alive.append(contentsOf: World.tub(at: Coord(6, 1)))
        world = World(size: 30, aliveCells: alive)
        worldView.world = world
        evolve()
    }
    
    func evolve() {
        let mainQueue = DispatchQueue.main
        let deadline = DispatchTime.now() + .milliseconds(500)
        mainQueue.asyncAfter(deadline: deadline) {
            let next = self.world?.evolve()
            self.world = next
            self.worldView.world = next
            self.evolve()
        }
    }
}
