//
//  Cell.swift
//  Conway
//
//  Created by Adrian Kosmaczewski on 19.10.17.
//  Copyright © 2017 Adrian Kosmaczewski. All rights reserved.
//

import Foundation

public enum Cell {
    case alive
    case dead
    
    var tableDescription: String {
        if self == .alive {
            return " x  | "
        }
        return "     | "
    }
}
