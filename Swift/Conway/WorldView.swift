//
//  WorldView.swift
//  Conway
//
//  Created by Adrian Kosmaczewski on 19.10.17.
//  Copyright © 2017 Adrian Kosmaczewski. All rights reserved.
//

import UIKit

class WorldView: UIView {
    var rectangles = [Coord: CGRect]()
    var paths = [UIBezierPath]()
    
    var world: World? = nil {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        if let w = world {
            let width = rect.size.width
            let height = rect.size.height
            let size = CGFloat(w.size)
            let horizontalStep = width / size
            let verticalStep = height / size
            let context = UIGraphicsGetCurrentContext()
            
            // Cells
            context?.setStrokeColor(UIColor.white.cgColor)
            context?.setLineWidth(5)
            if rectangles.count == 0 {
                createRectangles(horizontalStep, verticalStep)
            }
            for (coord, cell) in w.cells {
                if cell == .alive {
                    let rect = rectangles[coord]!
                    context?.fill(rect)
                    context?.stroke(rect)
                }
            }
            
            context?.setStrokeColor(UIColor.gray.cgColor)
            context?.setLineWidth(2)
            if paths.count == 0 {
                createGrid(horizontalStep, verticalStep, width, height)
            }
            for path in paths {
                path.stroke()
            }
        }
    }
    
    private func createRectangles(_ horizontalStep: CGFloat, _ verticalStep: CGFloat) {
        if let w = world {
            for (coord, _) in w.cells {
                let x = CGFloat(coord.x) * horizontalStep
                let y = CGFloat(coord.y) * verticalStep
                let rect = CGRect(x: x, y: y, width: horizontalStep, height: verticalStep)
                rectangles[coord] = rect
            }
        }
    }
    
    private func createGrid(_ horizontalStep: CGFloat, _ verticalStep: CGFloat, _ width: CGFloat, _ height: CGFloat) {
        if let w = world {
            // Vertical lines
            for i in 1..<w.size {
                let vertical = UIBezierPath()
                let distance = CGFloat(i)
                let startingPoint = CGPoint(x: horizontalStep * distance, y: 0)
                let endPoint = CGPoint(x: horizontalStep * distance, y: height)
                vertical.move(to: startingPoint)
                vertical.addLine(to: endPoint)
                paths.append(vertical)
            }
            
            // Horizontal lines
            for j in 1..<w.size {
                let horizontal = UIBezierPath()
                let distance = CGFloat(j)
                let startingPoint = CGPoint(x: 0, y: verticalStep * distance)
                let endPoint = CGPoint(x: width, y: verticalStep * distance)
                horizontal.move(to: startingPoint)
                horizontal.addLine(to: endPoint)
                paths.append(horizontal)
            }
        }
    }
}
