//
//  World.swift
//  Conway
//
//  Created by Adrian Kosmaczewski on 19.10.17.
//  Copyright © 2017 Adrian Kosmaczewski. All rights reserved.
//

import Foundation

public struct World {
    let size: Int
    var cells = [Coord: Cell]()

    public init(size: Int = 5, aliveCells: [Coord]? = nil) {
        self.size = size
        
        for a in 0..<size {
            for b in 0..<size {
                let coord = Coord(a, b)
                if let contains = aliveCells?.contains(coord) {
                    cells[coord] = contains ? .alive : .dead
                }
                else {
                    cells[coord] = .dead
                }
            }
        }
    }

    public mutating func setAlive(coordinates: [Coord]) {
        for coord in coordinates {
            cells[coord] = .alive
        }
    }
    
    public func evolve() -> World {
        var alive = [Coord]()
        
        for (coord, cell) in cells {
            var count = 0
            _ = (-1...1).map { a in
                let neighbors = (-1...1).map { Coord(coord.x + a, coord.y + $0) }
                    .filter { $0 != coord }
                count += neighbors.map { cells[$0] }
                    .filter { $0 == .alive }.count
            }
            
            switch cell {
            case .alive:
                if count == 2 || count == 3 {
                    alive.append(coord)
                }
            case .dead:
                if count == 3 {
                    alive.append(coord)
                }
            }
        }
        
        return World(size: size, aliveCells: alive)
    }
}

extension World {
    public static func blinker(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x, coord.y),
            Coord(coord.x + 1, coord.y),
            Coord(coord.x + 2, coord.y)
        ]
    }
    
    public static func beacon(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x, coord.y),
            Coord(coord.x + 1, coord.y),
            Coord(coord.x, coord.y + 1),
            Coord(coord.x + 1, coord.y + 1),
            Coord(coord.x + 2, coord.y + 2),
            Coord(coord.x + 3, coord.y + 2),
            Coord(coord.x + 2, coord.y + 3),
            Coord(coord.x + 3, coord.y + 3),
        ]
    }
    
    public static func glider(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x + 2, coord.y + 2),
            Coord(coord.x + 1, coord.y + 2),
            Coord(coord.x, coord.y + 2),
            Coord(coord.x + 2, coord.y + 1),
            Coord(coord.x + 1, coord.y),
        ]
    }
    
    public static func block(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x, coord.y),
            Coord(coord.x + 1, coord.y),
            Coord(coord.x, coord.y + 1),
            Coord(coord.x + 1, coord.y + 1),
        ]
    }
    
    public static func tub(at coord: Coord) -> [Coord] {
        return [
            Coord(coord.x + 1, coord.y),
            Coord(coord.x, coord.y + 1),
            Coord(coord.x + 2, coord.y + 1),
            Coord(coord.x + 1, coord.y + 2),
        ]
    }
}

extension World: Equatable {
    public static func ==(lhs: World, rhs: World) -> Bool {
        return lhs.cells == rhs.cells
    }
}

extension World: CustomStringConvertible {
    public var description: String {
//        let result = cells.filter { $0.value == .alive }
//            .flatMap { $0.key }
//            .reduce("") { $0 + "\($1)\n" }
//        return result
        var str = "\n"
        
        for a in 0..<size {
            if a == 0 {
                // First line with coordinates
                str += "   "
                for b in 0..<size {
                    str += "   \(b)   "
                }
                str += "\n"
            }
            str += " \(a) | "
            for b in 0..<size {
                let coord = Coord(b, a)
                if let cell = cells[coord] {
                    str += cell.tableDescription
                }
            }
            str += "\n"
        }
        
        return str
    }
}
