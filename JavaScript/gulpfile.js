const browserify = require('browserify')
const gulp = require('gulp')
const uglify = require('gulp-uglify')
const source = require('vinyl-source-stream')
const buffer = require('vinyl-buffer')

exports.default = gulp.task(
  'default',
  gulp.series(function () {
    return browserify({
      entries: ['src/main_browser.js']
    })
      .bundle()
      .pipe(source('conway.js'))
      .pipe(buffer())
      .pipe(uglify())
      .pipe(gulp.dest('dist'))
  })
)
