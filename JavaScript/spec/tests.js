const { World } = require('../src/World')
const { Coord } = require('../src/Coord')
const { describe, it } = require('mocha')
const { expect } = require('chai')

describe('Block', () => {
  it('Evolves', () => {
    const alive = World.block(new Coord(0, 0))
    const original = new World(5, alive)
    const next = original.evolve()
    expect(next).to.deep.equal(original)
  })
})

describe('Tub', () => {
  it('Evolves', () => {
    const alive = World.tub(new Coord(0, 0))
    const original = new World(5, alive)
    const next = original.evolve()
    expect(next).to.deep.equal(original)
  })
})

describe('Blinker', () => {
  it('Evolves', () => {
    const alive = World.blinker(new Coord(0, 1))
    const original = new World(3, alive)
    const gen1 = original.evolve()
    const expectedAlive = [
      new Coord(1, 0),
      new Coord(1, 1),
      new Coord(1, 2)
    ]
    const expected = new World(3, expectedAlive)
    expect(expected).to.deep.equal(gen1)
    const gen2 = gen1.evolve()
    expect(original).to.deep.equal(gen2)
  })
})
