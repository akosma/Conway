const { World } = require('./World')
const { Cell } = require('./Cell')
const { Coord } = require('./Coord')

class WorldView {
  // eslint-disable-next-line accessor-pairs
  set world (newWorld) {
    this.currentWorld = newWorld
    this.clear()
    this.draw()
  }

  constructor(canvas) {
    this.ctx = canvas.getContext('2d')
    this.currentWorld = null
    this.contentWidth = canvas.width
    this.contentHeight = canvas.height
  }

  clear () {
    if (this.ctx) {
      this.ctx.clearRect(0, 0, this.contentWidth, this.contentHeight)
      this.ctx.fillStyle = 'rgba(255, 255, 255, 0.5)'
      this.ctx.fillRect(0, 0, this.contentWidth, this.contentHeight)
    }
  }

  draw () {
    if (this.currentWorld && this.ctx) {
      const size = this.currentWorld.size
      const horizontalStep = this.contentWidth / size
      const verticalStep = this.contentHeight / size

      // Cells
      this.ctx.lineWidth = 3
      this.ctx.strokeStyle = 'white'
      this.ctx.fillStyle = 'black'
      Object.entries(this.currentWorld.cells).forEach(([coordText, cell]) => {
        if (cell === Cell.alive) {
          const coord = Coord.fromString(coordText)
          const x = coord.x * horizontalStep
          const y = coord.y * verticalStep
          this.ctx.fillRect(x, y, horizontalStep, verticalStep)
          this.ctx.rect(x, y, horizontalStep, verticalStep)
        }
      })

      // Lines
      this.ctx.beginPath()
      for (let i = 1; i < this.currentWorld.size; ++i) {
        this.ctx.moveTo(horizontalStep * i, 0)
        this.ctx.lineTo(horizontalStep * i, this.contentHeight)
        this.ctx.moveTo(0, verticalStep * i)
        this.ctx.lineTo(this.contentWidth, verticalStep * i)
      }

      // The HTML5 canvas performs much better when
      // all stroke() calls are done in just one batch!
      // https://www.html5rocks.com/en/tutorials/canvas/performance/
      this.ctx.lineWidth = 0.5
      this.ctx.strokeStyle = 'lightgrey'
      this.ctx.stroke()
    }
  }
}

module.exports = { WorldView }
