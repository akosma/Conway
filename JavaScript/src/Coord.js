/* eslint-disable no-useless-constructor */
class Coord {
  constructor (x, y) {
    this.x = x
    this.y = y
  }

  toString () {
    return `${this.x}:${this.y}`
  }

  static fromString (str) {
    const parts = str.split(':')
    const c = parts.map(parseFloat)
    return new Coord(c[0], c[1])
  }
}

module.exports = { Coord }
