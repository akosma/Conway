const { Cell } = require('./Cell')
const { Coord } = require('./Coord')

class World {
  constructor (size, aliveCells) {
    this.size = size
    this.cells = {}
    const aliveCellStrings = aliveCells.map(item => item.toString())
    for (let a = 0; a < size; ++a) {
      for (let b = 0; b < size; ++b) {
        const coord = `${a}:${b}`
        if (aliveCellStrings.includes(coord)) {
          this.cells[coord] = Cell.alive
        } else {
          this.cells[coord] = Cell.dead
        }
      }
    }
  }

  evolve () {
    const alive = []
    const range = [-1, 0, 1]
    Object.entries(this.cells).forEach(([coord, cell]) => {
      const coordObj = Coord.fromString(coord)
      let count = 0
      for (const a of range) {
        for (const b of range) {
          const currentCoord = (new Coord(coordObj.x + a, coordObj.y + b)).toString()
          if (coord !== currentCoord && this.cells[currentCoord] && this.cells[currentCoord] === Cell.alive) {
            count += 1
          }
        }
      }

      switch (cell) {
        case Cell.alive: {
          if (count === 2 || count === 3) {
            alive.push(coord)
          }
          break
        }
        case Cell.dead: {
          if (count === 3) {
            alive.push(coord)
          }
          break
        }
      }
    })
    return new World(this.size, alive)
  }

  toString () {
    let str = '\n'

    for (let a = 0; a < this.size; ++a) {
      if (a === 0) {
        // First line with coordinates
        str += '    '
        for (let b = 0; b < this.size; ++b) {
          str += b.toString().padStart(3) + '|'
        }
        str += '\n'
      }
      str += a.toString().padStart(3) + '|'
      for (let b = 0; b < this.size; ++b) {
        str += this.cells[`${b}:${a}`]
      }
      str += '\n'
    }
    return str
  }

  static blinker (origin) {
    return [
      new Coord(origin.x, origin.y),
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x + 2, origin.y)
    ]
  }

  static beacon (origin) {
    return [
      new Coord(origin.x, origin.y),
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x, origin.y + 1),
      new Coord(origin.x + 1, origin.y + 1),
      new Coord(origin.x + 2, origin.y + 2),
      new Coord(origin.x + 3, origin.y + 2),
      new Coord(origin.x + 2, origin.y + 3),
      new Coord(origin.x + 3, origin.y + 3)
    ]
  }

  static glider (origin) {
    return [
      new Coord(origin.x + 2, origin.y + 2),
      new Coord(origin.x + 1, origin.y + 2),
      new Coord(origin.x, origin.y + 2),
      new Coord(origin.x + 2, origin.y + 1),
      new Coord(origin.x + 1, origin.y)
    ]
  }

  static block (origin) {
    return [
      new Coord(origin.x, origin.y),
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x, origin.y + 1),
      new Coord(origin.x + 1, origin.y + 1)
    ]
  }

  static tub (origin) {
    return [
      new Coord(origin.x + 1, origin.y),
      new Coord(origin.x, origin.y + 1),
      new Coord(origin.x + 2, origin.y + 1),
      new Coord(origin.x + 1, origin.y + 2)
    ]
  }
}

module.exports = { World }
