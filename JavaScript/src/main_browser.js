const { World } = require('./World')
const { Coord } = require('./Coord')
const { WorldView } = require('./WorldView')

const blinker = World.blinker(new Coord(0, 1))
const beacon = World.beacon(new Coord(10, 10))
const glider = World.glider(new Coord(4, 5))
const block1 = World.block(new Coord(1, 10))
const block2 = World.block(new Coord(18, 3))
const tub = World.tub(new Coord(6, 1))
const alive = blinker.concat(beacon).concat(glider).concat(block1).concat(block2).concat(tub)

const canvas = document.getElementById('worldView')
const generationLabel = document.getElementById('generationLabel')
const worldView = new WorldView(canvas)

let world = new World(30, alive)
let generation = 0

worldView.world = world

const callback = () => {
  generation++
  world = world.evolve()
  worldView.world = world
  generationLabel.innerText = `Generation ${generation}`

  // For super-fast animation:
  // requestAnimationFrame(callback);
}

callback()
setInterval(callback, 500)
