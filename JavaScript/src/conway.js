const { World } = require('./World')
const { Coord } = require('./Coord')

process.on('SIGINT', () => {
  console.clear()
  process.exit(0)
})

const blinker = World.blinker(new Coord(0, 1))
const beacon = World.beacon(new Coord(10, 10))
const glider = World.glider(new Coord(4, 5))
const block1 = World.block(new Coord(1, 10))
const block2 = World.block(new Coord(18, 3))
const tub = World.tub(new Coord(6, 1))
const alive = blinker.concat(beacon).concat(glider).concat(block1).concat(block2).concat(tub)

let world = new World(30, alive)
let generation = 0

const callback = () => {
  world = world.evolve()
  generation++
  console.clear()
  console.log(`${world}`)
  console.log(`Generation ${generation}`)
}

setInterval(callback, 500)
