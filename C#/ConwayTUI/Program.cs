﻿using Terminal.Gui;
using System.Data;
using NStack;
using System.Linq;
using System.Globalization;
using ConwayLib;

static DataTable BuildDemoDataTable(World world)
{
    var dt = new DataTable();

    for (int i = 0; i < world.Size + 1; i++)
    {
        dt.Columns.Add(i.ToString());
    }

    for (int i = 0; i < world.Size; i++)
    {
        List<object> row = new List<object>() { i.ToString() };
        for (int j = 0; j < world.Size; j++)
        {
            var coord = new Coord { X = j, Y = i };
            row.Add(world.Cells[coord].ToEnumString().Replace("|", ""));
        }
        dt.Rows.Add(row.ToArray());
    }
    return dt;
}

Application.Init();
var tableView = new TableView()
{
    X = 0,
    Y = 0,
    Width = Dim.Fill(),
    Height = Dim.Fill(),
};

var menu = new MenuBar(new MenuBarItem[] {
            new("_File", new MenuItem [] {
                new("_Quit", "", () => { Application.RequestStop(); })
            })
        });
Application.Top.Add(menu);

var win = new Window("Conway")
{
    Width = Dim.Fill(),
    Height = Dim.Fill()
};
win.Add(tableView);
Application.Top.Add(win);

var alive = World.Blinker(new Coord { X = 0, Y = 1 });
alive.AddRange(World.Beacon(new Coord { X = 10, Y = 10 }));
alive.AddRange(World.Glider(new Coord { X = 4, Y = 5 }));
alive.AddRange(World.Block(new Coord { X = 1, Y = 10 }));
alive.AddRange(World.Block(new Coord { X = 18, Y = 3 }));
alive.AddRange(World.Tub(new Coord { X = 6, Y = 1 }));
var world = new World(30, alive);
var generation = 0;

Func<MainLoop, bool> callback = (MainLoop loop) =>
{
    generation++;
    world = world.Evolve();
    tableView.Table = BuildDemoDataTable(world);
    return true;
};
var token = Application.MainLoop.AddTimeout(TimeSpan.FromMilliseconds(500), callback);

Application.Run();
Application.Shutdown();
