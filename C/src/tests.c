#include "conway/conway.h"
#include <CUnit/Basic.h>
#include <stddef.h>
#include <stdlib.h>
#include <strings.h>

void test_block() {
    world_t original;
    world_init(&original);
    world_block(&original, (coord_t){0, 0});
    world_evolve(&original);

    world_t next;
    world_init(&next);
    world_block(&next, (coord_t){0, 0});
    CU_ASSERT(world_equals(&original, &next));
}

void test_tub() {
    world_t original;
    world_init(&original);
    world_tub(&original, (coord_t){0, 0});
    world_evolve(&original);

    world_t next;
    world_init(&next);
    world_tub(&next, (coord_t){0, 0});
    CU_ASSERT(world_equals(&original, &next));
}

void test_blinker() {
    world_t original;
    world_init(&original);
    world_blinker(&original, (coord_t){0, 1});

    world_t gen2;
    world_copy(&gen2, &original);

    world_t gen1;
    world_init(&gen1);
    world_set_alive(&gen1, (coord_t){1, 0});
    world_set_alive(&gen1, (coord_t){1, 1});
    world_set_alive(&gen1, (coord_t){1, 2});

    world_evolve(&original);
    CU_ASSERT(world_equals(&original, &gen1));
    world_evolve(&original);
    CU_ASSERT(world_equals(&original, &gen2));
}

int32_t main() {
    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry()) {
        return CU_get_error();
    }

    /* add a suite to the registry */
    CU_pSuite pSuite = CU_add_suite("Tests", nullptr, nullptr);
    if (nullptr == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (nullptr == CU_add_test(pSuite, "Block", test_block)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (nullptr == CU_add_test(pSuite, "Tub", test_tub)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (nullptr == CU_add_test(pSuite, "Blinker", test_blinker)) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    const int32_t failed_tests = CU_get_number_of_tests_failed();
    CU_cleanup_registry();
    return failed_tests;
}
