#include "cell.h"
#include <stdlib.h>
#include <string.h>

char *cell_cstr(cell_t cell) {
    if (cell == ALIVE) {
        return " x |";
    }
    return "   |";
}
