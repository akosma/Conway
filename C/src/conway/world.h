#ifndef WORLD_H
#define WORLD_H

#include "cell.h"
#include "coord.h"

#define WORLD_SIZE 30

typedef struct world_t {
    cell_t cells[WORLD_SIZE][WORLD_SIZE];
} world_t;

void world_init(world_t *);

void world_evolve(world_t *);

void world_print(world_t *, int);

bool world_equals(world_t *, world_t *);

void world_copy(world_t *destination, world_t *origin);

void world_set_alive(world_t *, coord_t);

void world_blinker(world_t *, coord_t);

void world_tub(world_t *, coord_t);

void world_block(world_t *, coord_t);

void world_beacon(world_t *, coord_t);

void world_glider(world_t *, coord_t);

#endif // WORLD_H
