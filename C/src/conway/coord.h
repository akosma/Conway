#ifndef COORD_H
#define COORD_H

#include <stdint.h>

typedef struct coord_t {
    int32_t x;
    int32_t y;
} coord_t;

#endif // COORD_H
