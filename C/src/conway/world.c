#include "world.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdint.h>

int32_t world_get(world_t *world, coord_t coord) {
    if (coord.x < WORLD_SIZE && coord.y < WORLD_SIZE) {
        return world->cells[coord.x][coord.y];
    }
    raise(SIGTRAP);
    return 0;
}

bool world_is_alive(world_t *world, coord_t coord) {
    return world_get(world, coord) == ALIVE;
}

void world_set_alive(world_t *world, coord_t coord) {
    if (coord.x < WORLD_SIZE && coord.y < WORLD_SIZE) {
        world->cells[coord.x][coord.y] = ALIVE;
    }
}

void world_copy(world_t *destination, world_t *origin) {
    memcpy(destination->cells, origin->cells, sizeof(origin->cells));
}

bool world_equals(world_t *first, world_t *second) {
    return memcmp(first->cells, second->cells, sizeof(first->cells)) == 0;
}

void world_init(world_t *world) {
    memset(world->cells, DEAD, sizeof(world->cells));
}

void world_evolve(world_t *world) {
    world_t temp;
    world_copy(&temp, world);
    world_init(world);

    for (int32_t x = 0; x < WORLD_SIZE; x++) {
        for (int32_t y = 0; y < WORLD_SIZE; y++) {
            int32_t counter = 0;

            /* Check the neighbors */
            for (int32_t a = x - 1; a <= x + 1; a++) {
                for (int32_t b = y - 1; b <= y + 1; b++) {
                    if (a >= 0 && b >= 0 && a < WORLD_SIZE && b < WORLD_SIZE) {
                        counter += world_get(&temp, (coord_t){a, b});
                    }
                }
            }

            counter -= world_get(&temp, (coord_t){x, y}); /* Subtract self */

            /* Apply the rules */
            if (world_is_alive(&temp, (coord_t){x, y})) {
                if (counter == 2 || counter == 3) {
                    world_set_alive(world, (coord_t){x, y});
                }
            } else {
                if (counter == 3) {
                    world_set_alive(world, (coord_t){x, y});
                }
            }
        }
    }
}

void world_print(world_t *world, int32_t max_size) {
    printf("\n");
    for (int32_t a = 0; a < max_size; a++) {
        if (a == 0) {
            printf("    ");
            for (int32_t b = 0; b < max_size; b++) {
                printf(" %2d|", b);
            }
            printf("\n");
        }
        printf(" %2d|", a);
        for (int32_t b = 0; b < max_size; b++) {
            printf("%s", cell_cstr(world_get(world, (coord_t){b, a})));
        }
        printf("\n");
    }
    printf("\n");
}

void world_blinker(world_t *world, coord_t start) {
    world_set_alive(world, (coord_t){start.x, start.y});
    world_set_alive(world, (coord_t){start.x + 1, start.y});
    world_set_alive(world, (coord_t){start.x + 2, start.y});
}

void world_beacon(world_t *world, coord_t start) {
    world_set_alive(world, (coord_t){start.x, start.y});
    world_set_alive(world, (coord_t){start.x + 1, start.y});
    world_set_alive(world, (coord_t){start.x, start.y + 1});
    world_set_alive(world, (coord_t){start.x + 1, start.y + 1});
    world_set_alive(world, (coord_t){start.x + 2, start.y + 2});
    world_set_alive(world, (coord_t){start.x + 3, start.y + 2});
    world_set_alive(world, (coord_t){start.x + 2, start.y + 3});
    world_set_alive(world, (coord_t){start.x + 3, start.y + 3});
}

void world_glider(world_t *world, coord_t start) {
    world_set_alive(world, (coord_t){start.x + 2, start.y + 2});
    world_set_alive(world, (coord_t){start.x + 1, start.y + 2});
    world_set_alive(world, (coord_t){start.x, start.y + 2});
    world_set_alive(world, (coord_t){start.x + 2, start.y + 1});
    world_set_alive(world, (coord_t){start.x + 1, start.y});
}

void world_tub(world_t *world, coord_t start) {
    world_set_alive(world, (coord_t){start.x + 1, start.y});
    world_set_alive(world, (coord_t){start.x, start.y + 1});
    world_set_alive(world, (coord_t){start.x + 2, start.y + 1});
    world_set_alive(world, (coord_t){start.x + 1, start.y + 2});
}

void world_block(world_t *world, coord_t start) {
    world_set_alive(world, (coord_t){start.x, start.y});
    world_set_alive(world, (coord_t){start.x + 1, start.y});
    world_set_alive(world, (coord_t){start.x, start.y + 1});
    world_set_alive(world, (coord_t){start.x + 1, start.y + 1});
}
