#ifndef CELL_H
#define CELL_H

typedef enum cell_t { ALIVE = 1, DEAD = 0 } cell_t;

char *cell_cstr(cell_t cell);

#endif // CELL_H
