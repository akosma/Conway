#include "conway/conway.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Thanks to
// https://stackoverflow.com/a/17766999/133764
// for the handling of CTRL+C

volatile sig_atomic_t flag = 0;

void signal_handler(int32_t) { flag = 1; }

int32_t main() {
    signal(SIGINT, signal_handler);

    world_t world;
    world_init(&world);
    world_blinker(&world, (coord_t){0, 1});
    world_beacon(&world, (coord_t){10, 10});
    world_glider(&world, (coord_t){4, 5});
    world_block(&world, (coord_t){1, 10});
    world_block(&world, (coord_t){18, 3});
    world_tub(&world, (coord_t){6, 1});

    int32_t generation = 0;

    while (true) {
        if (flag == 1) {
            break;
        }
        system("clear");
        generation++;
        world_print(&world, WORLD_SIZE);
        fprintf(stdout, "Generation %d\n", generation);
        world_evolve(&world);
        usleep(500000); // 500 milliseconds in microseconds
    }

    system("clear");
    return EXIT_SUCCESS;
}
