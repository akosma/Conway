package ma.akos.Conway;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void block() {
        var alive = World.block(new Coord(0, 0));
        var original = new World(5, alive);
        var next = original.evolve();
        assertEquals(original, next);
    }

    @Test
    public void tub() {
        var alive = World.tub(new Coord(0, 0));
        var original = new World(5, alive);
        var next = original.evolve();
        assertEquals(original, next);
    }

    @Test
    public void blinker() {
        var alive = World.blinker(new Coord(0, 1));
        var original = new World(3, alive);
        var gen1 = original.evolve();
        var expectedAlive = List.of(new Coord(1, 0), new Coord(1, 1), new Coord(1, 2));
        var expected = new World(3, expectedAlive);
        assertEquals(expected, gen1);
        var gen2 = gen1.evolve();
        assertEquals(original, gen2);
    }
}
