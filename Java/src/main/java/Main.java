import ma.akos.Conway.World;
import ma.akos.Conway.Coord;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static boolean keepRunning = true;

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static void main(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            clearScreen();
            keepRunning = false;
        }));

        var aliveCells = new ArrayList<Coord>();
        aliveCells.addAll(World.blinker(new Coord(0, 1)));
        aliveCells.addAll(World.beacon(new Coord(10, 10)));
        aliveCells.addAll(World.glider(new Coord(4, 5)));
        aliveCells.addAll(World.block(new Coord(1, 10)));
        aliveCells.addAll(World.block(new Coord(18, 3)));
        aliveCells.addAll(World.tub(new Coord(6, 1)));
        var world = new World(30, aliveCells);
        int generation = 0;

        while (keepRunning) {
            clearScreen();
            generation++;
            System.out.println(world);
            System.out.println("Generation " + generation);
            world = world.evolve();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
