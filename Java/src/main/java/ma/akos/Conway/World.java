package ma.akos.Conway;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class World {
    private final int size;
    private final Map<Coord, Cell> cells;

    public World(int size, List<Coord> aliveCells) {
        this.size = size;
        this.cells = new HashMap<>();
        for (int a = 0; a < this.size; ++a) {
            for (int b = 0; b < this.size; ++b) {
                Coord currentCoord = new Coord(a, b);
                if (aliveCells.contains(currentCoord)) {
                    this.cells.put(currentCoord, Cell.ALIVE);
                } else {
                    this.cells.put(currentCoord, Cell.DEAD);
                }
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof World other)) {
            return false;
        }
        if (other.getSize() != this.size) {
            return false;
        }
        for (Coord coord : this.cells.keySet()) {
            Cell value = this.cells.get(coord);
            if (!other.getCells().containsKey(coord) || other.getCells().get(coord) != value) {
                return false;
            }
        }
        return true;
    }

    public int getSize() {
        return size;
    }

    public Map<Coord, Cell> getCells() {
        return cells;
    }

    public World evolve() {
        var aliveCells = new ArrayList<Coord>();
        for (Coord coord : this.cells.keySet()) {
            int count = 0;
            Cell cell = this.cells.get(coord);
            for (int a = -1; a < 2; ++a) {
                for (int b = -1; b < 2; ++b) {
                    Coord currentCoord = new Coord(coord.x() + a, coord.y() + b);
                    if (!currentCoord.equals(coord)
                            && this.cells.containsKey(currentCoord)
                            && this.cells.get(currentCoord) == Cell.ALIVE) {
                        count += 1;
                    }
                }
            }

            switch (cell) {
                case ALIVE:
                    if (count == 2 || count == 3) {
                        aliveCells.add(coord);
                    }
                    break;

                case DEAD:
                    if (count == 3) {
                        aliveCells.add(coord);
                    }
                    break;
            }
        }

        return new World(this.size, aliveCells);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("\n");
        for (int a = 0; a < this.size; ++a) {
            // First line with coordinates
            if (a == 0) {
                builder.append("    ");
                for (int b = 0; b < this.size; ++b) {
                    builder.append(String.format("%3d|", b));
                }
                builder.append("\n");
            }
            builder.append(String.format("%3d|", a));
            for (int b = 0; b < this.size; ++b) {
                Coord c = new Coord(b, a);
                builder.append(this.cells.get(c));
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public static List<Coord> blinker(Coord coord) {
        return List.of(
                new Coord(coord.x(), coord.y()),
                new Coord(coord.x() + 1, coord.y()),
                new Coord(coord.x() + 2, coord.y()));
    }

    public static List<Coord> beacon(Coord coord) {
        return List.of(
                new Coord(coord.x(), coord.y()),
                new Coord(coord.x() + 1, coord.y()),
                new Coord(coord.x(), coord.y() + 1),
                new Coord(coord.x() + 1, coord.y() + 1),
                new Coord(coord.x() + 2, coord.y() + 2),
                new Coord(coord.x() + 3, coord.y() + 2),
                new Coord(coord.x() + 2, coord.y() + 3),
                new Coord(coord.x() + 3, coord.y() + 3));
    }

    public static List<Coord> glider(Coord coord) {
        return List.of(
                new Coord(coord.x() + 2, coord.y() + 2),
                new Coord(coord.x() + 1, coord.y() + 2),
                new Coord(coord.x(), coord.y() + 2),
                new Coord(coord.x() + 2, coord.y() + 1),
                new Coord(coord.x() + 1, coord.y()));
    }

    public static List<Coord> block(Coord coord) {
        return List.of(
                new Coord(coord.x(), coord.y()),
                new Coord(coord.x() + 1, coord.y()),
                new Coord(coord.x(), coord.y() + 1),
                new Coord(coord.x() + 1, coord.y() + 1));
    }

    public static List<Coord> tub(Coord coord) {
        return List.of(
                new Coord(coord.x() + 1, coord.y()),
                new Coord(coord.x(), coord.y() + 1),
                new Coord(coord.x() + 2, coord.y() + 1),
                new Coord(coord.x() + 1, coord.y() + 2));
    }
}
