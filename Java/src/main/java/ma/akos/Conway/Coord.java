package ma.akos.Conway;

public record Coord(int x, int y) {
    @Override
    public String toString() {
        return String.format("%d:%d", this.x, this.y);
    }
}
