package ma.akos.Conway;

public enum Cell {
    DEAD, ALIVE;

    public String toString() {
        if (this == DEAD) {
            return "   |";
        }
        return " x |";
    }
}
