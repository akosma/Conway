= Java

This application is compatible with Java 17, and can be built using the `mvn package` command. To run it, use the `java -jar target/conway-1.0-SNAPSHOT.jar`.

== Tests

The Java application can be tested by running `mvn test` at the root of the folder.
