= Kotlin

The Android application was written in https://kotlinlang.org/[Kotlin 2]. The project requires https://developer.android.com/studio/index.html[Android Studio]. This application does not have any external dependencies.

The command-line version can be run directly with:

```
$ ./gradlew run
```

Build and run the CLI app with the command:

```
$ ./gradlew build
$ java -jar app/cli/build/libs/cli.jar
```

== Tests

The Android tests can be run with:

```
$ ./gradlew test
```

