package ma.akos.conway

import ma.akos.conwaylib.Coord
import ma.akos.conwaylib.World
import org.junit.Assert.assertEquals
import org.junit.Test

class ExampleUnitTest {
    @Test
    fun block() {
        val alive = World.block(Coord(0, 0))
        val original = World(5, alive)
        val next = original.evolve()
        assertEquals(original, next)
    }

    @Test
    fun tub() {
        val alive = World.tub(Coord(0, 0))
        val original = World(5, alive)
        val next = original.evolve()
        assertEquals(original, next)
    }

    @Test
    fun blinker() {
        val alive = World.blinker(Coord(0, 1))
        val original = World(3, alive)
        val gen1 = original.evolve()
        val expectedAlive = listOf(
            Coord(1, 0),
            Coord(1, 1),
            Coord(1, 2)
        )
        val expected = World(3, expectedAlive)
        assertEquals(expected, gen1)
        val gen2 = gen1.evolve()
        assertEquals(original, gen2)
    }
}
