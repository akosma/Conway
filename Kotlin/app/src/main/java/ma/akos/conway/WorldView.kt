package ma.akos.conway

import android.content.Context
import android.graphics.*
import android.view.View
import ma.akos.conwaylib.Cell
import ma.akos.conwaylib.Coord
import ma.akos.conwaylib.World

class WorldView(context: Context) : View(context) {
    private lateinit var mWorld: World
    private var mRectangles = mutableMapOf<Coord, Rect>()
    private var mPaths = mutableListOf<Path>()

    private val cellPaint = object : Paint() {
        init {
            style = Style.FILL
            color = Color.BLACK
        }
    }
    private val cellStroke = object : Paint() {
        init {
            style = Style.STROKE
            strokeCap = Cap.ROUND
            strokeWidth = 10f
            color = Color.WHITE
        }
    }
    private val linePaint = object : Paint() {
        init {
            style = Style.STROKE
            strokeCap = Cap.SQUARE
            strokeWidth = 2f
        }
    }

    var world: World
        get() = mWorld
        set(newWorld) {
            mWorld = newWorld
            invalidate()
        }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val world = mWorld
        val size = world.size
        val contentWidth = width
        val contentHeight = height

        val horizontalStep = contentWidth / size
        val verticalStep = contentHeight / size

        if (mRectangles.isEmpty()) {
            createRectangles(horizontalStep, verticalStep)
        }
        for ((coord, cell) in world.cells) {
            if (cell == Cell.Alive) {
                val rect = mRectangles[coord]
                if (rect != null) {
                    canvas.drawRect(rect, cellPaint)
                    canvas.drawRect(rect, cellStroke)
                }
            }
        }

        if (mPaths.isEmpty()) {
            createGrid(horizontalStep, verticalStep, contentWidth, contentHeight)
        }
        for (path in mPaths) {
            canvas.drawPath(path, linePaint)
        }
    }

    private fun createRectangles(horizontalStep: Int, verticalStep: Int) {
        for ((coord, _) in mWorld.cells) {
            val x = coord.x * horizontalStep
            val y = coord.y * verticalStep
            val rect = Rect(x, y, x + horizontalStep, y + verticalStep)
            mRectangles[coord] = rect
        }
    }

    private fun createGrid(horizontalStep: Int, verticalStep: Int, contentWidth: Int, contentHeight: Int) {
        // Vertical lines
        for (i in 1 until mWorld.size) {
            val vertical = Path()
            val distance = i.toFloat()
            vertical.moveTo(horizontalStep * distance, 0f)
            vertical.lineTo(horizontalStep * distance, contentHeight.toFloat())
            mPaths.add(vertical)
        }

        // Horizontal lines
        for (j in 1 until mWorld.size) {
            val horizontal = Path()
            val distance = j.toFloat()
            horizontal.moveTo(0f, verticalStep * distance)
            horizontal.lineTo(contentWidth.toFloat(), verticalStep * distance)
            mPaths.add(horizontal)
        }
    }
}
