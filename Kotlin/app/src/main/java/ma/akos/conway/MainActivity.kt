package ma.akos.conway

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import ma.akos.conwaylib.Coord
import ma.akos.conwaylib.World

class MainActivity : AppCompatActivity() {
    private lateinit var worldView: WorldView
    private val handler = Handler(Looper.getMainLooper())
    private var generation: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val alive = mutableListOf<Coord>()
        alive.addAll(World.blinker(Coord(0, 1)))
        alive.addAll(World.beacon(Coord(10, 10)))
        alive.addAll(World.glider(Coord(4, 5)))
        alive.addAll(World.block(Coord(1, 10)))
        alive.addAll(World.block(Coord(18, 3)))
        alive.addAll(World.tub(Coord(6, 1)))

        worldView = WorldView(this)
        worldView.world = World(30, alive)
        title = "Conway – Generation $generation"
        setContentView(worldView)
        evolve()
    }

    private fun evolve() {
        val runnable = Runnable {
            generation += 1
            title = "Conway – Generation $generation"
            worldView.world = worldView.world.evolve()
            evolve()
        }
        handler.postDelayed(runnable, 500)
    }
}
