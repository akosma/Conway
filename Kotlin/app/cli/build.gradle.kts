plugins {
    id("java-library")
    alias(libs.plugins.jetbrains.kotlin.jvm)
    id("application")
}

application {
    mainClass.set("ma.akos.conwaycli.MainKt")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

kotlin {
    compilerOptions {
        jvmTarget = org.jetbrains.kotlin.gradle.dsl.JvmTarget.JVM_11
    }
}

dependencies {
    implementation(project(":app:lib"))
}

tasks.jar {
    manifest {
        attributes["Main-Class"] = "ma.akos.conwaycli.MainKt"
    }

    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    // Include all runtime dependencies in the JAR
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) }) {
        exclude("META-INF/*.SF", "META-INF/*.DSA", "META-INF/*.RSA") // Avoid duplicate metadata errors
    }

    dependsOn(":app:lib:jar")
}
