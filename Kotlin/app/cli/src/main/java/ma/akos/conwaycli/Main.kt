package ma.akos.conwaycli

import ma.akos.conwaylib.Coord
import ma.akos.conwaylib.World
import sun.misc.Signal

fun main() {
    var keepRunning = true
    Signal.handle(Signal("INT")) {
        keepRunning = false
        clearScreen()
    }
    val alive = mutableListOf<Coord>()
    alive.addAll(World.blinker(Coord(0, 1)))
    alive.addAll(World.beacon(Coord(10, 10)))
    alive.addAll(World.glider(Coord(4, 5)))
    alive.addAll(World.block(Coord(1, 10)))
    alive.addAll(World.block(Coord(18, 3)))
    alive.addAll(World.tub(Coord(6, 1)))

    var world = World(30, alive)
    var generation = 0

    while(keepRunning) {
        generation++
        clearScreen()
        println(world)
        println("Generation $generation")
        world = world.evolve()
        Thread.sleep(500)
    }
}

private fun clearScreen() {
    println("\u001Bc")
}
