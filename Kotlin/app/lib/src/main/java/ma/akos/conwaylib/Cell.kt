package ma.akos.conwaylib

enum class Cell {
    Alive,
    Dead;

    fun tableDescription(): String {
        if (this == Alive) {
            return " x |"
        }
        return "   |"
    }
}
