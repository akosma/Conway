package ma.akos.conwaylib

data class Coord(val x: Int, val y: Int)
