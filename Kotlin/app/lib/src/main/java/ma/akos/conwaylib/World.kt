package ma.akos.conwaylib

class World(val size: Int, private val aliveCells: List<Coord>) {
    val cells = mutableMapOf<Coord, Cell>()

    init {
        for (a in 0 until size) {
            (0 until size)
                .map { Coord(a, it) }
                .forEach {
                    if (aliveCells.contains(it)) {
                        cells[it] = Cell.Alive
                    } else {
                        cells[it] = Cell.Dead
                    }
                }
        }
    }

    fun evolve(): World {
        val alive = mutableListOf<Coord>()
        for ((coord, cell) in cells) {
            var count = 0
            (-1..1).map { a ->
                count += (-1..1)
                    .map { Coord(coord.x + a, coord.y + it) }
                    .filter { it != coord }
                    .map { cells[it] }
                    .count { it == Cell.Alive }
            }

            when (cell) {
                Cell.Alive -> {
                    if (count == 2 || count == 3) {
                        alive.add(coord)
                    }
                }
                Cell.Dead -> {
                    if (count == 3) {
                        alive.add(coord)
                    }
                }
            }
        }
        return World(size, alive)
    }

    override fun toString(): String {
        val str = StringBuffer()
        for (a in 0 until size) {
            if (a == 0) {
                // First line with coordinates
                str.append("    ")
                for (b in 0 until size) {
                    str.append(String.format("%1$3s|", b))
                }
                str.append("\n")
            }
            str.append(String.format("%1$3s|", a))
            str.append((0 until size)
                .map { Coord(it, a) }
                .map { cells[it]?.tableDescription() }
                .reduce { acc, s -> acc + s })
            str.append("\n")
        }
        return str.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other is World) {
            return cells == other.cells && size == other.size
        }
        return false
    }

    override fun hashCode(): Int {
        return cells.hashCode()
    }

    companion object {
        fun blinker(coord: Coord): List<Coord> {
            return listOf(
                Coord(coord.x, coord.y),
                Coord(coord.x + 1, coord.y),
                Coord(coord.x + 2, coord.y)
            )
        }

        fun beacon(coord: Coord): List<Coord> {
            return listOf(
                Coord(coord.x, coord.y),
                Coord(coord.x + 1, coord.y),
                Coord(coord.x, coord.y + 1),
                Coord(coord.x + 1, coord.y + 1),
                Coord(coord.x + 2, coord.y + 2),
                Coord(coord.x + 3, coord.y + 2),
                Coord(coord.x + 2, coord.y + 3),
                Coord(coord.x + 3, coord.y + 3)
            )
        }

        fun glider(coord: Coord): List<Coord> {
            return listOf(
                Coord(coord.x + 2, coord.y + 2),
                Coord(coord.x + 1, coord.y + 2),
                Coord(coord.x, coord.y + 2),
                Coord(coord.x + 2, coord.y + 1),
                Coord(coord.x + 1, coord.y)
            )
        }

        fun block(coord: Coord): List<Coord> {
            return listOf(
                Coord(coord.x, coord.y),
                Coord(coord.x + 1, coord.y),
                Coord(coord.x, coord.y + 1),
                Coord(coord.x + 1, coord.y + 1)
            )
        }

        fun tub(coord: Coord): List<Coord> {
            return listOf(
                Coord(coord.x + 1, coord.y),
                Coord(coord.x, coord.y + 1),
                Coord(coord.x + 2, coord.y + 1),
                Coord(coord.x + 1, coord.y + 2)
            )
        }
    }
}
