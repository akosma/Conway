#!/usr/bin/env ruby
require_relative 'cell.rb'
require_relative 'coord.rb'

include Cell

class World
  attr_reader :size
  attr_reader :cells

  def initialize(size, alive_cells)
    @size = size
    @cells = Hash.new

    for a in 0..(size - 1)
      for b in 0..(size - 1)
        c = Coord.new(a, b)
        if alive_cells.any? { |ac| ac.x == c.x && ac.y == c.y }
          @cells[c] = Cell::ALIVE
        else
          @cells[c] = Cell::DEAD
        end
      end
    end
  end

  def evolve
    alive_cells = []
    @cells.each do |coord, cell|
      count = 0
      [-1, 0, 1].each do |a|
        [-1, 0, 1].each do |b|
          current_coord = Coord.new(coord.x + a, coord.y + b)
          count += 1 if current_coord != coord && @cells.key?(current_coord) && @cells[current_coord] == Cell::ALIVE
        end
      end

      case cell
      when Cell::ALIVE
        alive_cells.push(coord) if count == 2 || count == 3
      else
        alive_cells.push(coord) if count == 3
      end
    end
    World.new(@size, alive_cells)
  end

  def ==(w)
    false if @size != w.size
    @cells.each do |coord, cell|
      false unless w.cells.key?(coord)
      false unless w.cells[coord] == @cells[coord]
    end
  end

  def to_s
    s = "\n"
    for a in 0..(@size - 1)
      # First line with coordinates
      if a == 0
        s << "    "
        for b in 0..(@size - 1)
          s << "%3s|" % b
        end
        s << "\n"
      end
      s << "%3s|" % a
      for b in 0..(@size - 1)
        c = Coord.new(b, a)
        cell = @cells[c]
        s << cell_to_s(cell)
      end
      s << "\n"
    end
    s << "\n"
    s
  end

  def self.blinker(c)
    [ Coord.new(c.x, c.y),
      Coord.new(c.x + 1, c.y),
      Coord.new(c.x + 2, c.y) ]
  end

  def self.beacon(c)
    [ Coord.new(c.x, c.y),
      Coord.new(c.x + 1, c.y),
      Coord.new(c.x, c.y + 1),
      Coord.new(c.x + 1, c.y + 1),
      Coord.new(c.x + 2, c.y + 2),
      Coord.new(c.x + 3, c.y + 2),
      Coord.new(c.x + 2, c.y + 3),
      Coord.new(c.x + 3, c.y + 3) ]
  end

  def self.glider(c)
    [ Coord.new(c.x + 2, c.y + 2),
      Coord.new(c.x + 1, c.y + 2),
      Coord.new(c.x, c.y + 2),
      Coord.new(c.x + 2, c.y + 1),
      Coord.new(c.x + 1, c.y) ]
  end

  def self.block(c)
    [ Coord.new(c.x, c.y),
      Coord.new(c.x + 1, c.y),
      Coord.new(c.x, c.y + 1),
      Coord.new(c.x + 1, c.y + 1) ]
  end

  def self.tub(c)
    [ Coord.new(c.x + 1, c.y),
      Coord.new(c.x, c.y + 1),
      Coord.new(c.x + 2, c.y + 1),
      Coord.new(c.x + 1, c.y + 2) ]
  end
end
