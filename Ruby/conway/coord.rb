class Coord
  attr_reader :x
  attr_reader :y

  def initialize(x, y)
    @x = x
    @y = y
  end

  def self.parse(s)
    parts = s.split(':').map { |p| p.to_i }
    Coord.new(parts[0], parts[1])
  end

  def hash
    to_s.hash
  end

  def eql?(c)
    to_s == c.to_s
  end

  def !=(c)
    to_s != c.to_s
  end

  def to_s
    "#{@x}:#{@y}"
  end
end
