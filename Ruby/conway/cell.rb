module Cell
  DEAD = 0
  ALIVE = 1

  def cell_to_s(c)
    if c == DEAD
      "   |"
    else
      " x |"
    end
  end
end
