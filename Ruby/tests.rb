#!/usr/bin/env ruby

require "test/unit"
require_relative 'conway/world'
require_relative 'conway/coord'

class Tests < Test::Unit::TestCase
  def test_block
    alive = World.block Coord.new(0, 0)
    original = World.new(5, alive)
    next_world = original.evolve
    assert_equal(next_world, original)
  end

  def test_tub
    alive = World.tub Coord.new(0, 0)
    original = World.new(5, alive)
    next_world = original.evolve
    assert_equal(next_world, original)
  end

  def test_blinker
    alive = World.blinker Coord.new(0, 1)
    original = World.new(3, alive)
    gen1 = original.evolve
    expected_alive = [ Coord.new(1, 0),
        Coord.new(1, 1),
        Coord.new(1, 2) ]
    expected = World.new(3, expected_alive)
    assert_equal(expected, gen1)
    gen2 = gen1.evolve
    assert_equal(original, gen2)
  end
end
