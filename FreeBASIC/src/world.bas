#include once "../inc/world.bi"
#include once "../inc/hashtable.bi"
#include once "../inc/coord.bi"
#include once "../inc/cell.bi"

Function ArrayContains(aliveCells(Any) As Coord, c As Coord) As Boolean
    For i As Integer = Lbound(aliveCells) To Ubound(aliveCells)
        Dim current As Coord = aliveCells(i)
        If current.Equals(c) Then
            Return True
        End If
    Next
    Return False
End Function

Constructor World(newSize As Integer, aliveCells(Any) As Coord)
    size = newSize
    cells = New Hashtable(1024)
    For a As Integer = 0 To (size - 1)
        For b As Integer = 0 To (size - 1)
            Dim c As Coord = Coord(a, b)
            If ArrayContains(aliveCells(), c) Then
                cells->putvalue(c.ToString(), Alive)
            Else
                cells->putvalue(c.ToString(), Dead)
            End If
        Next
    Next
End Constructor

Destructor World( )
    Delete cells
End Destructor

Function World.Evolve() As World Ptr
    Dim aliveCells(Any) As Coord
    Redim aliveCells(size * size)

    Dim index As Integer = 0
    For a As Integer = 0 To (size - 1)
        For b As Integer = 0 To (size - 1)
            Dim count As Integer = 0
            Dim c As Coord = Coord(a, b)
            Dim v As Integer
            cells->getvalue(c.ToString(), v)

            For d As Integer = -1 To 1 Step 1
                For e As Integer = -1 To 1 Step 1
                    Dim currentCoord As Coord = Coord(c.x + d, c.y + e)
                    Dim currentCoordStr As String = currentCoord.ToString()
                    Dim vc As Integer
                    Dim found As Boolean
                    found = cells->getvalue(currentCoordStr, vc)
                    If (Not currentCoord.Equals(c)) And found And vc = Alive Then
                        count = count + 1
                    End If
                Next
            Next

            Select Case v
            Case Alive
                If count = 2 Or count = 3 Then
                    aliveCells(index) = c
                    index = index + 1
                End If
            Case Dead
                If count = 3 Then
                    aliveCells(index) = c
                    index = index + 1
                End If
            End Select
        Next
    Next

    Dim w As World Ptr = New World(size, aliveCells())
    Return w
End Function

Function FixedWidthString(s As String, m As Integer = 4, t As String = " ") As String
    Dim l As Integer = Len(s)
    If l >= m Then
        Return Left(s, m)
    End If
    Dim result As String
    For i As Integer = l To m - 1
        result = result & t
    Next
    result = result & s
    Return result
End Function

Function World.ToString() As String
    Dim result As String
    Const CRLF As String = Chr(13, 10)
    For a As Integer = 0 To (size - 1)
        If a = 0 Then
            ' First line with coordinates
            result = result & "    "
            For b As Integer = 0 To (size - 1)
                result = result & FixedWidthString(b & "|")
            Next
            result = result & CRLF
        End If
        result = result & FixedWidthString(a & "|")
        For b As Integer = 0 To (size - 1)
            Dim c As Coord = Coord(b, a)
            Dim coordStr As String = c.ToString()
            Dim cellValue As Integer
            cells->getvalue(coordStr, cellValue)
            result = result & CellToString(cellValue)
        Next
        result = result & CRLF
    Next
    Return result
End Function

Function World.Equals(w As World Ptr) As Boolean
    If size <> w->size Then
        Return False
    End If
    For a As Integer = 0 To (size - 1)
        For b As Integer = 0 To (size - 1)
            Dim c As Coord = Coord(a, b)
            Dim coordStr As String = c.ToString()
            Dim As Integer v1, v2
            cells->getvalue(coordStr, v1)

            Dim found As Boolean
            found = w->cells->getvalue(coordStr, v2)

            If Not found Or v1 <> v2 Then
                Return False
            End If
        Next
    Next
    Return True
End Function

Sub AppendArray(c As Coord, aliveCells(Any) As Coord)
    Dim arrayLen as Integer
    arraylen = UBound(aliveCells) - LBound(aliveCells) + 1
    Redim Preserve aliveCells(1 To arraylen + 1)
    aliveCells(arraylen + 1) = c
End Sub

Sub Blinker(c As Coord, aliveCells(Any) As Coord)
    AppendArray(Coord(c.x, c.y), aliveCells())
    AppendArray(Coord(c.x + 1, c.y), aliveCells())
    AppendArray(Coord(c.x + 2, c.y), aliveCells())
End Sub

Sub Beacon(c As Coord, aliveCells(Any) As Coord)
    AppendArray(Coord(c.x, c.y), aliveCells())
    AppendArray(Coord(c.x + 1, c.y), aliveCells())
    AppendArray(Coord(c.x, c.y + 1), aliveCells())
    AppendArray(Coord(c.x + 1, c.y + 1), aliveCells())
    AppendArray(Coord(c.x + 2, c.y + 2), aliveCells())
    AppendArray(Coord(c.x + 3, c.y + 2), aliveCells())
    AppendArray(Coord(c.x + 2, c.y + 3), aliveCells())
    AppendArray(Coord(c.x + 3, c.y + 3), aliveCells())
End Sub

Sub Glider(c As Coord, aliveCells(Any) As Coord)
    AppendArray(Coord(c.x + 2, c.y + 2), aliveCells())
    AppendArray(Coord(c.x + 1, c.y + 2), aliveCells())
    AppendArray(Coord(c.x, c.y + 2), aliveCells())
    AppendArray(Coord(c.x + 2, c.y + 1), aliveCells())
    AppendArray(Coord(c.x + 1, c.y), aliveCells())
End Sub

Sub Block(c As Coord, aliveCells(Any) As Coord)
    AppendArray(Coord(c.x, c.y), aliveCells())
    AppendArray(Coord(c.x + 1, c.y), aliveCells())
    AppendArray(Coord(c.x, c.y + 1), aliveCells())
    AppendArray(Coord(c.x + 1, c.y + 1), aliveCells())
End Sub

Sub Tub(c As Coord, aliveCells(Any) As Coord)
    AppendArray(Coord(c.x + 1, c.y), aliveCells())
    AppendArray(Coord(c.x, c.y + 1), aliveCells())
    AppendArray(Coord(c.x + 2, c.y + 1), aliveCells())
    AppendArray(Coord(c.x + 1, c.y + 2), aliveCells())
End Sub
