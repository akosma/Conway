#include once "../inc/coord.bi"

Constructor Coord(x As Integer, y As Integer)
   This.x = x
   This.y = y
End Constructor

Constructor Coord()
   This.x = -1
   This.y = -1
End Constructor

Function Coord.ToString() As String
    Return Str(This.x) & ":" & Str(This.y)
End Function

Function Coord.Equals(c As Coord) As Boolean
   Return This.x = c.x And This.y = c.y
End Function

'Adapted from:
'https://freebasic.net/forum/viewtopic.php?t=13975
Sub Split(Text As String, Delim As String = ":", Count As Long = 2, Ret() As String)
   Dim As Long x, p
   If Count < 1 Then
      Do
         x = InStr(x + 1, Text, Delim)
         p += 1
      Loop Until x = 0
      Count = p - 1
   ElseIf Count = 1 Then
      ReDim Ret(Count - 1)
      Ret(0) = Text
   Else
      Count -= 1
   End If
   Dim RetVal(Count) As Long
   x = 0
   p = 0
   Do Until p = Count
      x = InStr(x + 1,Text,Delim)
      RetVal(p) = x
      p += 1
   Loop
   ReDim Ret(Count)
   Ret(0) = Left(Text, RetVal(0) - 1 )
   p = 1
   Do Until p = Count
      Ret(p) = Mid(Text, RetVal(p - 1) + 1, RetVal(p) - RetVal(p - 1) - 1 )
      p += 1
   Loop
   Ret(Count) = Mid(Text, RetVal(Count - 1) + 1)
End Sub

Function CoordFromString(text As String) As Coord
    Dim As Integer x, y
    Dim As String s()
    Split text, ":", 2, s()
    x = Valint(s(0))
    y = Valint(s(1))
    Dim c As Coord = Coord(x, y)
    Return c
End Function
