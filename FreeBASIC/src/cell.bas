#include once "../inc/cell.bi"

Function CellToString(c As Cell) As String
    If c = Dead Then
        Return "   |"
    End If
    Return " x |"
End Function
