#include once "../inc/hashtable.bi"

'Adapted from:
'https://www.freebasic.net/forum/viewtopic.php?t=24358

'Fixed sized hashtable based on the Robin Hood algorithm.
'Accepts (most of) the FreeBasic primative types.
'Set types required in the macro
#macro robinhoodmacro(typename,keytype,valuetype)

constructor typename(size as ulongint,histogramsize as ulong=128)
   tablemask=size-1
   if (size and tablemask)<>0 then error(1)   'Size 2,4,8,16.....
   redim table(tablemask),dishisto(histogramsize-1)
end constructor

destructor typename()
   erase table,dishisto
end destructor

'load and save not offered for zstring and wstring as key or value
#if not(typeof(keytype)=typeof(zstring) or typeof(keytype)=typeof(wstring) _
   or typeof(valuetype)=typeof(zstring) or typeof(valuetype)=typeof(wstring))
function typename.save(filename as string) as boolean
   var ff=freefile()
   var ok=open(filename, for binary,access write, as ff)
   if ok<>0 then
      close(ff)
      return false
   end if
   ok or=put(#ff,1,count)
   ok or=put(#ff,,tablemask)
   ok or=put(#ff,,maxdis)
   ok or=put(#ff,,ubound(dishisto))
   ok or=put(#ff,,dishisto())
   #if typeof(keytype)=typeof(string) and typeof(valuetype)=typeof(string)
      for i as ulongint =0 to tablemask
         ok or=put(#ff,,table(i).hash)
         write #ff,table(i).key,table(i).value
         ok or=err()
      next
   #elseif typeof(keytype)=typeof(string)
      for i as ulongint =0 to tablemask
         ok or=put(#ff,,table(i).hash)
         write #ff,table(i).key
         ok or=err()
         ok or=put(#ff,,table(i).value)
      next
   #elseif typeof(valuetype)=typeof(string)
      for i as ulongint =0 to tablemask
         ok or=put(#ff,,table(i).hash)
         ok or=put(#ff,,table(i).key)
         write #ff,table(i).value
         ok or=err()
      next
   #else
      ok or=put(#ff,,table())
   #endif
   close(ff)
   return ok=0
end function

function typename.load(filename as string) as boolean
   var ff=freefile()
   var ok=open(filename, for binary,access read, as ff)
   if ok<>0 then
      close(ff)
      return false
   end if
   ok or=get(#ff,1,count)
   ok or=get(#ff,,tablemask)
   ok or=get(#ff,,maxdis)
   dim as integer dhtop
   ok or=get(#ff,,dhtop)
   redim dishisto(dhtop)
   ok or=err()
   redim table(tablemask)
   ok or=err()
   ok or=get(#ff,,dishisto())
   #if typeof(keytype)=typeof(string) and typeof(valuetype)=typeof(string)
      for i as ulongint =0 to tablemask
         ok or=get(#ff,,table(i).hash)
         input #ff,table(i).key,table(i).value
         ok or=err()
      next
   #elseif typeof(keytype)=typeof(string)
      for i as ulongint =0 to tablemask
         ok or=get(#ff,,table(i).hash)
         input #ff,table(i).key
         ok or=err()
         ok or=get(#ff,,table(i).value)
      next
   #elseif typeof(valuetype)=typeof(string)
      for i as ulongint =0 to tablemask
         ok or=get(#ff,,table(i).hash)
         ok or=get(#ff,,table(i).key)
         input #ff,table(i).value
         ok or=err()
      next
   #else
      ok or=get(#ff,,table())
   #endif
   close(ff)
   return ok=0
end function
#endif

function typename.getvalue(key as keytype,byref value as valuetype) as boolean
   var hash=mix13(key)
   for i as ulongint =0 to maxdis
      var idx=(i+hash) and tablemask
      if table(idx).hash=hash andalso table(idx).key=key then
         value=table(idx).value
         return true
      end if
      if table(idx).hash=0 then return false
   next
   return false
end function

function typename.exists(key as keytype) as boolean
   var hash=mix13(key)
   for i as ulongint =0 to maxdis
      var idx=(i+hash) and tablemask
      if table(idx).hash=hash andalso table(idx).key=key then return true
      if table(idx).hash=0 then return false
   next
   return false
end function

function typename.undatevalue(key as keytype,value as valuetype) as boolean
   var hash=mix13(key)
   for i as ulongint =0 to maxdis
      var idx=(i+hash) and tablemask
      if table(idx).hash=hash andalso table(idx).key=key then
         table(idx).value=value
         return true
      end if
      if table(idx).hash=0 then return false
   next
   return false
end function

function typename.putvalueonce(key as keytype,value as valuetype) as boolean
   if count>tablemask then return false   'there are no empty slots so return
   var hash=mix13(key)
   var idx=hash
   do
      idx and=tablemask
      var d1=((idx-hash) and tablemask)          'carried item displacement
      if table(idx).hash=0 then               'an empty slot
         inchistogram(d1)                  'update the displacement histogram
         table(idx).hash=hash               'insert
         table(idx).key=key
         table(idx).value=value
         count+=1
         return true
      end if
      var d2=((idx-table(idx).hash) and tablemask)   'displacement of item in the slot
      if d2<d1 then                  'then dump carried item (swap)
         dechistogram(d2)            'remove slot item displacement from the histogram
         inchistogram(d1)            'put the dumped item's displacement into the histogram
         var t=table(idx)
         table(idx).hash=hash
         table(idx).key=key
         table(idx).value=value
         hash=t.hash
         key=t.key
         value=t.value
      end if
      idx+=1                        'move on to the next slot
   loop
   return true
end function

function typename.putvalue(key as keytype,value as valuetype) as boolean
   if undatevalue(key,value) then return true
   return putvalueonce(key,value)
end function

sub typename.remove(key as keytype)
   var hash=mix13(key)
   for i as ulongint =0 to maxdis
      var idx=(i+hash) and tablemask
      if table(idx).hash=hash andalso table(idx).key=key then
         dechistogram((idx-hash) and tablemask)
         do
            var idxplus=(idx+1) and tablemask
            var s=table(idxplus)
            if s.hash=0 then exit do
            var d=(idxplus-s.hash) and tablemask
            if d=0 then exit do
            dechistogram(d)
            inchistogram(d-1)
            table(idx)=s
            idx=idxplus
         loop
         table(idx).hash=0
         count-=1
         return
      end if
      if table(idx).hash=0 then return
   next
end sub

sub typename.inchistogram(dis as ulong)
   if dis>ubound(dishisto) then redim preserve dishisto(dis+32)
   dishisto(dis)+=1
   if dis>maxdis then maxdis=dis
end sub

sub typename.dechistogram(dis as ulong)
   dishisto(dis)-=1
   if (dishisto(dis)=0) and (maxdis=dis) then
      while maxdis>0
         maxdis-=1
         if dishisto(maxdis)>0 then return
      wend
   end if
end sub

function typename.mix13(k as keytype) as ulongint
   dim as ulongint h
   #if typeof(keytype)=typeof(byte) or typeof(keytype)=typeof(ubyte) or typeof(keytype)=typeof(short) _
   or typeof(keytype)=typeof(ushort) or typeof(keytype)=typeof(long) or typeof(keytype)=typeof(ulong) _
   or typeof(keytype)=typeof(integer) or typeof(keytype)=typeof(uinteger) or typeof(keytype)=typeof_
   (longint) or typeof(keytype)=typeof(ulongint)
      h=k
   #elseif typeof(keytype)=typeof(single)
      h=*cast(ulong ptr,@k)
   #elseif typeof(keytype)=typeof(double)
      h=*cast(ulongint ptr,@k)
   #elseif typeof(keytype)=typeof(string) or typeof(keytype)=typeof(zstring) or typeof(keytype)=typeof(wstring)
      for i as ulong=1 to len(k)
         h+=asc(k,i)
         h*=&h5320B74ECA44ADACULL
         h xor=h shr 16
      next
   #else
      #error
   #endif
   h+=&h9E3779B97F4A7C15ULL
     h = ( h xor ( h shr 30 ) ) * &hBF58476D1CE4E5B9ULL
    h = ( h xor ( h shr 27 ) ) * &h94D049BB133111EBULL
   h xor=h shr 31
   if h=0 then h=1
   return h
end function
#endmacro

robinhoodmacro(Hashtable, String, Integer)
