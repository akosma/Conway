#include once "fbcunit/inc/fbcunit.bi"
#include once "inc/world.bi"
#include once "inc/coord.bi"

SUITE(Conway)
    TEST(BlockTest)
        Dim aliveCells(Any) As Coord
        Block (Coord(0, 0), aliveCells())
        Dim originalWorld As World Ptr = New World(5, aliveCells())
        Dim nextWorld As World Ptr = originalWorld->Evolve()
        CU_ASSERT(originalWorld->Equals(nextWorld))
    END_TEST

    TEST(TubTest)
        Dim aliveCells(Any) As Coord
        Tub (Coord(0, 0), aliveCells())
        Dim originalWorld As World Ptr = New World(5, aliveCells())
        Dim nextWorld As World Ptr = originalWorld->Evolve()
        CU_ASSERT(originalWorld->Equals(nextWorld))
    END_TEST

    TEST(BlinkerTest)
        Dim aliveCells(Any) As Coord
        Blinker (Coord(0, 1), aliveCells())
        Dim originalWorld As World Ptr = New World(3, aliveCells())
        Dim gen1 As World Ptr = originalWorld->Evolve()
        Dim expectedAlive(3) As Coord = { Coord(1, 0), Coord(1, 1), Coord(1, 2) }
        Dim expectedWorld As World Ptr = new World(3, expectedAlive())
        CU_ASSERT(expectedWorld->Equals(gen1))
        Dim gen2 As World Ptr = gen1->Evolve()
        CU_ASSERT(originalWorld->Equals(gen2))
    END_TEST
END_SUITE

fbcu.run_tests
