#include once "hashtable.bi"
#include once "coord.bi"

Type World
	size As Integer
	cells As Hashtable Ptr

	Declare Constructor()
	Declare Constructor(size As Integer, aliveCells(Any) As Coord)
	Declare Destructor()
	Declare Function Evolve() As World Ptr
	Declare Function ToString() As String
	Declare Function Equals(w As World Ptr) As Boolean
End Type

Declare Sub Blinker(c As Coord, blinker(Any) As Coord)
Declare Sub Beacon(c As Coord, aliveCells(Any) As Coord)
Declare Sub Glider(c As Coord, aliveCells(Any) As Coord)
Declare Sub Block(c As Coord, aliveCells(Any) As Coord)
Declare Sub Tub(c As Coord, aliveCells(Any) As Coord)
