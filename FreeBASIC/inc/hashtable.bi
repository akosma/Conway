'Adapted:
'https://www.freebasic.net/forum/viewtopic.php?t=24358

'Fixed sized hashtable based on the Robin Hood algorithm.
'Accepts (most of) the FreeBasic primative types.
'Set types required in the macro
#macro robinhoodmacrobi(typename,keytype,valuetype)

type rh4item
   hash as ulongint
   key as keytype
   value as valuetype
end type

type typename
   count as ulongint         'total items in the hash table
   tablemask as ulongint      'indexing mask for the table
   maxdis as ulongint         'current maximum displacement
   dishisto(any) as ulongint   'displacement histogram
   table(any) as rh4item      'items
   ' size should be a power of 2, eg. 2,4,8,16
   declare constructor (size as ulongint,histogramsize as ulong=128)
   declare destructor()

   declare function getvalue(key as keytype,byref value as valuetype) as boolean
   declare function putvalue(key as keytype,value as valuetype) as boolean
   declare function exists(key as keytype) as boolean
   declare function putvalue(key as keytype) as boolean
   declare function undatevalue(key as keytype,value as valuetype) as boolean
   declare function putvalueonce(key as keytype,value as valuetype) as boolean
   declare sub remove(key as keytype)
'other
   declare sub inchistogram(dis as ulong)
   declare sub dechistogram(dis as ulong)
   declare function mix13(h as keytype) as ulongint
#if not(typeof(keytype)=typeof(zstring) or typeof(keytype)=typeof(wstring) _
   or typeof(valuetype)=typeof(zstring) or typeof(valuetype)=typeof(wstring))
   declare function save(filename as string) as boolean
   declare function load(filename as string) as boolean
#endif
end type

#endmacro

robinhoodmacrobi(Hashtable, String, Integer)
