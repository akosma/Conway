Type Coord
	x As Integer
	y As Integer

	Declare Constructor()
	Declare Constructor(x As Integer, y As Integer)
    Declare Function ToString() As String
	Declare Function Equals(c As Coord) As Boolean
End Type

Declare Function CoordFromString(input As String) As Coord
