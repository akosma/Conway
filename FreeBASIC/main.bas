#include once "inc/coord.bi"
#include once "inc/world.bi"

' https://www.freebasic.net/forum/viewtopic.php?t=6669
#define SIGINT 2
#define SIG_IGN cptr(sighandler_t, 1)

Type sighandler_t As Sub cdecl (ByVal As Integer)

Declare Function signal cdecl alias "signal" ( _
                                               ByVal signum As Integer, _
                                               ByVal handler As sighandler_t _
                                             ) As sighandler_t

Sub foo cdecl (ByVal sig As Integer)
    Cls()
    System()
End Sub

If signal(SIGINT, @foo) = SIG_IGN Then signal(SIGINT, SIG_IGN)

Dim aliveCells(Any) As Coord
Blinker (Coord(0, 1), aliveCells())
Beacon (Coord(10, 10), aliveCells())
Glider (Coord(4, 5), aliveCells())
Block (Coord(1, 10), aliveCells())
Block (Coord(18, 3), aliveCells())
Tub (Coord(6, 1), aliveCells())
Dim w As World Ptr = New World(30, aliveCells())
Dim generation As Integer = 0

While InKey <> "1"
    Cls()
    generation = generation + 1
    Print w->ToString()
    Print "Generation " & generation
    Sleep(500)
    Dim w2 As World Ptr
    w2 = w->Evolve()
    Delete w
    w = w2
Wend
