Partial Public Module ConwayLib
    Public Structure Coord
        Private ReadOnly _x As Integer
        Private ReadOnly _y As Integer

        Public ReadOnly Property X As Integer
            Get
                Return _x
            End Get
        End Property

        Public ReadOnly Property Y As Integer
            Get
                Return _y
            End Get
        End Property

        Sub New(x As Integer, y As Integer)
            Me.New()
            _x = x
            _y = y
        End Sub

        Public Overrides Function ToString() As String
            Return String.Format("{0}:{1}", _x, _y)
        End Function

        Public Overrides Function Equals(obj As Object) As Boolean
            If IsNothing(obj) Then
                Throw New ArgumentNullException(obj)
            End If
            Dim coord As Coord
            coord = CType(obj, Coord)
            return _x = coord.X And _y = coord.Y
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return ToString().GetHashCode()
        End Function

        Public Shared Operator =(h1 As Coord, h2 As Coord) As Boolean
            Return h1.Equals(h2)
        End Operator

        Public Shared Operator <>(h1 As Coord, h2 As Coord) As Boolean
            Return Not h1.Equals(h2)
        End Operator
    End Structure
End Module