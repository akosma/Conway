Partial Public Module ConwayLib
    Public Enum Cell
        Alive
        Dead
    End Enum

    Public Function ToEnumString(cell As Cell) As String
        If cell = Cell.Alive Then
            Return " x |"
        End If
        Return "   |"
    End Function
End Module
