Imports System.Text

Public Partial Module ConwayLib
    Public Class World
        Private ReadOnly _cells As Dictionary(Of Coord, Cell) = New Dictionary(Of Coord,Cell)()
        Private ReadOnly _size As Integer

        Sub New (size As Integer, aliveCells As ICollection(Of Coord))
            _size = size
            For a = 0 To _size - 1
                For b = 0 To _size - 1
                    Dim coord = New Coord(a, b)
                    If aliveCells.Contains(coord) Then
                        _cells(coord) = Cell.Alive
                    Else
                        _cells(coord) = Cell.Dead
                    End If
                Next
            Next
        End Sub

        Public Function Evolve() As World
            Dim aliveCells = new List(Of Coord)
            For Each item In _cells
                Dim coord = item.Key
                Dim cell = item.Value
                Dim count = 0
                For a = -1 To 1
                    For b = -1 To 1
                        Dim currentCoord = New Coord(coord.X + a, coord.Y + b)
                        If currentCoord <> coord Then
                            If _cells.ContainsKey(currentCoord) Then
                                If _cells(currentCoord) = Cell.Alive Then
                                    count += 1
                                End If
                            End If
                        End If
                    Next
                Next

                Select Case cell
                    Case Cell.Alive
                        If count = 2 Or count = 3 Then
                            aliveCells.Add(coord)
                        End If
                    Case Cell.Dead
                        If count = 3 Then
                            aliveCells.Add(coord)
                        End If
                End Select
            Next

            Return New World(_size, aliveCells)
        End Function

        Public Overrides Function ToString() As String
            Dim builder = new StringBuilder(vbCrLf)
            For a = 0 To _size - 1
                If a = 0 Then
                    'First line with coordinates
                    builder.Append("    ")
                    For b = 0 To _size - 1
                        builder.AppendFormat("{0,3}|", b)
                    Next
                    builder.Append(vbCrLf)
                End If
                builder.AppendFormat("{0,3}|", a)
                For b = 0 To _size - 1
                    Dim coord = New Coord(b, a)
                    Dim cell = _cells(coord)
                    builder.Append(ToEnumString(cell))
                Next
                builder.Append(vbCrLf)
            Next
            Return builder.ToString()
        End Function

        Public Overrides Function Equals(obj As Object) As Boolean
            Return Not IsNothing(obj) And Equals(CType(obj, World))
        End Function

        Private Overloads Function Equals(other As World) As Boolean
            Return _size = other._size And _cells.SequenceEqual(other._cells)
        End Function

        Public Overrides Function GetHashCode() As Integer
            Return HashCode.Combine(_size, _cells)
        End Function

        Public Shared Operator =(ByVal h1 As World,
                                 ByVal h2 As World) As Boolean
            Return h1.Equals(h2)
        End Operator

        Public Shared Operator <>(ByVal h1 As World,
                                  ByVal h2 As World) As Boolean
            Return Not h1.Equals(h2)
        End Operator

        Public Shared Function Blinker(coord As Coord) As List(Of Coord)
            Dim list = New List(Of Coord)
            list.Add(New Coord(coord.X, coord.Y))
            list.Add(New Coord(coord.X + 1, coord.Y))
            list.Add(New Coord(coord.X + 2, coord.Y))
            Return list
        End Function

        Public Shared Function Beacon(coord As Coord) As List(Of Coord)
            Dim list = New List(Of Coord)
            list.Add(New Coord(coord.X, coord.Y))
            list.Add(New Coord(coord.X + 1, coord.Y))
            list.Add(New Coord(coord.X, coord.Y + 1))
            list.Add(New Coord(coord.X + 1, coord.Y + 1))
            list.Add(New Coord(coord.X + 2, coord.Y + 2))
            list.Add(New Coord(coord.X + 3, coord.Y + 2))
            list.Add(New Coord(coord.X + 2, coord.Y + 3))
            list.Add(New Coord(coord.X + 3, coord.Y + 3))
            Return list
        End Function

        Public Shared Function Glider(coord As Coord) As List(Of Coord)
            Dim list = New List(Of Coord)
            list.Add(New Coord(coord.X + 2, coord.Y + 2))
            list.Add(New Coord(coord.X + 1, coord.Y + 2))
            list.Add(New Coord(coord.X, coord.Y + 2))
            list.Add(New Coord(coord.X + 2, coord.Y + 1))
            list.Add(New Coord(coord.X + 1, coord.Y))
            Return list
        End Function

        Public Shared Function Block(coord As Coord) As List(Of Coord)
            Dim list = New List(Of Coord)
            list.Add(New Coord(coord.X, coord.Y))
            list.Add(New Coord(coord.X + 1, coord.Y))
            list.Add(New Coord(coord.X, coord.Y + 1))
            list.Add(New Coord(coord.X + 1, coord.Y + 1))
            Return list
        End Function

        Public Shared Function Tub(coord As Coord) As List(Of Coord)
            Dim list = New List(Of Coord)
            list.Add(New Coord(coord.X + 1, coord.Y))
            list.Add(New Coord(coord.X, coord.Y + 1))
            list.Add(New Coord(coord.X + 2, coord.Y + 1))
            list.Add(New Coord(coord.X + 1, coord.Y + 2))
            Return list
        End Function
    End Class
End Module
