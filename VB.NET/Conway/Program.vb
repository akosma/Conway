Imports System.Threading
Imports ConwayLib

Module Program
    Private Sub HandleCancelKeyPress
        Console.Clear()
        Environment.Exit(0)
    End Sub

    Sub Main(args As String())
        AddHandler Console.CancelKeyPress, AddressOf HandleCancelKeyPress

        Dim alive = New List(Of Coord)
        alive.AddRange(World.Blinker(New Coord(0, 1)))
        alive.AddRange(World.Beacon(New Coord(10, 10)))
        alive.AddRange(World.Glider(New Coord(4, 5)))
        alive.AddRange(World.Block(New Coord(1, 10)))
        alive.AddRange(World.Block(New Coord(18, 3)))
        alive.AddRange(World.Tub(New Coord(6, 1)))
        Dim w = New World(30, alive)
        Dim generation = 0
        Console.Clear()

        Do While True
            generation += 1
            Console.WriteLine(w)
            Console.WriteLine("Generation {0}", generation)
            w = w.Evolve()
            Thread.Sleep(500)
            Console.Clear()
        Loop
    End Sub
End Module
