Imports Xunit
Imports ConwayLib

Namespace Tests
    Public Class ConwayTests
        <Fact>
        Sub Blinker()
            Dim alive = World.Blinker(New Coord(0, 1))
            Dim original = New World(3, alive)
            Dim gen1 = original.Evolve()
            Dim expectedAlive = New List(Of Coord)
            expectedAlive.Add(New Coord(1, 0))
            expectedAlive.Add(New Coord(1, 1))
            expectedAlive.Add(New Coord(1, 2))
            Dim expected = New World(3, expectedAlive)
            Assert.Equal(expected, gen1)
            Dim gen2 = gen1.Evolve()
            Assert.Equal(original, gen2)
        End Sub

        <Fact>
        Sub Block()
            Dim alive = World.Block(New Coord(0, 0))
            Dim original = New World(5, alive)
            Dim nextWorld = original.Evolve()
            Assert.Equal(nextWorld, original)
        End Sub

        <Fact>
        Sub Tub()
            Dim alive = World.Tub(New Coord(0, 0))
            Dim original = New World(5, alive)
            Dim nextWorld = original.Evolve()
            Assert.Equal(nextWorld, original)
        End Sub
    End Class
End Namespace
