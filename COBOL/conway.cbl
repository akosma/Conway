      * Conway Game of Life in COBOL
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Conway.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 SizeValue PIC 9(2) VALUE 30.
       01 X PIC 99 VALUE ZERO.
       01 Y PIC 99 VALUE ZERO.
       01 A PIC S99 VALUE ZERO.
       01 B PIC S99 VALUE ZERO.
       01 Temp PIC 99 VALUE ZERO.
       01 MinA PIC S99 VALUE ZERO.
       01 MinB PIC S99 VALUE ZERO.
       01 MaxA PIC S99 VALUE ZERO.
       01 MaxB PIC S99 VALUE ZERO.
       01 Counter PIC 9 VALUE ZERO.
       01 DisplayX PIC Z(2)9 VALUE ZERO.
       01 DisplayY PIC Z(2)9 VALUE ZERO.
       01 Generation PIC 999 VALUE ZERO.
       01 Gen PIC Z(3)9 VALUE ZERO.
       01 World.
           02 Row OCCURS 30 TIMES.
               03 Cell PIC 9 VALUE ZERO OCCURS 30 TIMES.
       01 WorldCopy.
           02 RowCopy OCCURS 30 TIMES.
               03 CellCopy PIC 9 VALUE ZERO OCCURS 30 TIMES.

       PROCEDURE DIVISION.
       BEGIN.
           PERFORM INIT-WORLD
           PERFORM FOREVER
               CALL 'SYSTEM' USING 'clear'
               PERFORM DISPLAY-TABLE
               PERFORM DISPLAY-GENERATION
               PERFORM EVOLVE
               CONTINUE AFTER 0.5 SECONDS
           END-PERFORM
           EXIT SECTION.

       INIT-WORLD.
      * Blinker 0, 1
           MOVE 1 TO Cell(1, 2)
           MOVE 1 TO Cell(2, 2)
           MOVE 1 TO Cell(3, 2)
      * Beacon 10, 10
           MOVE 1 TO Cell(11, 11)
           MOVE 1 TO Cell(11, 12)
           MOVE 1 TO Cell(12, 11)
           MOVE 1 TO Cell(12, 12)
           MOVE 1 TO Cell(13, 13)
           MOVE 1 TO Cell(13, 14)
           MOVE 1 TO Cell(14, 13)
           MOVE 1 TO Cell(14, 14)
      * Glider, 4, 5
           MOVE 1 TO Cell(6, 6)
           MOVE 1 TO Cell(7, 7)
           MOVE 1 TO Cell(5, 8)
           MOVE 1 TO Cell(6, 8)
           MOVE 1 TO Cell(7, 8)
      * Block 1, 10
           MOVE 1 TO Cell(2, 11)
           MOVE 1 TO Cell(2, 12)
           MOVE 1 TO Cell(3, 11)
           MOVE 1 TO Cell(3, 12)
      * Block 18, 3
           MOVE 1 TO Cell(19, 4)
           MOVE 1 TO Cell(19, 5)
           MOVE 1 TO Cell(20, 4)
           MOVE 1 TO Cell(20, 5)
      * Tub 6, 1
           MOVE 1 TO Cell(8, 2)
           MOVE 1 TO Cell(7, 3)
           MOVE 1 TO Cell(9, 3)
           MOVE 1 TO Cell(8, 4).

       DISPLAY-TABLE.
           DISPLAY " ".
           DISPLAY "    " WITH NO ADVANCING.
           PERFORM VARYING X FROM 1 BY 1 UNTIL X > SizeValue
               IF X = 1
                   PERFORM VARYING A FROM 1 BY 1 UNTIL A > SizeValue
                       MOVE A TO Temp
                       SUBTRACT 1 FROM Temp
                       MOVE Temp TO DisplayY
                       DISPLAY DisplayY WITH NO ADVANCING
                       DISPLAY "|" WITH NO ADVANCING
                   END-PERFORM
                   DISPLAY " "
               END-IF
               PERFORM VARYING Y FROM 1 BY 1 UNTIL Y > SizeValue
                   IF Y = 1
                       MOVE X TO Temp
                       SUBTRACT 1 FROM Temp
                       MOVE Temp TO DisplayX
                       DISPLAY DisplayX WITH NO ADVANCING
                       DISPLAY "|" WITH NO ADVANCING
                   END-IF
                   IF Cell(Y, X) = 1
                       DISPLAY " x |" WITH NO ADVANCING
                   ELSE
                       DISPLAY "   |" WITH NO ADVANCING
                   END-IF
               END-PERFORM
               DISPLAY " "
           END-PERFORM.

       DISPLAY-GENERATION.
           ADD 1 TO Generation
           MOVE Generation TO Gen
           DISPLAY " "
           DISPLAY "Generation ", Gen
           DISPLAY " ".

       EVOLVE.
           PERFORM VARYING X FROM 1 BY 1 UNTIL X > SizeValue
               PERFORM VARYING Y FROM 1 BY 1 UNTIL Y > SizeValue
                   MOVE Cell(X, Y) TO CellCopy(X, Y)
                   MOVE ZERO TO Cell(X, Y)
               END-PERFORM
           END-PERFORM

           PERFORM VARYING X FROM 1 BY 1 UNTIL X > SizeValue
               PERFORM VARYING Y FROM 1 BY 1 UNTIL Y > SizeValue
                   MOVE ZERO TO Counter
                   MOVE X TO MinA
                   SUBTRACT 1 FROM MinA
                   MOVE X TO MaxA
                   ADD 1 TO MaxA
                   PERFORM VARYING A FROM MinA BY 1 UNTIL A > MaxA
                       MOVE Y TO MinB
                       SUBTRACT 1 FROM MinB
                       MOVE Y TO MaxB
                       ADD 1 TO MaxB
                       PERFORM VARYING B FROM MinB BY 1
                               UNTIL B > MaxB
                           IF A >= 1 AND B >= 1
                               AND A <= SizeValue
                               AND B <= SizeValue
                                   ADD CellCopy(A, B) TO Counter
                           END-IF
                       END-PERFORM
                   END-PERFORM
                   SUBTRACT CellCopy(X, Y) FROM Counter
                   EVALUATE CellCopy(X, Y)
                   WHEN 1
                       IF Counter = 2 OR Counter = 3
                           MOVE 1 TO Cell(X, Y)
                       END-IF
                   WHEN OTHER
                       IF Counter = 3
                           MOVE 1 TO Cell(X, Y)
                       END-IF
                   END-EVALUATE
               END-PERFORM
           END-PERFORM.
