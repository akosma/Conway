Option Explicit

TestBlock
TestTub
TestBlinker

Sub TestBlock
    Dim original, nextWorld
    ReDim alive(-1)

    Block alive, (New Coord)(0, 0)
    Set original = (New World)(5, alive)
    Set nextWorld = original.Evolve
    assert_true original.Equal(nextWorld), "Worlds should be equal"
End Sub

Sub TestTub
    Dim original, nextWorld
    ReDim alive(-1)

    Tub alive, (New Coord)(0, 0)
    Set original = (New World)(5, alive)
    Set nextWorld = original.Evolve
    assert_true original.Equal(nextWorld), "Worlds should be equal"
End Sub

Sub TestBlinker
    Dim original, expected, gen1, gen2, expectedAlive
    ReDim alive(-1)

    Blinker alive, (New Coord)(0, 1)
    Set original = (New World)(3, alive)
    Set gen1 = original.Evolve
    expectedAlive = Array("1:0", "1:1", "1:2")

    Set expected = (New World)(3, expectedAlive)
    assert_true gen1.Equal(expected), "Worlds should be equal"

    Set gen2 = gen1.Evolve
    assert_true gen2.Equal(original), "Worlds should be equal"
End Sub
