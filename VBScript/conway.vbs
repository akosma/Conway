Option Explicit

Main

' Source:
' https://stackoverflow.com/a/61071064
Sub Cls
    WScript.StdOut.Write Chr(27) & "[34A"
    WScript.StdOut.Write Chr(27) & "[2J"
End Sub

' Endless loop printing world
Sub Main
    Dim generation, w, c
    ReDim aliveCells(-1)
    generation = 0

    Set c = (new Coord)(0, 1)
    Blinker aliveCells, c
    Beacon aliveCells, c.Init(10, 10)
    Glider aliveCells, c.Init(4, 5)
    Block aliveCells, c.Init(1, 10)
    Block aliveCells, c.Init(18, 3)
    Tub aliveCells, c.Init(6, 1)

    Set w = (New World)(29, aliveCells)

    Do
        Cls
        WScript.StdOut.Write vbCRLF & w.ToString
        generation = generation + 1
        WScript.StdOut.Write vbCRLF & "Generation " & generation
        WScript.Sleep 500
        Set w = w.Evolve
    Loop
End Sub
