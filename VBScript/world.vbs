Option Explicit

Function Format(number)
    Dim strNumber
    strNumber = CStr(number)
    If Len(strNumber) = 1 Then
        Format = "  " & strNumber & "|"
    Else
        Format = " " & strNumber & "|"
    End If
End Function

Sub ArrayAdd(arr, obj)
    ReDim Preserve arr(UBound(arr) + 1)
    arr(UBound(arr)) = obj
End Sub

Function ArrayExists(haystack, needle)
    Dim item
    For Each item In haystack
        If item = needle Then
            ArrayExists = True
            Exit Function
        End If
    Next
    ArrayExists = False
End Function

Class World
    Public cells
    Public size

    Public Default Function Init(s, aliveCells)
        Dim str, a, b
        size = s
        Set cells = CreateObject("Scripting.Dictionary")
        For a = 0 To size
            For b = 0 To size
                str = (New Coord)(a, b).ToString
                If ArrayExists(aliveCells, str) Then
                    cells.Add str, CellAlive
                Else
                    cells.Add str, CellDead
                End If
            Next
        Next

        Set Init = Me
    End Function

    Public Function Equal(w)
        If size <> w.size Then
            Equal = False
            Exit Function
        End If

        Dim key, value
        For Each key In cells.Keys
            If Not w.cells.Exists(key) Then
                Equal = False
                Exit Function
            End If

            value = cells.Item(key)
            If Not w.cells.Item(key) = value Then
                Equal = False
                Exit Function
            End If
        Next
        Equal = True
    End Function

    ' Evolve world to the next generation
    Public Function Evolve
        Dim counter, x, y, a, b, key
        ReDim aliveCells(-1)
        For x = 0 To size
            For y = 0 To size
                counter = 0
                For a = x - 1 To x + 1
                    For b = y - 1 To y + 1
                        If a >= 0 And b >= 0 And a <= size And b <= size Then counter = counter + cells.Item((New Coord)(a, b).ToString)
                    Next
                Next
                key = (New Coord)(x, y).ToString
                counter = counter - cells.Item(key)
                Select Case cells.Item(key)
                    Case CellAlive
                        If counter = 2 Or counter = 3 Then
                            ArrayAdd aliveCells, key
                        End If
                    Case CellDead
                        If counter = 3 Then
                            ArrayAdd aliveCells, key
                        End If
                End Select
            Next
        Next
        Set Evolve = (New World)(size, aliveCells)
    End Function

    Public Function ToString
        Dim a, b, str
        str = ""
        For a = 0 To size
            If a = 0 Then
                ' Add first line with coordinates
                For b = 0 To size
                    If b = 0 Then
                        str = str & "    "
                    End If
                    str = str & Format(b)
                Next
                str = str & vbCRLF
            End If
            str = str & Format(a)
            For b = 0 To size
                If cells.Item((New Coord)(b, a).ToString) = CellDead Then
                    str = str & "   |"
                Else
                    str = str & " x |"
                End If
            Next
            str = str & vbCRLF
        Next
        ToString = str
    End Function
End Class

Sub Blinker(aliveCells, c)
    Dim c1, c2
    Set c1 = (New Coord)(c.x + 1, c.y)
    Set c2 = (New Coord)(c.x + 2, c.y)
    ArrayAdd aliveCells, c.ToString
    ArrayAdd aliveCells, c1.ToString
    ArrayAdd aliveCells, c2.ToString
End Sub

Sub Beacon(aliveCells, c)
    Dim c1, c2, c3, c4, c5, c6, c7
    Set c1 = (New Coord)(c.x + 1, c.y)
    Set c2 = (New Coord)(c.x,     c.y + 1)
    Set c3 = (New Coord)(c.x + 1, c.y + 1)
    Set c4 = (New Coord)(c.x + 2, c.y + 2)
    Set c5 = (New Coord)(c.x + 3, c.y + 2)
    Set c6 = (New Coord)(c.x + 2, c.y + 3)
    Set c7 = (New Coord)(c.x + 3, c.y + 3)
    ArrayAdd aliveCells, c.ToString
    ArrayAdd aliveCells, c1.ToString
    ArrayAdd aliveCells, c2.ToString
    ArrayAdd aliveCells, c3.ToString
    ArrayAdd aliveCells, c4.ToString
    ArrayAdd aliveCells, c5.ToString
    ArrayAdd aliveCells, c6.ToString
    ArrayAdd aliveCells, c7.ToString
End Sub

Sub Glider(aliveCells, c)
    Dim c1, c2, c3, c4, c5
    Set c1 = (New Coord)(c.x + 2, c.y + 2)
    Set c2 = (New Coord)(c.x + 1, c.y + 2)
    Set c3 = (New Coord)(c.x,     c.y + 2)
    Set c4 = (New Coord)(c.x + 2, c.y + 1)
    Set c5 = (New Coord)(c.x + 1, c.y)
    ArrayAdd aliveCells, c1.ToString
    ArrayAdd aliveCells, c2.ToString
    ArrayAdd aliveCells, c3.ToString
    ArrayAdd aliveCells, c4.ToString
    ArrayAdd aliveCells, c5.ToString
End Sub

Sub Block(aliveCells, c)
    Dim c1, c2, c3
    Set c1 = (New Coord)(c.x + 1, c.y)
    Set c2 = (New Coord)(c.x,     c.y + 1)
    Set c3 = (New Coord)(c.x + 1, c.y + 1)
    ArrayAdd aliveCells, c.ToString
    ArrayAdd aliveCells, c1.ToString
    ArrayAdd aliveCells, c2.ToString
    ArrayAdd aliveCells, c3.ToString
End Sub

Sub Tub(aliveCells, c)
    Dim c1, c2, c3, c4
    Set c1 = (New Coord)(c.x + 1, c.y)
    Set c2 = (New Coord)(c.x,     c.y + 1)
    Set c3 = (New Coord)(c.x + 2, c.y + 1)
    Set c4 = (New Coord)(c.x + 1, c.y + 2)
    ArrayAdd aliveCells, c1.ToString
    ArrayAdd aliveCells, c2.ToString
    ArrayAdd aliveCells, c3.ToString
    ArrayAdd aliveCells, c4.ToString
End Sub
