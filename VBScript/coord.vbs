Option Explicit

Class Coord
	Public x, y

	Public Default Function Init(a, b)
		x = a
		y = b
		Set Init = Me
	End Function

	Public Function ToString
		ToString = CStr(x) & ":" & CStr(y)
	End Function
End Class

Function FromString(s)
	Dim parts
	parts = Split(s, ":")
	Set FromString = (New Coord)(parts(0), parts(1))
End Function
