Object subclass: Cell [
    | state |
    <comment: 'I am an object in a cell that is either alive or dead'>

    Cell class >> newAlive: alive [
        | c |
        c := super new.
        c initAlive: alive.
        ^c ]

    initAlive: alive [ state := alive. ]

    aliveValue := Cell newAlive: true.
    deadValue := Cell newAlive: false.

    printOn: stream [
        state
            ifTrue: [ ' x |' displayOn: stream ]
            ifFalse: [ '   |' displayOn: stream ] ] ]

Cell class extend [
    alive [ ^aliveValue ]
    dead [ ^deadValue ] ]
