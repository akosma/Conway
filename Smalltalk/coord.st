Object subclass: Coord [
    | x y |
    <comment: 'I represent a location in a world, containing a cell either alive or dead'>

    Coord class >> newWithX:x y:y [
        | c |
        c := super new.
        c initWithX:x y:y.
        ^c ]

    initWithX:xValue y:yValue [
        x := xValue.
        y := yValue ]

    printOn: stream [
        x displayOn: stream.
        ':' displayOn: stream.
        y displayOn: stream ]

    x [ ^x ]

    y [ ^y ]

    = arg [ ^ x = arg x and: [ y = arg y ] ]

    hash [ ^ self displayString hash ] ]
