#!/usr/local/bin/gst-sunit -f

FileStream fileIn: 'coord.st'.
FileStream fileIn: 'cell.st'.
FileStream fileIn: 'world.st'.

TestCase subclass: ConwayTests [
    testBlock [
        | alive original next |
        'testBlock' displayNl.
        alive := World block:(Coord newWithX:0 y:0).
        original := World newWithSize:5 aliveCells:alive.
        next := original evolve.
        self assert: next = original ]

    testTub [
        | alive original next |
        'testTub' displayNl.
        alive := World tub:(Coord newWithX:0 y:0).
        original := World newWithSize:5 aliveCells:alive.
        next := original evolve.
        self assert: next = original ]

    testBlinker [
        | alive original gen1 gen2 expectedAlive expected |
        'testBlinker' displayNl.
        alive := World blinker:(Coord newWithX:0 y:1).
        original := World newWithSize:3 aliveCells:alive.
        gen1 := original evolve.
        expectedAlive := Set new.
        expectedAlive add:(Coord newWithX:1 y:0).
        expectedAlive add:(Coord newWithX:1 y:1).
        expectedAlive add:(Coord newWithX:1 y:2).
        expected := World newWithSize:3 aliveCells:expectedAlive.
        self assert: expected = gen1.
        gen2 := gen1 evolve.
        self assert: original = gen2 ] ]

ConwayTests suite run.
