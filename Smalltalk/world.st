Object subclass: World [
    | size cells |
    <comment: 'I am the place where cells live and die'>

    World class >> newWithSize:size aliveCells:cells [
        | w |
        w := super new.
        w initWithSize:size aliveCells:cells.
        ^w ]

    initWithSize:newSize aliveCells:aliveCells [
        | coord interval |
        size := newSize.
        cells := Dictionary new.
        interval := Interval from: 0 to: size - 1.

        interval do: [ :a |
            interval do: [ :b |
                coord := Coord newWithX:a y:b.
                (aliveCells includes:coord)
                    ifTrue: [ cells at:coord put:Cell alive ]
                    ifFalse: [ cells at:coord put:Cell dead ] ] ] ]

    evolve [
        | alive |
        alive := Set new.
        cells keysAndValuesDo: [ :coord :cell |
            | count interval |
            interval := Interval from: -1 to: 1.
            count := 0.
            interval do: [ :a |
                interval do: [ :b |
                    | currentCoord |
                    currentCoord := Coord newWithX:(coord x + a) y:(coord y + b).
                    (coord = currentCoord) ifFalse: [ (cells includesKey:currentCoord) ifTrue: [
                            | currentCell |
                            currentCell := cells at: currentCoord.
                            currentCell = Cell alive ifTrue: [ count := count + 1 ] ] ] ] ].

            cell = Cell alive ifTrue: [ (count = 2 or: [ count = 3 ]) ifTrue: [ alive add: coord ] ].
            cell = Cell dead ifTrue: [ count = 3 ifTrue: [ alive add: coord ] ] ].

        ^ World newWithSize: size aliveCells: alive ]

    printOn: stream [
        | interval |
        interval := Interval from: 0 to: size - 1.
        interval do: [ :a |
            (a = 0)
                ifTrue: [
                    '    ' displayOn: stream.
                    interval do: [ :b |
                        (b printPaddedWith: Character space to:3) displayOn: stream.
                        '|' displayOn: stream ].
                    Character cr displayOn: stream.
                    Character lf displayOn: stream ].
            (a printPaddedWith: Character space to:3) displayOn: stream.
            '|' displayOn: stream.
            interval do: [ :b |
                | cell coord |
                coord := Coord newWithX:b y:a.
                cell := cells at:coord.
                cell displayOn: stream ].
            Character cr displayOn: stream.
            Character lf displayOn: stream ] ]

    size [ ^size ]

    cells [ ^cells ]

    = arg [ ^ size = arg size and: [ cells = arg cells ] ] ]

World class extend [
    blinker:coord [
        | result |
        result := Set new.
        result add:(Coord newWithX:coord x     y:coord y).
        result add:(Coord newWithX:coord x + 1 y:coord y).
        result add:(Coord newWithX:coord x + 2 y:coord y).
        ^result ]

    beacon:coord [
        | result |
        result := Set new.
        result add:(Coord newWithX:coord x     y:coord y).
        result add:(Coord newWithX:coord x + 1 y:coord y).
        result add:(Coord newWithX:coord x     y:coord y + 1).
        result add:(Coord newWithX:coord x + 1 y:coord y + 1).
        result add:(Coord newWithX:coord x + 2 y:coord y + 2).
        result add:(Coord newWithX:coord x + 3 y:coord y + 2).
        result add:(Coord newWithX:coord x + 2 y:coord y + 3).
        result add:(Coord newWithX:coord x + 3 y:coord y + 3).
        ^result ]

    glider:coord [
        | result |
        result := Set new.
        result add:(Coord newWithX:coord x + 2 y:coord y + 2).
        result add:(Coord newWithX:coord x + 1 y:coord y + 2).
        result add:(Coord newWithX:coord x     y:coord y + 2).
        result add:(Coord newWithX:coord x + 2 y:coord y + 1).
        result add:(Coord newWithX:coord x + 1 y:coord y).
        ^result ]

    block:coord [
        | result |
        result := Set new.
        result add:(Coord newWithX:coord x     y:coord y).
        result add:(Coord newWithX:coord x + 1 y:coord y).
        result add:(Coord newWithX:coord x     y:coord y + 1).
        result add:(Coord newWithX:coord x + 1 y:coord y + 1).
        ^result ]

    tub:coord [
        | result |
        result := Set new.
        result add:(Coord newWithX:coord x + 1 y:coord y).
        result add:(Coord newWithX:coord x     y:coord y + 1).
        result add:(Coord newWithX:coord x + 2 y:coord y + 1).
        result add:(Coord newWithX:coord x + 1 y:coord y + 2).
        ^result ] ]
