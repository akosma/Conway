#!/usr/local/bin/gst -f

FileStream fileIn: 'coord.st'.
FileStream fileIn: 'cell.st'.
FileStream fileIn: 'world.st'.

Object subclass: Conway [
    <comment: 'I am the entry point of the program'>

        Conway class >> main [
            | cells world generation |
            cells := World blinker:(Coord newWithX:0 y:1).
            cells addAll:(World beacon:(Coord newWithX:10 y:10)).
            cells addAll:(World glider:(Coord newWithX:4 y:5)).
            cells addAll:(World block:(Coord newWithX:1 y:10)).
            cells addAll:(World block:(Coord newWithX:18 y:3)).
            cells addAll:(World tub:(Coord newWithX:6 y:1)).
            world := World newWithSize:30 aliveCells:cells.
            generation := 0.
            [
                Conway clearScreen.
                generation := generation + 1.
                world printNl.
                'Generation ' display.
                generation printNl.
                (Delay forMilliseconds:500) wait.
                world := world evolve.
            ] repeat ]

		Conway class >> clearScreen [
			<comment: 'I clear the screen, courtesy of https://stackoverflow.com/a/24833963 and https://stackoverflow.com/a/37778152/133764'>
			('%1[2J' % #($<16r1B>)) displayNl.
			('%1[H' % #($<16r1B>)) displayNl ] ]

Conway main.
