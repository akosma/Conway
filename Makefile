COLOR = \033[1;33m
NC = \033[0m

all: crystal kotlin ts cpp go php c csharp fsharp basic ruby swift objc objfw python java lua pascal rust vb scala st lisp dart perl js d zig vbscript

stats:
	cloc --list-file=cloc1.txt --csv --quiet; \
	cloc --list-file=cloc2.txt --csv --quiet; \
	cloc --list-file=cloc3.txt --csv --quiet; \
	cloc --list-file=cloc4.txt --csv --quiet; \
	cloc --list-file=cloc5.txt --csv --quiet; \
	cloc --list-file=cloc6.txt --csv --quiet; \
	cloc --list-file=cloc7.txt --csv --force-lang=Pascal --quiet;

############ Kotlin

.PHONY: kotlin
kotlin:
	@echo "${COLOR}--------------------- Kotlin ----------------------${NC}"; \
	cd Kotlin; \
	./gradlew test

############ JavaScript

JavaScript/node_modules:
	@cd JavaScript; \
	npm install

.PHONY: js
js: JavaScript/node_modules
	@echo "${COLOR}--------------------- JavaScript ----------------------${NC}"; \
	cd JavaScript; \
	npm test;

############ TypeScript

.PHONY: ts
ts:
	@echo "${COLOR}--------------------- TypeScript ----------------------${NC}"; \
	cd TypeScript; \
	deno test spec/tests.ts

############ C++

C++/build/tests:
	@cd C++; \
  	mkdir -p build; cd build; \
	cmake .. "-DCMAKE_TOOLCHAIN_FILE=~/.vcpkg/scripts/buildsystems/vcpkg.cmake"; \
	cmake --build .

.PHONY: cpp
cpp: C++/build/tests
	@echo "${COLOR}--------------------- C++ ----------------------${NC}"; \
	cd C++; \
	build/tests; \

############ C

C/build/tests:
	@cd C; \
  	mkdir -p build; cd build; \
	cmake .. "-DCMAKE_TOOLCHAIN_FILE=~/.vcpkg/scripts/buildsystems/vcpkg.cmake"; \
	cmake --build .

.PHONY: c
c: C/build/tests
	@echo "${COLOR}--------------------- C ----------------------${NC}"; \
	cd C; \
	build/tests; \

############ Go

.PHONY: go
go:
	@echo "${COLOR}--------------------- Go ----------------------${NC}"; \
	cd Go; \
	go test -test.v;

############ PHP

PHP/composer.phar:
	@cd PHP; \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
	php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"; \
	php composer-setup.php; \
	php -r "unlink('composer-setup.php');"; \
	php composer.phar install;

.PHONY: php
php: PHP/composer.phar
	@echo "${COLOR}--------------------- PHP ----------------------${NC}"; \
	cd PHP; \
	vendor/bin/pest

############ Visual Basic .NET

VB.NET/ConwayLib/bin/Debug/net6.0/ConwayLib.dll:
	@cd VB.NET; \
	dotnet build

.PHONY: vb
vb: VB.NET/ConwayLib/bin/Debug/net6.0/ConwayLib.dll
	@echo "${COLOR}--------------------- VB.NET ----------------------${NC}"; \
	cd VB.NET; \
	dotnet test

############ C#

C\#/ConwayLib/bin/Debug/net6.0/ConwayLib.dll:
	@cd C#; \
	dotnet build

.PHONY: csharp
csharp: C\#/ConwayLib/bin/Debug/net6.0/ConwayLib.dll
	@echo "${COLOR}--------------------- C# ----------------------${NC}"; \
	cd C#; \
	dotnet test

############ F#

F\#/ConwayLib/bin/Debug/net6.0/ConwayLib.dll:
	@cd F#; \
	dotnet build

.PHONY: fsharp
fsharp: F\#/ConwayLib/bin/Debug/net6.0/ConwayLib.dll
	@echo "${COLOR}--------------------- F# ----------------------${NC}"; \
	cd F#; \
	dotnet test

############ FreeBASIC

FreeBASIC/bin/tests:
	@cd FreeBASIC; \
	make

.PHONY: basic
basic: FreeBASIC/bin/tests
	@echo "${COLOR}--------------------- FreeBASIC ----------------------${NC}"; \
	cd FreeBASIC; \
	bin/tests

############ Python

.PHONY: python
python:
	@echo "${COLOR}--------------------- Python ----------------------${NC}"; \
	cd Python; \
	./tests.py

############ Ruby

.PHONY: ruby
ruby:
	@echo "${COLOR}--------------------- Ruby ----------------------${NC}"; \
	cd Ruby; \
	./tests.rb

############ Swift

.PHONY: swift
swift:
	@echo "${COLOR}--------------------- Swift ----------------------${NC}"; \
	cd Swift; \
	swift test

############ Objective-C

Objective-C/bin/tests:
	@cd Objective-C; \
	make

.PHONY: objc
objc: Objective-C/bin/tests
	@echo "${COLOR}-------------- Objective-C (GNUStep) -------------${NC}"; \
	cd Objective-C; \
	bin/tests

ObjFW/bin/tests:
	@cd ObjFW; \
	make

.PHONY: objfw
objfw: ObjFW/bin/tests
	@echo "${COLOR}-------------- Objective-C (ObjFW) --------------${NC}"; \
	cd ObjFW; \
	bin/tests

############ Java

.PHONY: java
java:
	@echo "${COLOR}--------------------- Java ----------------------${NC}"; \
	cd Java; \
	mvn test

############ Lua

.PHONY: lua
lua:
	@echo "${COLOR}--------------------- Lua ----------------------${NC}"; \
	cd Lua; \
	./tests.lua -v

############ FreePascal

.PHONY: pascal
pascal:
	@echo "${COLOR}--------------------- FreePascal ----------------------${NC}"; \
	cd FreePascal; \
	make tests;

############ Rust

.PHONY: rust
rust:
	@echo "${COLOR}--------------------- Rust ----------------------${NC}"; \
	cd Rust; \
	cargo test;

############ Smalltalk

.PHONY: st
st:
	@echo "${COLOR}--------------------- Smalltalk ----------------------${NC}"; \
	cd Smalltalk; \
	./tests.st;

############ Common Lisp

.PHONY: lisp
lisp:
	@echo "${COLOR}--------------------- Common Lisp ----------------------${NC}"; \
	cd CommonLisp; \
	CL_SOURCE_REGISTRY=$$(pwd)/conway sbcl --script tests.lisp;


############ Dart

.PHONY: dart
dart:
	@echo "${COLOR}--------------------- Dart ----------------------${NC}"; \
	cd Dart; \
	dart test tests/conway.dart

############ Perl

.PHONY: perl
perl:
	@echo "${COLOR}--------------------- Perl ----------------------${NC}"; \
	cd Perl; \
	./tests.pl


############ Crystal

.PHONY: crystal
crystal:
	@echo "${COLOR}--------------------- Crystal ----------------------${NC}"; \
	cd Crystal; \
	crystal tests.cr


############ D

D/tests:
	cd D; \
	dmd conway.d conwaylib/package.d conwaylib/cell.d conwaylib/coord.d conwaylib/world.d -unittest -of=tests

.PHONY: d
d: D/tests
	@echo "${COLOR}--------------------- D ----------------------${NC}"; \
	cd D; \
	./tests


############ Zig

zig:
	cd Zig; \
	zig build test


############ VBScript

vbscript:
	cd VBScript; \
	cscript vbsunit.wsf tests.vbs

############ Scala

scala:
	cd Scala; \
	sbt test

