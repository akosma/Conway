(asdf:defsystem "conway"
  :description "Conway's Game of Life in Common Lisp"
  :version "0.0.1"
  :author "Adrian Kosmaczewski <me@akos.ma>"
  :licence "Public Domain"
  :components ((:file "coord")
			   (:file "world")
			   (:file "cell")))
