(defclass world ()
  ((size :accessor size :initarg :size)
   (cells :accessor cells :initarg :cells))
  (:documentation "A lattice of cells referenced by coord values."))

(defun find-in-list (object list)
  (loop named outer for a in list do
	(if (equalp a object)
		(return-from outer t))))

(defun make-world (size alive-cells)
  (defvar cells (make-hash-table :test #'equalp))
  (loop for a from 0 to (- size 1) do
	(loop for b from 0 to (- size 1) do
	  (let ((c (make-coord :x a :y b)))
		(if (find-in-list c alive-cells)
			(setf (gethash c cells) 'alive)
			(setf (gethash c cells) 'dead)))))
  (make-instance 'world :size size :cells cells))

(defun equal-world (this other)
  (with-slots (size cells) this
	(defvar equal-size (= size (size other)))
	(defvar equal-cells t)
	(loop for key being the hash-keys of cells using (hash-value value) do
		  (let ((othercell (gethash key (cells other))))
			(setq equal-cells (and equal-cells (equalp othercell value)))))
	(and equal-size equal-cells)))

(defmethod evolve ((object world))
  (with-slots (size cells) object
	(defvar alive-cells)
	(setq alive-cells (list nil))
	(loop for key being the hash-keys of cells using (hash-value value) do
	  (let ((count 0))
		(loop for a from -1 to 1 do
		  (loop for b from -1 to 1 do
			(let ((current-coord (make-coord :x (+ a (coord-x key)) :y (+ b (coord-y key)))))
			  (if (not (equalp current-coord key))
				  (if (gethash current-coord cells)
					  (let ((cell (gethash current-coord cells)))
						(if (equalp cell 'alive)
							(setq count (+ 1 count)))))))))
		(if (equalp value 'alive)
			(if (or (= 2 count) (= 3 count))
				(setq alive-cells (append (list key) alive-cells)))
			(if (= 3 count)
				(setq alive-cells (append (list key) alive-cells))))))
	(make-world size alive-cells)))

(defmethod print-world ((object world))
  (with-slots (size cells) object
	(loop for a from 0 to (- size 1) do
	  ;; First line with coordinates
	  (if (= a 0)
		  (progn
			(format t "    ")
			(loop for b from 0 to (- size 1) do
			  (format t "~3d|" b))
			(format t "~%")))
	  (format t "~3d|" a)
	  (loop for b from 0 to (- size 1) do
		(let ((c (make-coord :x b :y a)))
		  (format t "~A" (print-cell (gethash c cells)))))
	  (format t "~%"))))

(defun make-blinker (origin)
  (list (make-coord :x (coord-x origin)       :y (coord-y origin))
		(make-coord :x (+ 1 (coord-x origin)) :y (coord-y origin))
		(make-coord :x (+ 2 (coord-x origin)) :y (coord-y origin))))

(defun make-beacon (origin)
  (list (make-coord :x (coord-x origin)       :y (coord-y origin))
		(make-coord :x (+ 1 (coord-x origin)) :y (coord-y origin))
		(make-coord :x (coord-x origin)       :y (+ 1 (coord-y origin)))
		(make-coord :x (+ 1 (coord-x origin)) :y (+ 1 (coord-y origin)))
		(make-coord :x (+ 2 (coord-x origin)) :y (+ 2 (coord-y origin)))
		(make-coord :x (+ 3 (coord-x origin)) :y (+ 2 (coord-y origin)))
		(make-coord :x (+ 2 (coord-x origin)) :y (+ 3 (coord-y origin)))
		(make-coord :x (+ 3 (coord-x origin)) :y (+ 3 (coord-y origin)))))

(defun make-glider (origin)
  (list (make-coord :x (+ 2 (coord-x origin)) :y (+ 2 (coord-y origin)))
		(make-coord :x (+ 1 (coord-x origin)) :y (+ 2 (coord-y origin)))
		(make-coord :x (coord-x origin)       :y (+ 2 (coord-y origin)))
		(make-coord :x (+ 2 (coord-x origin)) :y (+ 1 (coord-y origin)))
		(make-coord :x (+ 1 (coord-x origin)) :y (coord-y origin))))

(defun make-block (origin)
  (list (make-coord :x (coord-x origin)       :y (coord-y origin))
		(make-coord :x (+ 1 (coord-x origin)) :y (coord-y origin))
		(make-coord :x (coord-x origin)       :y (+ 1 (coord-y origin)))
		(make-coord :x (+ 1 (coord-x origin)) :y (+ 1 (coord-y origin)))))

(defun make-tub (origin)
  (list (make-coord :x (+ 1 (coord-x origin)) :y (coord-y origin))
		(make-coord :x (coord-x origin)       :y (+ 1 (coord-y origin)))
		(make-coord :x (+ 2 (coord-x origin)) :y (+ 1 (coord-y origin)))
		(make-coord :x (+ 1 (coord-x origin)) :y (+ 2 (coord-y origin)))))

