(deftype cell () '(dead alive))

(defun print-cell (c)
  (if (eq c 'alive)
	  (format nil " x |")
	  (format nil "   |")))
