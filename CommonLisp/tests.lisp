(require "asdf")
(asdf:operate 'asdf:load-op 'conway)
(load "~/quicklisp/setup.lisp")
(ql:quickload :lisp-unit)
(use-package :lisp-unit)

(define-test test-block
  (let ((alive (make-block (make-coord :x 0 :y 0))))
	(let ((original (make-world 5 alive)))
	  (let ((next (evolve original)))
		(assert-true (equal-world original next))))))

(define-test test-tub
  (let ((alive (make-tub (make-coord :x 0 :y 0))))
	(let ((original (make-world 5 alive)))
	  (let ((next (evolve original)))
		(assert-true (equal-world original next))))))

(define-test test-blinker
  (let ((alive (make-blinker (make-coord :x 0 :y 1))))
	(let ((original (make-world 3 alive)))
	  (let ((gen1 (evolve original)))
		(let ((expected-alive (list (make-coord :x 1 :y 0)
									(make-coord :x 1 :y 1)
									(make-coord :x 1 :y 2))))
		  (let ((expected (make-world 3 expected-alive)))
			(assert-true (equal-world expected gen1))
			(let ((gen2 (evolve gen1)))
			  (assert-true (equal-world original gen2)))))))))

(run-tests :all)

