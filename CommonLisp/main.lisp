(require "asdf")
(asdf:operate 'asdf:load-op 'conway)

(defun clear-screen ()
  ;; https://rosettacode.org/wiki/Terminal_control/Clear_the_screen#Common_Lisp
  (format t "~C[2J" #\Esc))

(defvar alive-cells (make-blinker (make-coord :x 0 :y 1)))
(setq alive-cells (append (make-beacon (make-coord :x 10 :y 10)) alive-cells))
(setq alive-cells (append (make-glider (make-coord :x 4 :y 5)) alive-cells))
(setq alive-cells (append (make-block (make-coord :x 1 :y 10)) alive-cells))
(setq alive-cells (append (make-block (make-coord :x 18 :y 3)) alive-cells))
(setq alive-cells (append (make-tub (make-coord :x 6 :y 1)) alive-cells))
(defvar world (make-world 30 alive-cells))
(defvar generation 1)

(loop
  (clear-screen)
  (print-world world)
  (format t "~%Generation ~d~%" generation)
  (setq generation (+ 1 generation))
  (setq world (evolve world))
  (sleep 0.5))

