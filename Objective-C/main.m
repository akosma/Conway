#include <signal.h>
#import <Foundation/Foundation.h>
#import "conway/CWWorld.h"

volatile sig_atomic_t flag = 0;

void signal_handler(int sig) {
    flag = 1;
}

int main (int argc, const char * argv[]) {
    signal(SIGINT, signal_handler);

    NSMutableArray *alive = [NSMutableArray array];
    [alive addObjectsFromArray:[CWWorld blinkerAt:NSMakePoint(0, 1)]];
    [alive addObjectsFromArray:[CWWorld beaconAt:NSMakePoint(10, 10)]];
    [alive addObjectsFromArray:[CWWorld gliderAt:NSMakePoint(4, 5)]];
    [alive addObjectsFromArray:[CWWorld blockAt:NSMakePoint(1, 10)]];
    [alive addObjectsFromArray:[CWWorld blockAt:NSMakePoint(18, 3)]];
    [alive addObjectsFromArray:[CWWorld tubAt:NSMakePoint(6, 1)]];
    CWWorld *world = [[CWWorld alloc] initWithSize:30 aliveCells:alive];

    int generation = 0;

    while (true) {
        if (flag == 1) {
            break;
        }
        system("clear");
        generation++;
        fprintf(stdout, "%s \nGeneration %d\n", [[world description] UTF8String], generation);
        [NSThread sleepForTimeInterval:0.5];
        world = [world evolve];
    }

    system("clear");
    return EXIT_SUCCESS;
}
