#import <Foundation/Foundation.h>
#import "conway/CWWorld.h"

void block() {
    NSArray *alive = [CWWorld blockAt:NSMakePoint(0, 0)];
    CWWorld *original = [[CWWorld alloc] initWithSize:5 aliveCells:alive];
    CWWorld *next = [original evolve];
    assert([next isEqual:original]);
    printf ("Block passed\n");
}

void tub() {
    NSArray *alive = [CWWorld tubAt:NSMakePoint(0, 0)];
    CWWorld *original = [[CWWorld alloc] initWithSize:5 aliveCells:alive];
    CWWorld *next = [original evolve];
    assert([next isEqual:original]);
    printf ("Tub passed\n");
}

void blinker() {
    NSArray *alive = [CWWorld blinkerAt:NSMakePoint(0, 1)];
    CWWorld *original = [[CWWorld alloc] initWithSize:3 aliveCells:alive];
    CWWorld *gen1 = [original evolve];
    NSArray *expectedAlive = [NSArray arrayWithObjects:[NSValue valueWithPoint:NSMakePoint(1, 0)],
                                                       [NSValue valueWithPoint:NSMakePoint(1, 1)],
                                                       [NSValue valueWithPoint:NSMakePoint(1, 2)], nil];
    CWWorld *expected = [[CWWorld alloc] initWithSize:3 aliveCells:expectedAlive];
    assert([expected isEqual:gen1]);
    CWWorld *gen2 = [gen1 evolve];
    assert([original isEqual:gen2]);
    printf ("Blinker passed\n");
}

int main (int argc, const char * argv[]) {
    block();
    tub();
    blinker();
    printf ("All tests passed\n");
    return 0;
}
