#import <Foundation/Foundation.h>

@interface CWWorld : NSObject

@property (readonly) int size;
@property (readonly, retain) NSDictionary *cells;

- (id)initWithSize:(int)size aliveCells:(NSArray *)cells;

- (CWWorld *)evolve;

+ (NSArray *)blinkerAt:(NSPoint)coord;

+ (NSArray *)beaconAt:(NSPoint)coord;

+ (NSArray *)gliderAt:(NSPoint)coord;

+ (NSArray *)blockAt:(NSPoint)coord;

+ (NSArray *)tubAt:(NSPoint)coord;

@end
