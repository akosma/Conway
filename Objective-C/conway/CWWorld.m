#import "CWWorld.h"
#import "CWCell.h"

@implementation CWWorld

@synthesize size = _size;
@synthesize cells = _cells;

- (id)initWithSize:(int)size aliveCells:(NSArray *)cells {
    if ((self = [super init])) {
        _size = size;
        NSMutableDictionary *aliveCells = [NSMutableDictionary dictionary];
        for (int a = 0; a < _size; ++a) {
            for (int b = 0; b < _size; ++b) {
                NSValue *coord = [NSValue valueWithPoint:NSMakePoint(a, b)];
                if ([cells containsObject:coord]) {
                    [aliveCells setObject:[NSNumber numberWithInt:kCWCellAlive] forKey:coord];
                }
                else {
                    [aliveCells setObject:[NSNumber numberWithInt:kCWCellDead] forKey:coord];
                }
            }
        }
        _cells = aliveCells;
    }
    return self;
}

- (BOOL)isEqual:(CWWorld *)other {
    if (_size != other.size) return NO;
    for (NSValue *coord in self.cells) {
        if ([other.cells objectForKey:coord] == nil) return NO;
        NSValue *cell = [self.cells objectForKey:coord];
        NSValue *otherCell = [other.cells objectForKey:coord];
        if (![cell isEqual:otherCell]) return NO;
    }
    return YES;
}

- (CWWorld *)evolve {
    NSMutableArray *alive = [NSMutableArray array];
    for (NSValue *value in self.cells) {
        NSPoint coord = [value pointValue];
        CWCell cell = [[self.cells objectForKey:value] intValue];
        int count = 0;
        for (int a = -1; a <= 1; ++a) {
            for (int b = -1; b <= 1; ++b) {
                NSValue *currentCoord = [NSValue valueWithPoint:NSMakePoint((coord.x + a), (coord.y + b))];
                NSNumber *cellObj = [self.cells objectForKey:currentCoord];
                if (![currentCoord isEqual:value]
                    && [cellObj intValue] == kCWCellAlive) count += 1;
            }
        }

        if (cell == kCWCellAlive) {
            if (count == 2 || count == 3) [alive addObject:value];
        }
        else {
            if (count == 3) [alive addObject:value];
        }
    }
    return [[CWWorld alloc] initWithSize:_size aliveCells:alive];
}

- (NSString *)description {
    NSMutableString *result = [NSMutableString stringWithString:@"    "];
    for (int a = 0; a < _size; ++a) {
        // First line with coordinates
        if (a == 0) {
            for (int b = 0; b < _size; ++b)
            {
                [result appendFormat:@"%3d|", b];
            }
            [result appendString:@"\n"];
        }

        [result appendFormat:@"%3d|", a];
        for (int b = 0; b < _size; ++b) {
            NSValue *coord = [NSValue valueWithPoint:NSMakePoint(b, a)];
            CWCell cell = [[self.cells objectForKey:coord] intValue];
            [result appendString:cellDescription(cell)];
        }
        [result appendString:@"\n"];
    }
    return [result description];
}

+ (NSArray *)blinkerAt:(NSPoint)coord {
    return [NSArray arrayWithObjects:[NSValue valueWithPoint:NSMakePoint(coord.x      , coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 2), coord.y)], nil];
}

+ (NSArray *)beaconAt:(NSPoint)coord {
    return [NSArray arrayWithObjects:[NSValue valueWithPoint:NSMakePoint(coord.x      , coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint(coord.x      , (coord.y + 1))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), (coord.y + 1))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 2), (coord.y + 2))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 3), (coord.y + 2))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 2), (coord.y + 3))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 3), (coord.y + 3))], nil];
}

+ (NSArray *)gliderAt:(NSPoint)coord {
    return [NSArray arrayWithObjects:[NSValue valueWithPoint:NSMakePoint((coord.x + 2), (coord.y + 2))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), (coord.y + 2))],
                                     [NSValue valueWithPoint:NSMakePoint(coord.x      , (coord.y + 2))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 2), (coord.y + 1))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), coord.y)], nil];
}

+ (NSArray *)blockAt:(NSPoint)coord {
    return [NSArray arrayWithObjects:[NSValue valueWithPoint:NSMakePoint(coord.x      , coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint(coord.x      , (coord.y + 1))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), (coord.y + 1))], nil];
}

+ (NSArray *)tubAt:(NSPoint)coord {
    return [NSArray arrayWithObjects:[NSValue valueWithPoint:NSMakePoint((coord.x + 1), coord.y)],
                                     [NSValue valueWithPoint:NSMakePoint(coord.x      , (coord.y + 1))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 2), (coord.y + 1))],
                                     [NSValue valueWithPoint:NSMakePoint((coord.x + 1), (coord.y + 2))], nil];
}

@end
