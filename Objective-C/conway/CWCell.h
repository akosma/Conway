#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, CWCell) {
    kCWCellDead,
    kCWCellAlive
};

NSString *cellDescription(CWCell cell) {
    if (cell == kCWCellAlive) {
        return @" x |";
    }
    return @"   |";
}
