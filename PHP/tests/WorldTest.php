<?php
declare(strict_types=1);

use akosma\conway\Coord;
use akosma\conway\World;

test('block', function () {
    $alive = World::block(new Coord(0, 0));
    $original = new World(5, $alive);
    $next = $original->evolve();
    expect(0)->toEqual($original <=> $next);
});

test('tub', function () {
    $alive = World::tub(new Coord(0, 0));
    $original = new World(5, $alive);
    $next = $original->evolve();
    expect(0)->toEqual($original <=> $next);
});

test('blinker', function () {
    $alive = World::blinker(new Coord(0, 1));
    $original = new World(3, $alive);
    $gen1 = $original->evolve();
    $expectedAlive = [
        new Coord(1, 0),
        new Coord(1, 1),
        new Coord(1, 2)
    ];
    $expected = new World(3, $expectedAlive);
    expect(0)->toEqual($expected <=> $gen1);
    $gen2 = $gen1->evolve();
    expect(0)->toEqual($original <=> $gen2);
});
