<?php
declare(strict_types=1);

namespace akosma\conway;

readonly class Coord
{
    function __construct(public int $x, public int $y) { }

    public static function fromString(string $str): Coord
    {
        [$a, $b] = explode(separator: ":", string: $str);
        return new static(intval($a), intval($b));
    }

    public function __toString(): string
    {
        return sprintf("%d:%d", $this->x, $this->y);
    }
}
