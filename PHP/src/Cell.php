<?php
declare(strict_types=1);

namespace akosma\conway;

enum Cell : int
{
    case Dead = 0;
    case Alive = 1;

    public function toString(): string
    {
        return match ($this) {
            Cell::Alive => " x |",
            Cell::Dead => "   |",
        };
    }
}
