<?php
declare(strict_types=1);

namespace akosma\conway;

readonly class World
{
    private array $cells;

    function __construct(private int $size, array $aliveCells)
    {
        assert(assertion: $aliveCells != null);
        $cells = [];
        $range = range(start: 0, end: $size - 1);
        foreach ($range as $a) foreach ($range as $b) {
            $coord = new Coord(x: $a, y: $b);
            $cells[strval($coord)] = match (in_array($coord, $aliveCells)) {
                true => Cell::Alive,
                false => Cell::Dead,
            };
        }
        $this->cells = $cells;
    }

    public function evolve(): World
    {
        $alive = [];
        $range = range(start: -1, end: 1);
        foreach ($this->cells as $str => $cell) {
            $coord = Coord::fromString($str);
            $count = 0;

            foreach ($range as $a) foreach ($range as $b) {
                $currentCoord = sprintf("%d:%d", $coord->x + $a, $coord->y + $b);
                if ($currentCoord != $str
                    && array_key_exists(key: $currentCoord, array: $this->cells)
                    && $this->cells[$currentCoord] == Cell::Alive) {
                    $count += 1;
                }
            }

            switch ($cell) {
                case Cell::Alive:
                    if ($count == 2 || $count == 3) {
                        $alive[] = Coord::fromString($str);
                    }
                    break;

                case Cell::Dead:
                    if ($count == 3) {
                        $alive[] = Coord::fromString($str);
                    }
                    break;
            }
        }
        return new World(size: $this->size, aliveCells: $alive);
    }

    public function __toString(): string
    {
        $str = "\n";

        for ($a = 0; $a < $this->size; ++$a) {
            if ($a == 0) {
                // First line with coordinates
                $str .= "    ";
                for ($b = 0; $b < $this->size; ++$b) {
                    $str .= sprintf("%3d|", $b);
                }
                $str .= "\n";
            }
            $str .= sprintf("%3d|", $a);
            for ($b = 0; $b < $this->size; ++$b) {
                $coord = new Coord($b, $a);
                $cell = $this->cells[strval($coord)];
                $str .= $cell->toString();
            }
            $str .= "\n";
        }
        $str .= "\n";
        return $str;
    }

    public static function blinker(Coord $origin): array
    {
        return [
            new Coord(x: $origin->x, y: $origin->y),
            new Coord(x: $origin->x + 1, y: $origin->y),
            new Coord(x: $origin->x + 2, y: $origin->y)
        ];
    }

    public static function beacon(Coord $origin): array
    {
        return [
            new Coord(x: $origin->x, y: $origin->y),
            new Coord(x: $origin->x + 1, y: $origin->y),
            new Coord(x: $origin->x, y: $origin->y + 1),
            new Coord(x: $origin->x + 1, y: $origin->y + 1),
            new Coord(x: $origin->x + 2, y: $origin->y + 2),
            new Coord(x: $origin->x + 3, y: $origin->y + 2),
            new Coord(x: $origin->x + 2, y: $origin->y + 3),
            new Coord(x: $origin->x + 3, y: $origin->y + 3)
        ];
    }

    public static function glider(Coord $origin): array
    {
        return [
            new Coord(x: $origin->x + 2, y: $origin->y + 2),
            new Coord(x: $origin->x + 1, y: $origin->y + 2),
            new Coord(x: $origin->x, y: $origin->y + 2),
            new Coord(x: $origin->x + 2, y: $origin->y + 1),
            new Coord(x: $origin->x + 1, y: $origin->y)
        ];
    }

    public static function block(Coord $origin): array
    {
        return [
            new Coord(x: $origin->x, y: $origin->y),
            new Coord(x: $origin->x + 1, y: $origin->y),
            new Coord(x: $origin->x, y: $origin->y + 1),
            new Coord(x: $origin->x + 1, y: $origin->y + 1)
        ];
    }

    public static function tub(Coord $origin): array
    {
        return [
            new Coord(x: $origin->x + 1, y: $origin->y),
            new Coord(x: $origin->x, y: $origin->y + 1),
            new Coord(x: $origin->x + 2, y: $origin->y + 1),
            new Coord(x: $origin->x + 1, y: $origin->y + 2)
        ];
    }
}
