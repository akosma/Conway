#!/usr/bin/env php
<?php
declare(strict_types=1);

require_once "bootstrap.php";

use akosma\conway\World;
use akosma\conway\Coord;

// Handling CTRL+C cleanly
pcntl_async_signals(enable: true);
function sig_handler() : never
{
    system(command: 'clear');
    exit(0);
}
pcntl_signal(signal: SIGINT, handler: "sig_handler");

$blinker = World::blinker(origin: new Coord(x: 0, y: 1));
$beacon = World::beacon(origin: new Coord(x: 10, y: 10));
$glider = World::glider(origin: new Coord(x: 4, y: 5));
$block1 = World::block(origin: new Coord(x: 1, y: 10));
$block2 = World::block(origin: new Coord(x: 18, y: 3));
$tub = World::tub(origin: new Coord(x: 6, y: 1));
$alive = array_merge($blinker, $beacon, $glider, $block1, $block2, $tub);
$world = new World(size: 30, aliveCells: $alive);
$generation = 0;

while (true)
{
    system(command: 'clear');
    $generation++;
    echo $world;
    echo sprintf("Generation %d", $generation);
    $world = $world->evolve();
    usleep(microseconds: 500 * 1000);
}
