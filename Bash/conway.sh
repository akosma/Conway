#!/bin/bash

# Declare the associative array to hold the world
declare -A world

# Function to set a value in the world
set_value() {
    local x=$1
    local y=$2
    local value=$3
    world["${x},${y}"]="$value"
}

SIZE=30

initialize_world() {
    for ((y=0; y<SIZE; y++)); do
        for ((x=0; x<SIZE; x++)); do
            set_value $x $y "dead"
        done
    done
}

# Function to evolve the game state
evolve() {
    declare -A new_world
    local count
    local x
    local y
    local a
    local b

    for ((y=0; y<SIZE; y++)); do
        for ((x=0; x<SIZE; x++)); do
            count=0
            for dx in -1 0 1; do
                for dy in -1 0 1; do
                    if [[ dx -eq 0 && $dy -eq 0 ]]; then
                        continue
                    fi
                    a=$((x+dx))
                    b=$((y+dy))
                    if [[ "${world["${a},${b}"]}" == "alive" ]]; then
                        ((count++))
                    fi
                done
            done
            key="${x},${y}"
            if [[ ${world[$key]} == "alive" ]]; then
                if ((count == 2 || count == 3)); then
                    new_world[$key]="alive"
                else
                    new_world[$key]="dead"
                fi
            else
                if ((count == 3)); then
                    new_world[$key]="alive"
                else
                    new_world[$key]="dead"
                fi
            fi
        done
    done

    world=()
    for key in "${!new_world[@]}"; do
        world[$key]="${new_world[$key]}"
    done
}

# Function to print the grid
print_grid() {
    printf "\n"

    # Print the top coordinates
    echo -n "    "
    for ((x=0; x<SIZE; x++)); do
        printf "%3d|" $x
    done
    echo

    for ((y=0; y<SIZE; y++)); do
        # Print the left coordinates
        printf "%3d|" $y

        for ((x=0; x<SIZE; x++)); do
            if [[ ${world["${x},${y}"]} == "alive" ]]; then
                echo -n " x |"
            else
                echo -n "   |"
            fi
        done
        echo
    done
}

# Entity creation functions
create_blinker() {
    local x=$1
    local y=$2
    set_value "$x" "$y" "alive"
    set_value $((x+1)) "$y" "alive"
    set_value $((x+2)) "$y" "alive"
}

create_beacon() {
    local x=$1
    local y=$2
    set_value "$x" "$y" "alive"
    set_value $((x+1)) "$y" "alive"
    set_value "$x" $((y+1)) "alive"
    set_value $((x+1)) $((y+1)) "alive"
    set_value $((x+2)) $((y+2)) "alive"
    set_value $((x+3)) $((y+2)) "alive"
    set_value $((x+2)) $((y+3)) "alive"
    set_value $((x+3)) $((y+3)) "alive"
}

create_glider() {
    local x=$1
    local y=$2
    set_value $((x+2)) $((y+2)) "alive"
    set_value $((x+1)) $((y+2)) "alive"
    set_value "$x" $((y+2)) "alive"
    set_value $((x+2)) $((y+1)) "alive"
    set_value $((x+1)) "$y" "alive"
}

create_block() {
    local x=$1
    local y=$2
    set_value "$x" "$y" "alive"
    set_value $((x+1)) "$y" "alive"
    set_value "$x" $((y+1)) "alive"
    set_value $((x+1)) $((y+1)) "alive"
}

create_tub() {
    local x=$1
    local y=$2
    set_value $((x+1)) "$y" "alive"
    set_value "$x" $((y+1)) "alive"
    set_value $((x+2)) $((y+1)) "alive"
    set_value $((x+1)) $((y+2)) "alive"
}

# Initialize the game with predefined entities
initialize_game() {
    initialize_world
    create_blinker 0 1
    create_beacon 10 10
    create_glider 4 5
    create_block 1 10
    create_block 18 3
    create_tub 6 1
}

# Main loop
initialize_game
generation=0
while true; do
    clear
    print_grid
    echo
    ((generation+=1))
    printf "Generation %d" $generation
    sleep 0.5
    evolve
done
