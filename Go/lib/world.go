package lib

import (
	"fmt"
	"strings"

	"github.com/google/go-cmp/cmp"
)

// World Represents an evolving entity in Conway's Game of Life
type World struct {
	size  int
	cells map[Coord]Cell
}

// CoordInList evaluates whether the list contains the Coord object passed in parameter.
// Adapted from https://stackoverflow.com/a/10485970/133764
func CoordInList(a Coord, list []Coord) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// Equal varifies that the current instance is equal to the one passed as parameter.
// This method is used in tests.
func (w *World) Equal(another *World) bool {
	if w.size != another.size {
		return false
	}
	if !cmp.Equal(w.cells, another.cells) {
		return false
	}
	return true
}

// NewWorld creates and initializes a new world
func NewWorld(size int, coords []Coord) *World {
	cells := make(map[Coord]Cell)
	for a := 0; a < size; a++ {
		for b := 0; b < size; b++ {
			coord := Coord{a, b}
			if CoordInList(coord, coords) {
				cells[coord] = Alive
			} else {
				cells[coord] = Dead
			}
		}
	}

	world := World{size, cells}
	return &world
}

// Evolve returns the next generation of the current world
func (w *World) Evolve(channel chan *World) {
	r := [3]int{-1, 0, 1}
	alive := []Coord{}

	for coord, cell := range w.cells {
		count := 0
		for _, a := range r {
			for _, b := range r {
				currentCoord := Coord{coord.X + a, coord.Y + b}
				currentCell := w.cells[currentCoord]
				if currentCoord != coord && currentCell == Alive {
					count++
				}
			}
		}

		switch cell {
		case Alive:
			if count == 2 || count == 3 {
				alive = append(alive, coord)
			}
		case Dead:
			if count == 3 {
				alive = append(alive, coord)
			}
		}
	}

	channel <- NewWorld(w.size, alive)
}

func (w *World) String() string {
	var result strings.Builder
	result.WriteString("\n")
	for a := 0; a < w.size; a++ {
		if a == 0 {
			// First line with coordinates
			result.WriteString("    ")
			for b := 0; b < w.size; b++ {
				result.WriteString(fmt.Sprintf("%3d|", b))
			}
			result.WriteString("\n")
		}
		result.WriteString(fmt.Sprintf("%3d|", a))
		for b := 0; b < w.size; b++ {
			coord := Coord{b, a}
			cell := w.cells[coord]
			result.WriteString(cell.String())
		}
		result.WriteString("\n")
	}
	return result.String()
}

// Blinker returns a Blinker configuration
func Blinker(coord Coord) []Coord {
	result := []Coord{}
	result = append(result, Coord{coord.X, coord.Y})
	result = append(result, Coord{coord.X + 1, coord.Y})
	result = append(result, Coord{coord.X + 2, coord.Y})
	return result
}

// Beacon returns a Beacon configuration
func Beacon(coord Coord) []Coord {
	result := []Coord{}
	result = append(result, Coord{coord.X, coord.Y})
	result = append(result, Coord{coord.X + 1, coord.Y})
	result = append(result, Coord{coord.X, coord.Y + 1})
	result = append(result, Coord{coord.X + 1, coord.Y + 1})
	result = append(result, Coord{coord.X + 2, coord.Y + 2})
	result = append(result, Coord{coord.X + 3, coord.Y + 2})
	result = append(result, Coord{coord.X + 2, coord.Y + 3})
	result = append(result, Coord{coord.X + 3, coord.Y + 3})
	return result
}

// Glider returns a Glider configuration
func Glider(coord Coord) []Coord {
	result := []Coord{}
	result = append(result, Coord{coord.X + 2, coord.Y + 2})
	result = append(result, Coord{coord.X + 1, coord.Y + 2})
	result = append(result, Coord{coord.X, coord.Y + 2})
	result = append(result, Coord{coord.X + 2, coord.Y + 1})
	result = append(result, Coord{coord.X + 1, coord.Y})
	return result
}

// Block returns a Block configuration
func Block(coord Coord) []Coord {
	result := []Coord{}
	result = append(result, Coord{coord.X, coord.Y})
	result = append(result, Coord{coord.X + 1, coord.Y})
	result = append(result, Coord{coord.X, coord.Y + 1})
	result = append(result, Coord{coord.X + 1, coord.Y + 1})
	return result
}

// Tub returns a Tub configuration
func Tub(coord Coord) []Coord {
	result := []Coord{}
	result = append(result, Coord{coord.X + 1, coord.Y})
	result = append(result, Coord{coord.X, coord.Y + 1})
	result = append(result, Coord{coord.X + 2, coord.Y + 1})
	result = append(result, Coord{coord.X + 1, coord.Y + 2})
	return result
}
