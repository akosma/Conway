package lib

import "fmt"

// Coord represents a location in a World in X and Y coordinates
type Coord struct {
	X, Y int
}

func (c Coord) String() string {
	return fmt.Sprintf("%d:%d", c.X, c.Y)
}
