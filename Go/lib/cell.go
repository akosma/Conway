package lib

// Cell represents a cell, either alive or not
type Cell int

const (
	// Dead represents a dead cell
	Dead Cell = iota

	// Alive represents a living cell
	Alive
)

func (c Cell) String() string {
	return [2]string{"   |", " x |"}[c]
}
