package main

import (
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"time"

	"conway/lib"
)

var interrupt = false

func Clear() {
	// Only works for Linux
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func main() {
	// https://stackoverflow.com/a/11269077/133764
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for range c {
			interrupt = true
		}
	}()

	alive := lib.Blinker(lib.Coord{X: 0, Y: 1})
	alive = append(alive, lib.Beacon(lib.Coord{X: 10, Y: 10})...)
	alive = append(alive, lib.Glider(lib.Coord{X: 4, Y: 5})...)
	alive = append(alive, lib.Block(lib.Coord{X: 1, Y: 10})...)
	alive = append(alive, lib.Block(lib.Coord{X: 18, Y: 3})...)
	alive = append(alive, lib.Tub(lib.Coord{X: 6, Y: 1})...)

	world := lib.NewWorld(30, alive)
	generation := 1
	channel := make(chan *lib.World)
	Clear()

	for {
		fmt.Println(world)
		fmt.Printf("Generation %d", generation)
		time.Sleep(500 * time.Millisecond)
		generation++
		go world.Evolve(channel)
		world = <-channel
		Clear()
		if interrupt {
			break
		}
	}
}
