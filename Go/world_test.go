package main

import (
	"conway/lib"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestBlock(t *testing.T) {
	alive := lib.Block(lib.Coord{X: 0, Y: 0})
	original := lib.NewWorld(5, alive)
	channel := make(chan *lib.World)
	go original.Evolve(channel)
	next := <-channel

	// https://stackoverflow.com/a/45222521/133764
	if !cmp.Equal(next, original) {
		t.Errorf("got %s want %s", next, original)
	}
}

func TestTub(t *testing.T) {
	alive := lib.Tub(lib.Coord{X: 0, Y: 0})
	original := lib.NewWorld(5, alive)
	channel := make(chan *lib.World)
	go original.Evolve(channel)
	next := <-channel

	if !cmp.Equal(next, original) {
		t.Errorf("got %s want %s", next, original)
	}
}

func TestBlinker(t *testing.T) {
	alive := lib.Blinker(lib.Coord{X: 0, Y: 1})
	original := lib.NewWorld(3, alive)
	channel := make(chan *lib.World)
	go original.Evolve(channel)
	gen1 := <-channel
	expectedAlive := []lib.Coord{
		{X: 1, Y: 0},
		{X: 1, Y: 1},
		{X: 1, Y: 2},
	}

	expected := lib.NewWorld(3, expectedAlive)
	if !cmp.Equal(expected, gen1) {
		t.Errorf("got %s want %s", gen1, expected)
	}

	go gen1.Evolve(channel)
	gen2 := <-channel
	if !cmp.Equal(original, gen2) {
		t.Errorf("got %s want %s", gen2, original)
	}
}
