#!/usr/bin/env lua

require "conway/world"


-- http://lua-users.org/wiki/SleepFunction
function sleep(s)
    local ntime = os.clock() + s/10
    repeat until os.clock() > ntime
end

function mergeTables(...)
    -- https://stackoverflow.com/a/7574047/133764
    local arg={...}
    local result = {}
    for i, a in ipairs(arg) do
        for k, v in pairs(a) do table.insert(result, v) end
    end
    return result
end

-- https://stackoverflow.com/a/34409274/133764
-- Requires luaposix installed
local signal = require("posix.signal")
signal.signal(signal.SIGINT, function(signum)
    os.execute("clear")
    os.exit(0)
end)

aliveCells = mergeTables(blinker(Coord:new(0, 1))
                        , beacon(Coord:new(10, 10))
                        , glider(Coord:new(4, 5))
                        , block(Coord:new(1, 10))
                        , block(Coord:new(18, 3))
                        , tub(Coord:new(6, 1)))
world = World:new(30, aliveCells)
generation = 0

while true do
    os.execute("clear")
    generation = generation + 1
    print(world)
    print(string.format("Generation %d", generation))
    world = world:evolve()
    sleep(5)
end
