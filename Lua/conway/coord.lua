Coord = { x = 0, y = 0 }

function Coord:new (x, y)
    c = {}
    setmetatable(c, self)
    self.__index = self
    self.__tostring = Coord.toString
    c.x = x or 0
    c.y = y or 0
    return c
end

function Coord:parse(str)
    c = {}
    setmetatable(c, self)
    self.__index = self
    -- https://stackoverflow.com/a/19269176/133764
    x, y = str:match("([^,]+):([^,]+)")
    c.x = tonumber(x)
    c.y = tonumber(y)
    return c
end

function Coord:equals(other)
    if other == nil then return false end
    if self == other then return true end
    if self.x == other.x and self.y == other.y then
        return true
    end
    return false
end

function Coord:toString()
    return string.format("%d:%d", self.x, self.y)
end
