require "conway/coord"
require "conway/cell"

World = { size = 0, cells = {} }

function World:new(size, aliveCells)
    local function inTable(tbl, coord)
        for index, value in pairs(tbl) do
            if value:equals(coord) then
                return true
            end
        end
        return false
    end

    w = {}
    setmetatable(w, self)
    self.__index = self
    self.__tostring = World.toString
    w.size = size or 0
    w.cells = {}

    for a = 0, w.size - 1 do
        for b = 0, w.size - 1 do
            coord = Coord:new(a, b)
            if inTable(aliveCells, coord) then
                w.cells[tostring(coord)] = alive
            else
                w.cells[tostring(coord)] = dead
            end
        end
    end
    return w
end

function World:evolve()
    local aliveCells = {}
    for coordStr, cell in pairs(self.cells) do
        local coord = Coord:parse(coordStr)
        local count = 0
        for a = -1, 1 do
            for b = -1, 1 do
                local currentCoord = Coord:new(coord.x + a, coord.y + b)
                if not coord:equals(currentCoord)
                    and self.cells[tostring(currentCoord)] == alive then
                        count = count + 1
                end
            end
        end

        if cell == alive then
            if count == 2 or count == 3 then
                aliveCells[#aliveCells + 1] = coord
            end
        else
            if count == 3 then
                aliveCells[#aliveCells + 1] = coord
            end
        end
    end
    return World:new(self.size, aliveCells)
end

function World:toString()
    local str = "\n    "
    for a = 0, self.size - 1 do
        -- First line with coordinates
        if a == 0 then
            for b = 0, self.size - 1 do
                str = str .. string.format("%3d|", b)
            end
            str = str .. "\n"
        end
        str = str .. string.format("%3d|", a)
        for b = 0, self.size - 1 do
            local coord = Coord:new(b, a)
            str = str .. self.cells[tostring(coord)]
        end
        str = str .. "\n"
    end
    return str
end

function blinker(coord)
    return {
        Coord:new(coord.x, coord.y),
        Coord:new(coord.x + 1, coord.y),
        Coord:new(coord.x + 2, coord.y)
    }
end

function beacon(coord)
    return {
        Coord:new(coord.x, coord.y),
        Coord:new(coord.x + 1, coord.y),
        Coord:new(coord.x, coord.y + 1),
        Coord:new(coord.x + 1, coord.y + 1),
        Coord:new(coord.x + 2, coord.y + 2),
        Coord:new(coord.x + 3, coord.y + 2),
        Coord:new(coord.x + 2, coord.y + 3),
        Coord:new(coord.x + 3, coord.y + 3)
    }
end

function glider(coord)
    return {
        Coord:new(coord.x + 2, coord.y + 2),
        Coord:new(coord.x + 1, coord.y + 2),
        Coord:new(coord.x, coord.y + 2),
        Coord:new(coord.x + 2, coord.y + 1),
        Coord:new(coord.x + 1, coord.y)
    }
end

function block(coord)
    return {
        Coord:new(coord.x, coord.y),
        Coord:new(coord.x + 1, coord.y),
        Coord:new(coord.x, coord.y + 1),
        Coord:new(coord.x + 1, coord.y + 1)
    }
end

function tub(coord)
    return {
        Coord:new(coord.x + 1, coord.y),
        Coord:new(coord.x, coord.y + 1),
        Coord:new(coord.x + 2, coord.y + 1),
        Coord:new(coord.x + 1, coord.y + 2)
    }
end
