#!/usr/bin/env lua

lu = require('luaunit')
require "conway/world"

function testBlock()
    local alive = block(Coord:new(0, 0))
    local original = World:new(5, alive)
    local next = original:evolve()
    lu.assertEquals(next, original)
end

function testTub()
    local alive = tub(Coord:new(0, 0))
    local original = World:new(5, alive)
    local next = original:evolve()
    lu.assertEquals(next, original)
end

function testBlinker()
    local alive = blinker(Coord:new(0, 1))
    local original = World:new(3, alive)
    local gen1 = original:evolve()
    local expectedAlive = {
        Coord:new(1, 0),
        Coord:new(1, 1),
        Coord:new(1, 2)
    }
    local expected = World:new(3, expectedAlive)
    lu.assertEquals(expected, gen1)
    local gen2 = gen1:evolve()
    lu.assertEquals(original, gen2)
end

os.exit(lu.LuaUnit.run())
