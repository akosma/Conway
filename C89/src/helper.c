#include "helper.h"
#include <stdio.h>
#include <stdlib.h>

#if defined(__BORLANDC__) || defined(__MSC_VER) || defined(__POWERC__)
/* Borland C++ 3.1, Microsoft C/C++ 6.0 for DOS, and Mix PowerC */
#include <conio.h> /* clrscr(), or clrscrn() for PowerC */
#include <dos.h>   /* sleep() */

#elif defined(__MWERKS__)
/* Metrowerks CodeWarrior 1.1 for Mac OS 7 on 68k */
#include <Timer.h> /* Delay() */

#elif defined(THINK_C)
/* Symantec THINK C 5 for Mac OS 7 */
#include <console.h> /* To customize the window */
#include <unix.h>    /* sleep() */

#elif defined(__GNUC__) || defined(__WATCOMC__) || defined(__TINYC__)
/* GCC, Clang, and Watcom C/C++ */
#include <unistd.h> /* sleep() */
#endif

#if defined(__EMSCRIPTEN__)
#include <emscripten.h>

extern void emscripten_sleep(unsigned int);

EM_JS(void, emscripten_clrscr, (), {
    document.getElementById("output").innerText = "";
});
#endif

void init_console(void) {
#if defined(THINK_C)
    /* The title of the console is a Pascal string, not a C string! */
    unsigned char title[7] = {6, 'C', 'o', 'n', 'w', 'a', 'y'};
    console_options.nrows = 35;
    console_options.ncols = 126;
    console_options.pause_atexit = 0;
    console_options.title = title;
    cshow(stdout);
#endif
}

void wait(unsigned int seconds) {
#ifdef __MSC_VER
#define MK_FP(seg, ofs) ((void far *)(((unsigned long)(seg) << 16) | (unsigned)(ofs)))
    unsigned long startTime, elapsedTime;
    union REGS regs;

    /* Convert seconds to timer ticks (18.2 ticks per second) */
    unsigned long ticksToWait = (unsigned long)seconds * 18;

    /* Get the current timer ticks (BIOS Data Area 0x40:0x6C) */
    startTime = *(unsigned long far *)MK_FP(0x0040, 0x006C);

    do {
        /* Get the current timer ticks again */
        elapsedTime = *(unsigned long far *)MK_FP(0x0040, 0x006C) - startTime;

        /* Handle overflow of timer ticks (midnight rollover) */
        if ((long)elapsedTime < 0) {    /* Cast to signed long for comparison */
            elapsedTime += 86400L * 18; /* Number of ticks in a day */
        }

        /* Call DOS idle interrupt (INT 0x28) */
        int86(0x28, &regs, &regs);

    } while (elapsedTime < ticksToWait);
#elif defined(__MWERKS__)
    unsigned long ticks = seconds * 60;
    Delay(ticks, NULL);
#elif defined(__EMSCRIPTEN__)
    emscripten_sleep(seconds * 1000);
#else
    sleep(seconds);
#endif
}

void clear_screen(void) {
#if defined(__BORLANDC__)
    clrscr();
#elif defined(__EMSCRIPTEN__)
    emscripten_clrscr();
#elif defined(THINK_C)
    /* Do nothing, the Mac window has exactly the good size */
#elif defined(__MWERKS__)
    /* Poor man's clearing screen routine */
    int i;
    for (i = 0; i < 50; i++) {
        printf("\n");
    }
#elif defined(__GNUC__) || defined(__TINYC__)
    system("clear");
#elif defined(__WATCOMC__) || defined(__MSC_VER)
    system("cls");
#elif defined(__POWERC__)
    clrscrn();
#endif
}

void print_compiler(void) {
#if defined(__BORLANDC__)
    printf("Borland C++");
#elif defined(THINK_C)
    printf("THINK C");
#elif defined(__MWERKS__)
    printf("Metrowerks CodeWarrior");
#elif defined(__EMSCRIPTEN__)
    printf("Emscripten");
#elif defined(__ZIG__)
    printf("Zig CC");
#elif defined(__GNUC__) && !defined(__clang__)
    printf("GNU Compiler Collection");
#elif defined(__clang__)
    printf("Clang for LLVM");
#elif defined(__WATCOMC__)
    printf("Watcom C/C++");
#elif defined(__MSC_VER)
    printf("Microsoft C/C++");
#elif defined(__POWERC__)
    printf("PowerC");
#elif defined(__TINYC__)
    printf("Tiny C");
#endif
}
