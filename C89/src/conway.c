#include "world.h"
#include "helper.h"
#include <signal.h>
#include <stdio.h>

/* Text consoles on some platforms are smaller,
don't show the full world in those cases */
#if (defined(__GNUC__) || defined(THINK_C) || defined(__TINYC__)) && !defined(__EMSCRIPTEN__)
#define DISPLAY_SIZE WORLD_SIZE
#else
#define DISPLAY_SIZE 18
#endif

/* Borland C++ on OS/2 requires a special
decoration on signal handlers */
#if defined(__OS2__) && defined(__BORLANDC__)
#define ENTRY _USERENTRY
#else
#define ENTRY
#endif

/* Variable used to stop the application */
volatile sig_atomic_t signal_received = 0;

/* Called when user hits CTRL+C, causing the animation to stop */
void ENTRY signal_handler(int sig) {
    printf("Signal received: %d\n", sig);
    signal_received = 1;
}

/* Main entry point */
int main(void) {
    int generation = 0;
    world_t world; /* Global variable on the stack passed as reference */

    init_console();
    signal(SIGINT, signal_handler);

    world_init(&world);
    world_blinker(&world, 0, 1);
    world_beacon(&world, 10, 10);
    world_glider(&world, 4, 5);
    world_block(&world, 1, 10);
    world_block(&world, 18, 3);
    world_tub(&world, 6, 1);

    while (!signal_received) {
        clear_screen();
        world_print(&world, DISPLAY_SIZE);
        generation++;
        print_compiler();
        printf("\nGeneration %d\n", generation);
        wait(1); /* Pause for 1 second */
        world_evolve(&world);
    }
    clear_screen();
    return 0;
}
