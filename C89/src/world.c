#include "world.h"
#include <stdio.h>
#include <string.h>
#include <signal.h>

#define ALIVE 1
#define DEAD 0

void world_init(world_t *world) {
    memset(world->cells, DEAD, sizeof(world->cells));
}

void world_evolve(world_t *world) {
    world_t copy;
    int counter;
    int x = 0;
    int y = 0;
    int a = 0;
    int b = 0;

    world_copy(&copy, world);
    world_init(world);

    for (x = 0; x < WORLD_SIZE; x++) {
        for (y = 0; y < WORLD_SIZE; y++) {
            counter = 0;

            /* Check the neighbors */
            for (a = x - 1; a <= x + 1; a++) {
                for (b = y - 1; b <= y + 1; b++) {
                    if (a >= 0 && b >= 0 && a < WORLD_SIZE && b < WORLD_SIZE) {
                        counter += world_get(&copy, a, b);
                    }
                }
            }

            counter -= world_get(&copy, x, y); /* Subtract self */

            /* Apply the rules */
            if (world_get(&copy, x, y) == 1) {
                if (counter == 2 || counter == 3) {
                    world_set_alive(world, x, y);
                }
            } else {
                if (counter == 3) {
                    world_set_alive(world, x, y);
                }
            }
        }
    }
}

void world_copy(world_t *destination, world_t *origin) {
    memcpy(destination->cells, origin->cells, sizeof(origin->cells));
}

unsigned int world_equals(world_t *first, world_t *second) {
    return memcmp(first->cells, second->cells, sizeof(first->cells)) == 0;
}

void world_print(world_t *world, int max_size) {
    int a = 0;
    int b = 0;

    printf("\n");
    for (a = 0; a < max_size; a++) {
        if (a == 0) {
            printf("    ");
            for (b = 0; b < max_size; b++) {
                printf(" %2d|", b);
            }
            printf("\n");
        }
        printf(" %2d|", a);
        for (b = 0; b < max_size; b++) {
            if (world_is_alive(world, b, a)) {
                printf(" x |");
            } else {
                printf("   |");
            }
        }
        printf("\n");
    }
}

unsigned int world_is_alive(world_t *world, unsigned int x, unsigned int y) {
    return world_get(world, x, y) == ALIVE;
}

unsigned int world_get(world_t *world, unsigned int x, unsigned int y) {
    if (x < WORLD_SIZE && y < WORLD_SIZE) {
        return world->cells[x][y];
    }
    raise(SIGABRT);
    return 0;
}

void world_set_alive(world_t *world, unsigned int x, unsigned int y) {
    if (x < WORLD_SIZE && y < WORLD_SIZE) {
        world->cells[x][y] = ALIVE;
    }
}

void world_set_dead(world_t *world, unsigned int x, unsigned int y) {
    if (x < WORLD_SIZE && y < WORLD_SIZE) {
        world->cells[x][y] = DEAD;
    }
}

void world_blinker(world_t *world, unsigned int x, unsigned int y) {
    world_set_alive(world, x, y);
    world_set_alive(world, x + 1, y);
    world_set_alive(world, x + 2, y);
}

void world_beacon(world_t *world, unsigned int x, unsigned int y) {
    world_set_alive(world, x, y);
    world_set_alive(world, x + 1, y);
    world_set_alive(world, x, y + 1);
    world_set_alive(world, x + 1, y + 1);
    world_set_alive(world, x + 2, y + 2);
    world_set_alive(world, x + 3, y + 2);
    world_set_alive(world, x + 2, y + 3);
    world_set_alive(world, x + 3, y + 3);
}

void world_glider(world_t *world, unsigned int x, unsigned int y) {
    world_set_alive(world, x + 2, y + 2);
    world_set_alive(world, x + 1, y + 2);
    world_set_alive(world, x, y + 2);
    world_set_alive(world, x + 2, y + 1);
    world_set_alive(world, x + 1, y);
}

void world_tub(world_t *world, unsigned int x, unsigned int y) {
    world_set_alive(world, x + 1, y);
    world_set_alive(world, x, y + 1);
    world_set_alive(world, x + 2, y + 1);
    world_set_alive(world, x + 1, y + 2);
}

void world_block(world_t *world, unsigned int x, unsigned int y) {
    world_set_alive(world, x, y);
    world_set_alive(world, x + 1, y);
    world_set_alive(world, x, y + 1);
    world_set_alive(world, x + 1, y + 1);
}
