#ifndef WORLD_H
#define WORLD_H

#define WORLD_SIZE 30

/* Abstract data type holding a world filled with cells */
typedef struct world_t {
    unsigned int cells[WORLD_SIZE][WORLD_SIZE];
} world_t;

/* Resets world to all zeros */
void world_init(world_t *);

/* Evolve the world to the next generation */
void world_evolve(world_t *);

/* Copy the contents of the second world passed in argument unto the first */
void world_copy(world_t *, world_t *);

/* Compares two worlds and returns 1 if they are equal */
unsigned int world_equals(world_t *, world_t *);

/* Output a visual representation of the world */
void world_print(world_t *, int);

/* Returns 1 if the specified coordinate is alive, 0 otherwise */
unsigned int world_is_alive(world_t *, unsigned int, unsigned int);

/* Gets the value stored at the specified coordinate */
unsigned int world_get(world_t *, unsigned int, unsigned int);

/* Sets the specified coordinate as alive */
void world_set_alive(world_t *, unsigned int, unsigned int);

/* Sets the specified coordinate as dead */
void world_set_dead(world_t *, unsigned int, unsigned int);

/* Insert a Blinker at the specified location on the world */
void world_blinker(world_t *, unsigned int, unsigned int);

/* Insert a Beacon at the specified location on the world */
void world_beacon(world_t *, unsigned int, unsigned int);

/* Insert a Glider at the specified location on the world */
void world_glider(world_t *, unsigned int, unsigned int);

/* Insert a Tub at the specified location on the world */
void world_tub(world_t *, unsigned int, unsigned int);

/* Insert a Block at the specified location on the world */
void world_block(world_t *, unsigned int, unsigned int);

#endif /* WORLD_H */
