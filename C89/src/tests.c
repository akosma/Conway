#include "world.h"
#include "helper.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

void block(void) {
    world_t original;
    world_t next;

    world_init(&original);
    world_block(&original, 0, 0);
    world_evolve(&original);

    world_init(&next);
    world_block(&next, 0, 0);
    assert(world_equals(&original, &next));
    printf("Block passed\n");
}

void tub(void) {
    world_t original;
    world_t next;

    world_init(&original);
    world_tub(&original, 0, 0);
    world_evolve(&original);

    world_init(&next);
    world_tub(&next, 0, 0);
    assert(world_equals(&original, &next));
    printf("Tub passed\n");
}

void blinker(void) {
    world_t original;
    world_t gen1;
    world_t gen2;

    world_init(&original);
    world_blinker(&original, 0, 1);
    world_copy(&gen2, &original);

    world_init(&gen1);
    world_set_alive(&gen1, 1, 0);
    world_set_alive(&gen1, 1, 1);
    world_set_alive(&gen1, 1, 2);

    world_evolve(&original);
    assert(world_equals(&original, &gen1));
    world_evolve(&original);
    assert(world_equals(&original, &gen2));
    printf("Blinker passed\n");
}

int main(void) {
    block();
    tub();
    blinker();
    printf("All tests passed\nCompiled with: ");
    print_compiler();
    printf("\n");
    return 0;
}
