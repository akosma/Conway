#ifndef HELPER_H
#define HELPER_H

/* Pause execution for the time specified */
void wait(unsigned int);

/* Clear the console */
void clear_screen(void);

/* Prints a text identifying the current compiler name on the console */
void print_compiler(void);

/* Used by THINK C to customize console window */
void init_console(void);

#endif /* HELPER_H */
