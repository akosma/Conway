import std;
import core.thread;
import conwaylib;

bool stop = false;

extern(C) void signal(int sig, void function(int));
extern(C) void exit(int exit_val);

extern(C) void handler(int sig)
{
    stop = true;
}

// Courtesy of
// https://forum.dlang.org/post/feujxfibcgjymsgifgto@forum.dlang.org
void clrscr()
{
    spawnShell("clear").wait;
}

void main()
{
    enum SIGINT = 2;
    signal(SIGINT, &handler);
    scope (exit)
    {
        clrscr();
    }

    Coord[] alive = blinker(Coord(0, 1));
    alive ~= blinker(Coord(0, 1));
    alive ~= beacon(Coord(10, 10));
    alive ~= glider(Coord(4, 5));
    alive ~= block(Coord(1, 10));
    alive ~= block(Coord(18, 3));
    alive ~= tub(Coord(6, 1));

    World world = World(30, alive);
    int generation = 0;
    while (!stop)
    {
        clrscr();
        generation++;
        writeln(world);
        writeln(format("Generation %d", generation));
        Thread.sleep(dur!("msecs")(500));
        world = world.evolve();
    }
}
