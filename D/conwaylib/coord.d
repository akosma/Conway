module conwaylib.coord;

import std;

struct Coord
{
    int x;
    int y;

    string toString() const @safe pure
    {
        return format("%d:%d", this.x, this.y);
    }
}
