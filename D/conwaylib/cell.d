module conwaylib.cell;

enum Cell
{
    Alive, Dead
}

string cellToString(Cell c) @safe pure
{
    return c == Cell.Alive ? " x |" : "   |";
}
