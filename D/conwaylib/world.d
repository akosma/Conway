module conwaylib.world;

import std;
import std.algorithm: canFind;
import conwaylib.cell;
import conwaylib.coord;

struct World
{
    int size;
    Cell[Coord] cells;

    this(int size, Coord[] aliveCells) pure
    {
        this.size = size;
        for (int a = 0; a < size; ++a)
        {
            for (int b = 0; b < size; ++b)
            {
                Coord c = { a, b };
                if (aliveCells.canFind(c))
                {
                    cells[c] = Cell.Alive;
                }
                else
                {
                    cells[c] = Cell.Dead;
                }
            }
        }
    }

    World evolve() const pure
    {
        Coord[] alive;
        int[3] range = [ -1, 0, 1 ];
        foreach (coord, cell ; cells)
        {
            int count = 0;
            foreach (a ; range)
            {
                foreach (b ; range)
                {
                    Coord currentCoord = { coord.x + a, coord.y + b };
                    if (currentCoord != coord && currentCoord in cells && cells[currentCoord] == Cell.Alive)
                    {
                        count += 1;
                    }
                }
            }

            final switch(cell)
            {
                case Cell.Alive:
                    if (count == 2 || count == 3)
                    {
                        alive ~= coord;
                    }
                    break;

                case Cell.Dead:
                    if (count == 3)
                    {
                        alive ~= coord;
                    }
                    break;
            }
        }
        return World(size, alive);
    }

    string toString() const @safe pure
    {
        auto str = appender!string;
        str.reserve(size * size + size * 2);
        str ~= "\n";
        for (int a = 0; a < size; ++a)
        {
            if (a == 0)
            {
                // First line with coordinates
                str ~= "    ";
                for (int b = 0; b < size; ++b)
                {
                    str ~= format("%3d|", b);
                }
                str ~= "\n";
            }
            str ~= format("%3d|", a);
            for (int b = 0; b < size; ++b)
            {
                Coord coord = { b, a };
                Cell cell = cells[coord];
                str ~= cellToString(cell);
            }
            str ~= "\n";
        }
        return str.data();
    }
}

Coord[] blinker(Coord coord)
{
    Coord[] result = [
        { coord.x,     coord.y },
        { coord.x + 1, coord.y },
        { coord.x + 2, coord.y } ];
    return result;
}

Coord[] beacon(Coord coord)
{
    Coord[] result = [
            { coord.x,     coord.y     },
            { coord.x + 1, coord.y     },
            { coord.x,     coord.y + 1 },
            { coord.x + 1, coord.y + 1 },
            { coord.x + 2, coord.y + 2 },
            { coord.x + 3, coord.y + 2 },
            { coord.x + 2, coord.y + 3 },
            { coord.x + 3, coord.y + 3 } ];
    return result;
}

Coord[] glider(Coord coord)
{
    Coord[] result = [
            { coord.x + 2, coord.y + 2 },
            { coord.x + 1, coord.y + 2 },
            { coord.x,     coord.y + 2 },
            { coord.x + 2, coord.y + 1 },
            { coord.x + 1, coord.y     } ];
    return result;
}

Coord[] block(Coord coord)
{
    Coord[] result = [
            { coord.x,     coord.y     },
            { coord.x + 1, coord.y     },
            { coord.x,     coord.y + 1 },
            { coord.x + 1, coord.y + 1 } ];
    return result;
}

Coord[] tub(Coord coord)
{
    Coord[] result = [
            { coord.x + 1, coord.y     },
            { coord.x,     coord.y + 1 },
            { coord.x + 2, coord.y + 1 },
            { coord.x + 1, coord.y + 2 } ];
    return result;
}

unittest
{
    auto alive = block(Coord(0, 0));
    World original = World(5, alive);
    auto next = original.evolve();
    assert(next == original);
}

unittest
{
    auto alive = tub(Coord(0, 0));
    World original = World(5, alive);
    auto next = original.evolve();
    assert(next == original);
}

unittest
{
    Coord[] alive = blinker(Coord(0, 1));
    World original = World(3, alive);
    auto gen1 = original.evolve();
    Coord[] expectedAlive = [
            { 1, 0 },
            { 1, 1 },
            { 1, 2 }
    ];
    World expected = World(3, expectedAlive);
    assert(expected == gen1);
    auto gen2 = gen1.evolve();
    assert(original == gen2);
}
