require "coord"
require "world"

Signal::INT.trap do
  system "clear"
  exit 0
end

alive_cells = Array(Coord).new
alive_cells += World.blinker Coord.new(0, 1)
alive_cells += World.beacon Coord.new(10, 10)
alive_cells += World.glider Coord.new(4, 5)
alive_cells += World.block Coord.new(1, 10)
alive_cells += World.block Coord.new(18, 3)
alive_cells += World.tub Coord.new(6, 1)
w = World.new(30, alive_cells)
generation = 0

while true
  system "clear"
  generation += 1
  puts w
  puts "Generation %d" % generation
  sleep(0.5)
  w = w.evolve
end
