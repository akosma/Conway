require "./cell.cr"
require "./coord.cr"

include Cell

class World
  getter :size
  getter :cells

  def initialize(size : Int32, alive_cells : Array(Coord))
    @size = size
    @cells = Hash(Coord, Int32).new

    (0...size).each do |a|
      (0...size).each do |b|
        c = Coord.new(a, b)
        if alive_cells.any? { |ac| ac.x == c.x && ac.y == c.y }
          @cells[c] = Cell::ALIVE
        else
          @cells[c] = Cell::DEAD
        end
      end
    end
  end

  def evolve
    alive_cells = Array(Coord).new
    @cells.each do |coord, cell|
      count = 0
      [-1, 0, 1].each do |a|
        [-1, 0, 1].each do |b|
          current_coord = Coord.new(coord.x + a, coord.y + b)
          count += 1 if current_coord != coord && @cells.has_key?(current_coord) && @cells[current_coord] == Cell::ALIVE
        end
      end

      case cell
      when Cell::ALIVE
        alive_cells.push(coord) if count == 2 || count == 3
      else
        alive_cells.push(coord) if count == 3
      end
    end
    World.new(@size, alive_cells)
  end

  def hash(hasher)
    hasher = @size.hash(hasher)
    hasher = @cells.hash(hasher)
    hasher
  end

  def ==(w)
    hash == w.hash
  end

  def to_s(io : IO)
    (0...@size).each do |a|
      # First line with coordinates
      if a == 0
        io << "    "
        (0...@size).each do |b|
          io << "%3s|" % b
        end
        io << "\n"
      end
      io << "%3s|" % a
      (0...@size).each do |b|
        c = Coord.new(b, a)
        cell = @cells[c]
        io << cell_to_s(cell)
      end
      io << "\n"
    end
  end

  def self.blinker(c : Coord)
    [ Coord.new(c.x, c.y),
      Coord.new(c.x + 1, c.y),
      Coord.new(c.x + 2, c.y) ]
  end

  def self.beacon(c : Coord)
    [ Coord.new(c.x, c.y),
      Coord.new(c.x + 1, c.y),
      Coord.new(c.x, c.y + 1),
      Coord.new(c.x + 1, c.y + 1),
      Coord.new(c.x + 2, c.y + 2),
      Coord.new(c.x + 3, c.y + 2),
      Coord.new(c.x + 2, c.y + 3),
      Coord.new(c.x + 3, c.y + 3) ]
  end

  def self.glider(c : Coord)
    [ Coord.new(c.x + 2, c.y + 2),
      Coord.new(c.x + 1, c.y + 2),
      Coord.new(c.x, c.y + 2),
      Coord.new(c.x + 2, c.y + 1),
      Coord.new(c.x + 1, c.y) ]
  end

  def self.block(c : Coord)
    [ Coord.new(c.x, c.y),
      Coord.new(c.x + 1, c.y),
      Coord.new(c.x, c.y + 1),
      Coord.new(c.x + 1, c.y + 1) ]
  end

  def self.tub(c : Coord)
    [ Coord.new(c.x + 1, c.y),
      Coord.new(c.x, c.y + 1),
      Coord.new(c.x + 2, c.y + 1),
      Coord.new(c.x + 1, c.y + 2) ]
  end
end