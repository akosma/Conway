class Coord
  getter :x
  getter :y

  def initialize(x : Int32, y : Int32)
    @x = x
    @y = y
  end

  def hash(hasher)
    hasher = @x.hash(hasher)
    hasher = @y.hash(hasher)
    hasher
  end

  def ==(c)
    hash == c.hash
  end

  def !=(c)
    hash != c.hash
  end

  def to_s(io : IO)
    io << x << ":" << y
  end
end
