require "spec"
require "world"
require "coord"

describe "Conway" do
  it "block" do
    alive = World.block Coord.new(0, 0)
    original = World.new(5, alive)
    next_world = original.evolve
    next_world.should eq(original)
  end

  it "tub" do
    alive = World.tub Coord.new(0, 0)
    original = World.new(5, alive)
    next_world = original.evolve
    next_world.should eq(original)
  end

  it "blinker" do
    alive = World.blinker Coord.new(0, 1)
    original = World.new(3, alive)
    gen1 = original.evolve
    expected_alive = [ Coord.new(1, 0),
        Coord.new(1, 1),
        Coord.new(1, 2) ]
    expected = World.new(3, expected_alive)
    expected.should eq(gen1)
    gen2 = gen1.evolve
    original.should eq(gen2)
  end
end
